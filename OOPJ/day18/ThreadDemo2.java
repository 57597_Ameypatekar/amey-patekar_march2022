//1.Multithreading
//Assigning different task
class AddThread extends Thread{
	
		public void run(){
			System.out.println(Thread.currentThread().getName()+" Stared ");
			int sum=0;
			for(int i=1;i<=5;i++){
				sum=sum+i;
			
			}
			System.out.println("Sum :"+sum+" "+Thread.currentThread().getName()+" Ended ");
		}
}

class MultiplyThread extends Thread{
	
		public void run(){
			System.out.println(Thread.currentThread().getName()+" Stared ");
			int res=1;
			for(int i=1;i<=5;i++){
				res=res*i;
				
				
			}
			System.out.println("Result :"+res+" "+Thread.currentThread().getName()+" Ended ");
		}
}

class ThreadDemo2{
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName()+" Started");//main Started
		
		
		AddThread t1=new AddThread();//new state
		t1.setName("Add-Thread");//assigning the name
		MultiplyThread t2=new MultiplyThread();//new state
		t2.setName("Multiply-Thread");
		
		System.out.println(Thread.currentThread().getName()+" Starting Thread");
		t1.start();//Ready State
		t2.start();//Ready State
		
		
		System.out.println(Thread.currentThread().getName()+" Starting of Threads completed");
		
	}
}

/*
main Started
main Starting Thread
main Starting of Threads completed
Add-Thread Stared
Multiply-Thread Stared
Result :120 Multiply-Thread Ended
Sum :15 Add-Thread Ended
*/