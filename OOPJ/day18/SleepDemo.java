//how to control schedular
//sleep is static method
class MyThread6 extends Thread{
	public void run(){
			for(int i=1;i<=5;i++){
				System.out.println(i+" -> "+Thread.currentThread().getName());
			}
			System.out.println(Thread.currentThread().getName()+" Sleeping");
			try{
			Thread.sleep(20000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" Awake and done");
		}
		
	}


class SleepDemo{
	public static void main(String []args)throws InterruptedException{
		System.out.println(Thread.currentThread().getName()+" Started");//main Started
		MyThread6 t1 = new MyThread6();//new state
		MyThread6 t2 = new MyThread6();//new state
		MyThread6 t3 = new MyThread6();//new state
		MyThread6 t4 = new MyThread6();//new state
		
		t1.start();//ready state
		Thread.sleep(4000);
		t2.start();//ready state
		try{
			Thread.sleep(4000);
		}//running state
		catch(InterruptedException e){}
		t3.start();//ready state
		try{
			Thread.sleep(4000);
		}//running state
		catch(InterruptedException e){}
		t4.start();//ready state
		try{
			Thread.sleep(4000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" Starting of Threads completed");
	}
}

/*

*/