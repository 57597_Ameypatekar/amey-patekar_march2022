//Runnable interface
//share common implemention 
class MyRunnable implements Runnable{
	@Override
	public void run(){
			for(int i=1;i<=5;i++){
				System.out.println(i+" -> "+Thread.currentThread().getName());
			}
		}
	}


class RunnableDemo1{
	public static void main(String [] args){
		MyRunnable r1=new MyRunnable();//this is not thread
		
		Thread t1=new Thread(r1);//new state
		t1.setName("T1");
		Thread t2=new Thread(r1,"T2");//new state
		
		t1.start();
		t2.start();
	}
}

/*
1 -> T1
1 -> T2
2 -> T1
2 -> T2
3 -> T1
3 -> T2
4 -> T1
4 -> T2
5 -> T1
5 -> T2
*/