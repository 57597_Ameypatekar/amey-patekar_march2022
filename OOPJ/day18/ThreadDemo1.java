//1.Multithreading
//sharing common task
class MyThread extends Thread{
	
		public void run(){
			for(int i=1;i<=5;i++){
				System.out.println(i+" -> "+Thread.currentThread().getName());
				
			}
		}
}

class ThreadDemo1{
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName()+" Started");//main Started
		
		
		MyThread t1=new MyThread();//new state
		t1.setName("Thread 1");//assigning the name
		MyThread t2=new MyThread();//new state
		t2.setName("Thread 2");
		MyThread t3=new MyThread();//new state
		t3.setName("Thread 3");
		MyThread t4=new MyThread();//new state
		t4.setName("Thread 4");
		//all threads are under main thread
		System.out.println(Thread.currentThread().getName()+" Starting Thread");
		t1.start();//Ready State
		t2.start();//Ready State
		t3.start();//Ready State
		t4.start();//Ready State
		
		System.out.println(Thread.currentThread().getName()+" Starting of Threads completed");
		/*while(true)
		{
		System.out.println(Thread.currentThread().getName()+" Alive....!!!");
		}*/
	}
}

/*
main Started
main Starting Thread
main Starting of Threads completed
1 -> Thread 4
1 -> Thread 2
1 -> Thread 3
2 -> Thread 2
2 -> Thread 4
1 -> Thread 1
3 -> Thread 4
3 -> Thread 2
2 -> Thread 3
4 -> Thread 2
4 -> Thread 4
2 -> Thread 1
5 -> Thread 4
5 -> Thread 2
3 -> Thread 3
3 -> Thread 1
4 -> Thread 3
4 -> Thread 1
5 -> Thread 3
5 -> Thread 1
*/