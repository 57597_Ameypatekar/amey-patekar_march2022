//how to control schedular
class MyThread5 extends Thread{
	public void run(){
			for(int i=1;i<=5;i++){
				System.out.println(i+" -> "+Thread.currentThread().getName());
			}
		}
	}


class JoinDemo{
	public static void main(String []args){
		System.out.println(Thread.currentThread().getName()+" Started");//main Started
		MyThread5 t1 = new MyThread5();//new state
		MyThread5 t2 = new MyThread5();//new state
		MyThread5 t3 = new MyThread5();//new state
		MyThread5 t4 = new MyThread5();//new state
		
		t1.start();//ready state
		try{
			t1.join();
		}//running state
		catch(InterruptedException e){}
		t2.start();//ready state
		try{
			t2.join();
		}//running state
		catch(InterruptedException e){}
		t3.start();//ready state
		try{
			t3.join();
		}//running state
		catch(InterruptedException e){}
		t4.start();//ready state
		try{
			t4.join();
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" Starting of Threads completed");
	}
}

/*
main Started
1 -> Thread-0
2 -> Thread-0
3 -> Thread-0
4 -> Thread-0
5 -> Thread-0
1 -> Thread-1
2 -> Thread-1
3 -> Thread-1
4 -> Thread-1
5 -> Thread-1
1 -> Thread-2
2 -> Thread-2
3 -> Thread-2
4 -> Thread-2
5 -> Thread-2
1 -> Thread-3
2 -> Thread-3
3 -> Thread-3
4 -> Thread-3
5 -> Thread-3
main Starting of Threads completed

*/