//assign differet task to multiple thread using runnable interface
class AddRunnable implements Runnable{
	@Override
	public void run(){
			System.out.println(Thread.currentThread().getName()+" Stared ");
			int sum=0;
			for(int i=1;i<=5;i++){
				sum=sum+i;
				
				
			}
			System.out.println("Sum :"+sum+" "+Thread.currentThread().getName()+" Ended ");
		
	}
}
class MultiplyRunnable implements Runnable{
	@Override
	public void run(){
			System.out.println(Thread.currentThread().getName()+" Stared ");
			int res=1;
			for(int i=1;i<=5;i++){
				res=res*i;
			}
			System.out.println("Result :"+res+" "+Thread.currentThread().getName()+" Ended ");
		
		}
	}	


class RunnableDemo2{
	public static void main(String [] args){
		AddRunnable r1=new AddRunnable();//this is not thread
		MultiplyRunnable r2=new MultiplyRunnable();//this is not thread
		
		Thread t1=new Thread(r1,"Addthread");//new state
		Thread t2=new Thread(r2,"Multiplythread");//new state
		
		t1.start();
		t2.start();
		
		//Anonymous innerclass
		Runnable evenadd=new Runnable(){
			public void run(){
			System.out.println(Thread.currentThread().getName()+" Started ");
			int res=0;
			for(int i=1;i<=20;i++){
				if(i%2==0)
				{res+=i;}
			}
			System.out.println("Even Add Result :"+res+" "+Thread.currentThread().getName()+" Ended ");
		}
		};
		
		Thread t3=new Thread(evenadd,"Even-Add");//new state
		t3.start();//readystate
		
		
		System.out.println();
		
		
		//Lambda Expression
		Runnable oddAdd=()->{
			System.out.println(Thread.currentThread().getName()+" Stared ");
			int res=1;
			for(int i=1;i<=5;i++){
				res=res*i;
			}
			System.out.println("Result :"+res+" "+Thread.currentThread().getName()+" Ended ");
		};
		Thread t4=new Thread(oddAdd,"Odd-Add-thread");//new state
		t4.start();//readystate
		
	}
}