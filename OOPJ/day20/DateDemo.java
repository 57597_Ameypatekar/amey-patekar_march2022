import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
class DateDemo{
	public static void main(String[] args)throws ParseException{
		Date d =new Date();
		System.out.println(d);//Thu Apr 14 08:38:58 IST 2022
		
		//coverting date object to string
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strDate=sdf.format(d);
		System.out.println(strDate);//14/04/2022
		
		
		//coverting string to date
		String strDate1="01/01/2022";
		SimpleDateFormat sdf1=new SimpleDateFormat("dd/MM/yyyy");
		Date d1=sdf1.parse(strDate1); 
		System.out.println(d1);
		
		
		//Calender is a singleton class
		Calendar cal= Calendar.getInstance();
		System.out.println(cal);
		System.out.println(cal.get(Calendar.YEAR));
		System.out.println(cal.get(Calendar.MONTH)+1);
	}
}