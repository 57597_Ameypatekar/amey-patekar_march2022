//Instance Reference Variable

class A{
	int x;  //x=0
	int y;  //y=0
}

class InstanceRefVariable{
	int i;  //i=0
	int j;   //j=0
	A a;     // class A gets load  a=null
}

class InstanceRefVariableDemo{
	public static void main(String []args){
		InstanceRefVariable irv = new InstanceRefVariable();
		irv.i = 100;
		irv.j = 200;
		
		irv.a = new A();  // object created for class A
		irv.a.x=111;
		irv.a.y=222;
		
		System.out.println(irv.i);
		System.out.println(irv.j);
		System.out.println(irv.a.x);
		System.out.println(irv.a.y);
	}
}


