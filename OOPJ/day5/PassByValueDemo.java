class PassByValue{
	int a;
	int b;
	
	PassByValue(int a, int b){
	this.a=a;
	this.b=b;
	}

	void add(int a, int b){
	
	
	}
	
	static void m1(PassByValue o1, PassByValue o2)   // type and name of argument
	{
		o1.a=100;
		o1.b=200;
		o2.a=111;
		o2.b=222;
	}
	
	void m2( PassByValue o){
		
		this.a=1000;
		this.b=2000;
		
		o.a=1111;
		o.b=2222;
		
		//o1.a=1110;
		//o1.b=2220;
		
	}
}	
class PassByValueDemo{
	public static void main(String [] args){
	PassByValue oa1 = new PassByValue(10,20);
	PassByValue oa2 = new PassByValue(11,22);
	//PassByValue oa3 = new PassByValue(111,222);
	System.out.println(oa1.a+" "+oa1.b);// 10  20
	System.out.println(oa2.a+" "+oa2.b);// 11  22 
	
	PassByValue.m1(oa1,oa2);
	
	System.out.println(oa1.a+" "+oa1.b);//100  200
	System.out.println(oa2.a+" "+oa2.b);//111  222
	
	
	//oa1.m2(oa2,oa3);
	oa1.m2(oa2);	
	
	System.out.println(oa1.a+" "+oa1.b);//1000 2000
	System.out.println(oa2.a+" "+oa2.b);//1111 2222
	//System.out.println(oa3.a+" "+oa3.b);//1110 2220
	}
}
