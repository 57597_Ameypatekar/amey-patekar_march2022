class B{
	int p;
	int q;
	
}

class A{
	int x;
	int y;
	static B b;
}

class StaticRefVariable{
	
	static int i;
	static StaticRefVariable svar;   //svar=default value null
	static A avar;
	
	int a;
	int b;
	
}

class StaticRefVariableDemo{
	public static void main(String []args){
			
		System.out.println(StaticRefVariable.i);
		System.out.println(StaticRefVariable.svar);
		//StaticRefVariable.svar = null by default
		
		StaticRefVariable.i=100;
		StaticRefVariable.svar=new StaticRefVariable ();//object created
		
		System.out.println(StaticRefVariable.i);
		System.out.println(StaticRefVariable.svar);
		
		System.out.println(StaticRefVariable.svar.a);
		System.out.println(StaticRefVariable.svar.b);
		
		StaticRefVariable.svar.a=100;
		StaticRefVariable.svar.b=200;
		
		System.out.println(StaticRefVariable.svar.a);
		System.out.println(StaticRefVariable.svar.b);
		
		
		StaticRefVariable.avar=new A();
		System.out.println(StaticRefVariable.avar.x);
		System.out.println(StaticRefVariable.avar.y);
		
		
		StaticRefVariable.avar.x=111;
		StaticRefVariable.avar.y=222;
		System.out.println(StaticRefVariable.avar.x);
		System.out.println(StaticRefVariable.avar.y);
		
		
		A.b = new B();
		System.out.println(A.b.p);
		System.out.println(A.b.q);
		A.b.p=100;
		A.b.q=200;
		System.out.println(A.b.p);
		System.out.println(A.b.q);
		
		
		StaticRefVariable.avar.b.p=111;
		StaticRefVariable.avar.b.q=222;
		System.out.println(StaticRefVariable.avar.b.p);
		System.out.println(StaticRefVariable.avar.b.q);
	}
}