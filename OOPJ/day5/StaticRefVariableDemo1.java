// Static reference variable
class A{
	int x;
	int y;
	
}

class StaticRefVariable{
	
	static int i;
	static A avar;
	
	
	
}

class StaticRefVariableDemo1{
	public static void main(String []args){
			
		StaticRefVariable.i=100;
		StaticRefVariable.avar=new A();
		StaticRefVariable.avar.x=111;
		StaticRefVariable.avar.y=222;
		
		System.out.println(StaticRefVariable.i);
		System.out.println(StaticRefVariable.avar.x);
		System.out.println(StaticRefVariable.avar.y);
	}
}