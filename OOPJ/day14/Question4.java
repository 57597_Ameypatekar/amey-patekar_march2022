//question4
import java.io.*;
class AgeException extends Exception{
	AgeException(String em) {
	super(em);
	
	}
}
class Question4{
	
	public static void main(String []args){
	
		String s1=args[0];
		int s2=Integer.parseInt(args[1]);	
			
		System.out.println(s1+" "+s2);
	
	
	try{
	 if(s2>=18&&s2<60)
	 {
		System.out.println("valid age"); 
	 }
	 else{
		 
		 throw new AgeException("invalid");
	 }
	}
	catch(Exception	e){
		
		e.printStackTrace();
		
		System.out.println("Your Entered age is not in range");
		
	}
	}
}