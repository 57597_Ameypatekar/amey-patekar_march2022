//question2
import java.util.*;
class Question2{

	public static void main(String []args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter number for array length :");
	int num=sc.nextInt();
	int a[]=new int[num];
	System.out.println("Enter array elements :");
	for (int i=0;i<a.length;i++)
		a[i]=sc.nextInt();
	try{
		System.out.println("Enter Array element index :");
		int n=sc.nextInt();
		System.out.println("Array element at index ["+n+"] is "+a[n]);
			
	}catch(ArrayIndexOutOfBoundsException e)
	{
		e.printStackTrace();
		System.out.println("Entered input is not a valid for array index");
	}
	finally{
		System.out.println("finally..!!!");
	}

	}
}