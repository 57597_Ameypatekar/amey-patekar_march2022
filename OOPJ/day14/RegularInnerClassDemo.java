//Regular inner class
class OuterClass{
	int x=100;
	public void m1(){
		System.out.println("Outer class:m1");
		
	}
	
	//instance inner class can access all access modifiers
	class InnerClass{
		
		//static variable and static methods are not allowed in inner class
		int y=200;
		
		void m2(){
			
			System.out.println("Inner class:m2");
		}
		
	
	}
}

class RegularInnerClassDemo{
	public static void main(String []args){
		OuterClass oc=new OuterClass();
		System.out.println(oc.x);
		oc.m1();
		//way1
		OuterClass.InnerClass inner = oc.new InnerClass();
		//way2
		//OuterClass.InnerClass inner = new OuterClass().new InnerClass();
		System.out.println(inner.y);
		inner.m2();
	}
}


/*
100
Outer class:m1
200
Inner class:m2
*/