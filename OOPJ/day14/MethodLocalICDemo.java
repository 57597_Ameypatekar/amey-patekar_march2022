//Method Local Inner class
class MethodLocalIC{
	
	
		void m1(){
			int x=10;  //local variable
			System.out.println(x);
			
			//this class is local thats why only can apply final to class
			class MLIC{
				
				int y=20;
				void m1(){
					System.out.println("MLIC:m1()");
					System.out.println(x);//x can access but not modify
					//x=100; CE x cannot modify
				}
				
			}
			
			MLIC mlic = new MLIC();
			
			mlic.m1();
			System.out.println(mlic.y);
		}
}


class MethodLocalICDemo{
		public static void main(String []args){
		
		MethodLocalIC mlic =new MethodLocalIC();
		mlic.m1();
		
		
	}
}

/*
10
MLIC:m1()
10
20
*/