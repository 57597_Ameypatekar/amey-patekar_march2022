//Anonymous Inner Class
interface I1{
	void m1();
	
}


class Parent extends I1{
		
	static void m2(){
		System.out.println("Parent :m2()");
	}
}

class AnonymousInnerClassDemo1{
		public static void main(String []args){
		
			I1 a = new I1(){
			
			@Override
			public void m1()
			{
				System.out.println("child m1");
			};
			
			//m2(a);
		
			m2(new I1(){
			
			@Override
			public void m1()
			{
				System.out.println("child m1");
			});
		}
		
	}