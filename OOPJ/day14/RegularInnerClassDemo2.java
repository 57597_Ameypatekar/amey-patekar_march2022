//Regular inner class
class OuterClass{
	int x=100;
	int a=400;
	public void m1(){
		System.out.println("Outer class:m1");
		System.out.println(x);
		
	}
	
	void m3(){
		//to access inner class variables and methods
		//OuterClass.InnerClass i= this.new Inner();
		InnerClass i= new InnerClass();
		System.out.println(i.y);//200
		i.m1();//Inner class:m1
	}
	
	
	
	//instance inner class can access all access modifiers
	class InnerClass{
		int a=500;
		//static variable and static methods are not allowed in inner class
		int y=200;
		
		void m2(){
			
			System.out.println("Inner class:m2");
			System.out.println(x);//innerclass can access instace variables of outerclass
			m1();//innerclass can access instace methods of outerclass but if same name method is there then it will call from same class method
			System.out.println(a);//500
			System.out.println(this.a);//500
			System.out.println(OuterClass.this.a);//400
			OuterClass.this.m1();//outerclass method calling
		}
		
		void m1(){
			System.out.println("Inner class:m1");
		}
		
	
	}
}

class RegularInnerClassDemo2{
	public static void main(String []args){
		OuterClass oc=new OuterClass();
		System.out.println(oc.x);
		oc.m1();
		//way1
		OuterClass.InnerClass inner = oc.new InnerClass();
		//way2
		//OuterClass.InnerClass inner = new OuterClass().new InnerClass();
		System.out.println(inner.y);
		inner.m2();
		
		System.out.println();
		oc.m3();
	}
}


/*
100
Outer class:m1
100
200
Inner class:m2
100
Inner class:m1
500
500
400
Outer class:m1
100

200
Inner class:m1
*/