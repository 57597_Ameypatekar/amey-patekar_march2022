//Anonymous Inner Class
interface I1{
	void m2();
	void m3();
}


class Parent{
		
	void m1(){
		System.out.println("Parent :m1()");
	}
}

class Child extends Parent{
	@Override
	void m1(){
		System.out.println("Child :m1()");
	}
}

class CYZ implements I1{
	public void m2(){
		
		
	}
	public void m3(){
		
	}
}

class AnonymousInnerClassDemo{
		public static void main(String []args){
		
		//syntax for anonymous innerclass
		Parent p = new Parent(){
		@Override
		void m1(){
		System.out.println("child :m1()");
	}
			
		};
		
		//instead of creating concrete class you can create anonymous class
		//         |->child class of interface so there is compulsion of implements unimplemented methods of interface
		I1 i = new I1(){
			
			@Override
			public void m2()
			{
				System.out.println("child m2");
			}
			@Override
			public void m3()
			{
				System.out.println("child m3");
			}
		
		};
		
		p.m1();
		i.m2();
		i.m3();
		
		
			
	}
}

//method alwayd resolved on object in this case i is object
/*
child :m1()
child m2
child m3
*/