//question1
import java.util.*;
class Question1{

	public static void main(String []args){
	Scanner sc=new Scanner(System.in);
	String s=sc.next();
	int i=0;
	try{
		i=Integer.parseInt(s);
	}catch(NumberFormatException e)
	{
		System.out.println("Entered input is not a valid format for an integer.");
	}
	finally{
		System.out.println("finally..!!!");
	}
	int k=i*i;
	System.out.println("Square of "+i+" : "+k);
	}
}