//Regular inner class inheritance
class OuterClass{
	int x=100;
	int a=400;
		
	class InnerClass{
		
	
	}
	
	class InnerClass2 extends InnerClass{
	
	
	}
}

class RegularInnerClassDemo3{
	public static void main(String []args){
		
		OuterClass.InnerClass i = new OuterClass().new InnerClass2();
		
	}
}


/*


*/