class StringIndexOutOfBoundsExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	String str="abcd";
	
	try{
		char c=str.charAt(6);
		System.out.println("this line will not print");
	}catch(StringIndexOutOfBoundsException e){
		e.printStackTrace();
		System.out.println("string elements exceeds !!!");
	}
	
	
	System.out.println("After");
	}
}