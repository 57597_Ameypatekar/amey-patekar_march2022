class A{}
class B extends A{}

class ClassCastExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	A a=new A();
	
	try
		{
			B b =(B)a;//class cast exception
		}
	
	catch(ClassCastException e)//catch is handler
		{
			System.out.println(e.getMessage());//class A cannot be cast to class B
			
			e.printStackTrace();/*java.lang.ClassCastException: class A cannot be cast to class B (A and B are in unnamed module of loader 'app')
			at ClassCastExceptionDemo.main(ClassCastExceptionDemo.java:10)*/
			
			System.out.println("Invalid Downcasting");
			System.out.println("Ref var can hold value of same class or child class");
		}
		System.out.println("After");
	}
}

//try-catch()-finally
//try-catch()
//try-finally