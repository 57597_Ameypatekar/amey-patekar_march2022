
class ArithmeticExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	
	try{
		int i=10/0;
		System.out.println("this line will not print");
	}catch(ArithmeticException e){
		e.printStackTrace();
		System.out.println("zero se kon divide karta he be !!!");
	}
	
	finally{
		System.out.println("Release resources");
	}
	
	
	System.out.println("After");
	}
}



/*
class ArithmeticExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	
		
	finally{
		System.out.println("Release resources");
	}
	
	
	System.out.println("After");//After will not print on console
	}
}
*/
