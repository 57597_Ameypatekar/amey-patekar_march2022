class IndexOutOfBoundsExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	int a[]=new int[5];
	String str="abcd";
	try{
		//a[6]=10;
		char c=str.charAt(6);
		System.out.println("this line will not print");
	}catch(ArrayIndexOutOfBoundsException e){
		e.printStackTrace();
		System.out.println("1");
	}/*catch(StringIndexOutOfBoundsException e){
		e.printStackTrace();
		System.out.println("2 ");
	}*/catch(IndexOutOfBoundsException e){
		e.printStackTrace();
		System.out.println("3");
	}
	
		
	System.out.println("After");
	}
}