class NumberFormatExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	String s="ABC";
	try{
		int i=Integer.parseInt(s);
		System.out.println("this line will not print");
	}catch(NumberFormatException e){
		e.printStackTrace();
		System.out.println("number format not match !!!");
	}
	
	
	System.out.println("After");
	}
}