//Object class
//javap java.lang.Object
/*
  protected void finalize() throws java.lang.Throwable;
  public java.lang.String toString();
  public boolean equals(java.lang.Object);
  public native int hashCode();
*/
import java.util.*;
class Employee{
	private Integer employeeId;
	private String name,gender;
	private Double salary;
	
	public Employee(Integer employeeId, String name, String gender, Double salary){
	this.employeeId=employeeId;
	this.name=name;
	this.gender=gender;
	this.salary=salary;
	//System.out.println(this.employeeId+","+this.name+","+this.gender+","+this.salary);
	}
	
	//get and set methods
	
	//toString method
	@Override
	public String toString(){
		return this.employeeId+","+this.name+","+this.gender+","+this.salary;
	}
	
	
	/*//equals method
	@Override
	public boolean equals(Object o){
		//Employee e4=new Employee(1,"A","Male",10000.0);
		if(o instanceof Employee){
		Employee temp=(Employee)o;
		if(this.employeeId.equals(temp.employeeId)&&
		this.name.equals(temp.name)&&
		this.gender.equals(temp.gender)&&
		this.salary.equals(temp.salary))
		return true;
		}
		return false;
	}*/
	
	//hashcode
	@Override
	public int hashCode(){
		int prime = 17;
		prime = prime + prime*this.employeeId.hashCode();
		prime = prime + prime*this.name.hashCode();
		prime = prime + prime*this.gender.hashCode();
		prime = prime + (int)(prime*this.salary.hashCode());
		
		
		return prime;
	}
	
}

class ObjectClassDemo{
	public static void main(String...args){
		
	Employee e1=new Employee(1,"A","Male",10000.0);
	Employee e2=new Employee(2,"B","FeMale",50000.0);
	Employee e3=new Employee(3,"C","Male",20000.0);
	Employee e4=new Employee(1,"A","Male",10000.0);
	System.out.println(e1);
	System.out.println(e2);
	System.out.println(e3);
	System.out.println(e4);
	
	
	/*output:whithout invoking toString
	toString method of object class
	Employee@36baf30c
	Employee@7a81197d
	Employee@5ca881b5
	Employee@24d46ca6
	
	output:with invoking toString
	toString method of overriden method
	1,A,Male,10000.0
	2,B,FeMale,50000.0
	3,C,Male,20000.0
	1,A,Male,10000.0
	*/
	System.out.println("\n");
	
	
	System.out.println(e1==e4);//false

	System.out.println(e1.equals(e4));//
	
	
	String s1=new String("abc");
	String s2=new String("abc");
	System.out.println(s1.hashCode());
	System.out.println(s2.hashCode());
	System.out.println(s1.equals(s2));
	
	
	/*
	output:
	whitout equals method 
	false
	false
	with equals method
	false
	true
	*/
	
	System.out.println(e1.hashCode());
	System.out.println(e4.hashCode());
	
	/*
	without hashCode method
	303563356
	135721597
	with hashCode method
	282901304
	282901304
	*/

	}
}