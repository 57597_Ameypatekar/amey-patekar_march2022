//Static Inner Class
class StaticInnerClass{
	static int x=100;
	static void m1(){
		System.out.println("StaticInnerClass:m1");
	}
	
	static class InnerClass{
		int y=10;
		
		static void m2(){
			System.out.println("InnerClass:m2");
		}
		
		void m3(){
			System.out.println("InnerClass:m3");
		}
	}
}

class StaticInnerClassDemo{
	public static void main(String... args){
		StaticInnerClass.m1();
		
		StaticInnerClass.InnerClass.m2();
		
		StaticInnerClass.InnerClass i= new StaticInnerClass.InnerClass();
		System.out.println(i.y);
		i.m3();
	}
}

/*
StaticInnerClass:m1
InnerClass:m2
10
InnerClass:m3
*/