//lambda expression
//.class file of lambda expression will not generate


interface sum{
	int add(int a, int b, int c);
}
class LambdaExpressionDemo2{
	
	public static void main(String []args){
		//syntax for lambda expression
		// data type not compulsory otherwise give data type for all parameters 
		
		sum s = (d,e,f) ->  (d+e+f) ; 
		
		System.out.println("Sum="+s.add(10,20,30));
		
	}
}



/*
sum=60
*/

