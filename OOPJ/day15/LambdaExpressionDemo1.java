//lambda expression
//.class file of lambda expression will not generate
interface IntF2{
//functional interface has only one interface
	void m1(int a, int b);
}
class LambdaExpressionDemo1{
	
	public static void main(String []args){
		//syntax for lambda expression
		// data type not compulsory otherwise give data type for all parameters 
		IntF2 x= (c,d) -> {
		System.out.println("Lambda m1() Called");
		System.out.println("c="+c);
		System.out.println("d="+d);
		};
				
		x.m1(10,20);
	}
}

/*
Lambda m1() Called
c=10
d=20
*/

