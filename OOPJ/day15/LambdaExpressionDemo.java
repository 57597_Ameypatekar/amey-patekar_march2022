//lambda expression
//.class file of lambda expression will not generate
interface IntF1{
//functional interface has only one interface
	void m1();
}
class LambdaExpressionDemo{
	
	public static void main(String []args){
		//syntax for lambda expression
		IntF1 a =() -> {
			
			System.out.println("Lambda m1() Called");
			
		};
		
		a.m1();//Lambda m1() Called
	}
}

