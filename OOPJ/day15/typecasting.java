class parent{
	void m1(){
		System.out.println("parent m1()");
	}
}

class child extends parent{
	void m1(){
		System.out.println("child m1()");
	}
}

class childchild extends child{
	void m1(){
		System.out.println("childchild m1()");
	}
}

class typecasting {
 public static void main(String []args){
		
		parent p=new childchild();//upcasting
		//p.m1();
		
		c c= (child)p;
		c.m1();
		
		/*child c=(child)p; //downcasting
		
		child cc=(childchild)p;*/
		
		
 }
}
