//calculator using lambda expression
interface MathOperation{
	double calculator(double a,double b);
}

class MathOperaionDemo{
	public static void main(String []args){
	
	MathOperation m=(c,d)->c+d;
	MathOperation m1=(c,d)->c-d;
	MathOperation m2=(c,d)->c*d;
	MathOperation m3=(c,d)->c/d;
	
	System.out.println("Additon = "+m.calculator(20.2,10.25));
	System.out.println("Substraction = "+m1.calculator(20.25,10));
	System.out.println("Multiplication = "+m2.calculator(20.10,10.25));
	System.out.println("Division = "+m3.calculator(20.25,10.54));
	
	
	}
}
/*
Additon = 30.45
Substraction = 10.25
Multiplication = 206.025
Division = 1.9212523719165087
*/