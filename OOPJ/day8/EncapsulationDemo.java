//OOPs concept
//encapsulation

class Employee{
	//data=instance variable
	
	private String name;
	private int age;
	private double salary;
	
	public Employee(String name,int age,double salary){
	this.name=name;
	this.age=age;
	this.salary=salary;
	
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getAge(){
		return this.Age;
	}
	
	public double getSalary(){
		return this.Salary;
	}
	
}

class EncapsulationDemo{
	public static void main(String[] args){
		Employee e1=new Employee("A",22,22000.0);
		System.out.println(e1.getname);
		System.out.println(e1.getAge);
		System.out.println(e1.getSalary);
	}
	
	
}


//read-->get	to read data
//write-->set   to update data