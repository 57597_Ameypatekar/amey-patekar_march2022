class Animal{
	
	void eat(){
		System.out.println("Eat Method of class Animal");
	}
	
	void sleep(){
		System.out.println("Sleep Method of class Animal");
		
	}
}


class Bird extends Animal{

	void eat(){
		System.out.println("Eat Method of class Bird");
	}
	
	void sleep(){
		
		System.out.println("Sleep Method of class Bird");
	}
	
	void fly(){
		System.out.println("Fly Method of class Bird");
		
	}
}

class AnimalDemo{
	public static void main(String []args){
	
		Animal a=new Animal();
		a.eat();
		a.sleep();
		
		Bird b=new Bird();
		b.eat();
		b.sleep();
		b.fly();
	}
}

/*
Eat Method of class Animal
Sleep Method of class Animal
Eat Method of class Bird
Sleep Method of class Bird
Fly Method of class Bird

*/