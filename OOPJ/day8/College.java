import java.util.Scanner;
class Person{
	String name,gender;
	int age;
	
	void setData(){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter name   :");
		name=sc.nextLine();
		System.out.print("Enter gender :");
		gender=sc.nextLine();
		System.out.print("Enter age    :");
		age=sc.nextInt();
		sc.nextLine();
		
	}
	void getData(){
		System.out.println("   Name      :"+name);
		System.out.println("   Gender    :"+gender);
		System.out.println("   Age       :"+age);
	}
}

class Student extends Person{
	
	String id;
	void setData(){
		super.setData();
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter id     :");
		
		id=sc.nextLine();
	}
	void getData(){
		super.getData();
		
		System.out.println("   ID        :"+id);
		System.out.println("---------------------------------------------------");
	}
	
}

class Teacher extends Person{
	double salary;
	String subject;
	void setData(){
		super.setData();
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter salary :");
		salary=sc.nextDouble();
		sc.nextLine();
		System.out.print("Enter subject:");
		subject=sc.nextLine();
		
	}
	void getData(){
		super.getData();
		System.out.println("   Salary    :"+salary);
		System.out.println("   Subject   :"+subject);
		System.out.println("---------------------------------------------------");
	}
}

class CollegeStudent extends Student{
	int year;
	String major;
	void setData(){
		super.setData();
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter year   :");
		year=sc.nextInt();
		System.out.print("Enter major  :");
		sc.nextLine();
		major=sc.nextLine();
	}
	void getData(){
		super.getData();
		System.out.println("   year      :"+year);
		System.out.println("   major     :"+major);
		System.out.println("---------------------------------------------------");
	}
}

class College{
public static void main(String[]args){
	
	int N=1;
	while(N==1)
	{
		Scanner sc=new Scanner(System.in);
	System.out.println("---------------------------------------------------");
	System.out.println("Enter Number for which you want to add information:");
	System.out.print("1.Student 2.Teacher 3.College Student :");
	int n=sc.nextInt();
	
	switch (n)
	{
		case 1:
				Student s=new Student();
				System.out.println("---------------------------------------------------");
				System.out.println("Enter Student Information");
				System.out.println("---------------------------------------------------");
				s.setData();
				System.out.println("---------------------------------------------------");
				System.out.println("Student Information");
				System.out.println("---------------------------------------------------");
				s.getData();
				break;
		case 2:
				Teacher t=new Teacher();
				System.out.println("---------------------------------------------------");
				System.out.println("Enter Teacher Information");
				System.out.println("---------------------------------------------------");
				t.setData();
				System.out.println("---------------------------------------------------");
				System.out.println("Teacher Information");
				System.out.println("---------------------------------------------------");
				t.getData();
				break;
		case 3:
				CollegeStudent cs=new CollegeStudent();
				System.out.println("---------------------------------------------------");
				System.out.println("Enter College Student Information");
				System.out.println("---------------------------------------------------");
				cs.setData();
				System.out.println("---------------------------------------------------");
				System.out.println("College Student Information");
				System.out.println("---------------------------------------------------");
				cs.getData();
				break;
				
		default :
				System.out.println("---------------------------------------------------");
				System.out.println("Entered wrong number");
	}			
				System.out.println("want to continueY/N:1/0");
				 N=sc.nextInt();
	}
}
}


/*
---------------------------------------------------
Enter Number for which you want to add information:
1.Student 2.Teacher 3.College Student :1
---------------------------------------------------
Enter Student Information
---------------------------------------------------
Enter name   :Amey Ramesh Patekar
Enter gender :Male
Enter age    :27
Enter id     :020
---------------------------------------------------
Student Information
---------------------------------------------------
   Name      :Amey Ramesh Patekar
   Gender    :Male
   Age       :27
   ID        :020
---------------------------------------------------
want to continueY/N:1/0
1
---------------------------------------------------
Enter Number for which you want to add information:
1.Student 2.Teacher 3.College Student :2
---------------------------------------------------
Enter Teacher Information
---------------------------------------------------
Enter name   :Shriniwas k
Enter gender :Male
Enter age    :35
Enter salary :50000
Enter subject:Java
---------------------------------------------------
Teacher Information
---------------------------------------------------
   Name      :Shriniwas k
   Gender    :Male
   Age       :35
   Salary    :50000.0
   Subject   :Java
---------------------------------------------------
want to continueY/N:1/0
11
---------------------------------------------------
Enter Number for which you want to add information:
1.Student 2.Teacher 3.College Student :3
---------------------------------------------------
Enter College Student Information
---------------------------------------------------
Enter name   :Amey Patekar
Enter gender :Male
Enter age    :27
Enter year   :2016
Enter major  :Environmental Engineering
---------------------------------------------------
College Student Information
---------------------------------------------------
   Name      :Amey Patekar
   Gender    :Male
   Age       :27
   year      :2016
   major     :Environmental Engineering
---------------------------------------------------
want to continueY/N:1/0
0
*/