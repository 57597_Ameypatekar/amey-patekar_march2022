/*
	 class A
	 class B  
	 class C
*/

class A{
	int p = 10;
	int x = 100;
	void m1(){
		System.out.println("A:m1");
	}
}
class B extends A{
	int p = 20;
	int q = 30;
	void m2(){
		System.out.println("B:m2");
	}
}
class C extends B{
	int p = 40;
	int q = 50;
	int r = 60;
	void m3(){
		System.out.println("C:m3");
	}

}
class MultiLevelInheritanceDemo1{
	public static void main(String []args){
	A a1=new A();
	A a2=new B();
	A a3=new C();
	
	B b1=new B();
	B b2=new C();
	
	C c=new C();
	
	a1.m1();//A:m1
	b1.m2();//B:m2
	b2.m1();//A:m1
	c.m3();//C:m3
	c.m2();//B:m2
	c.m1();//A:m1
	
	}
}

//instace method always resolved on object type
