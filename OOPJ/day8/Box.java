/*
1. Create a class Box that uses a parameterized method to initialize the dimensions of a box.(dimensions are width, height, depth of double type). The class should have a method that can return volume. Obtain an object and print the corresponding volume in main() function.*/
class Box1{
		double w,h,d,vol;
		Box1(double w,double h,double d){
		this.w=w;
		this.h=h;
		this.d=d;
		}
		
		public double volume(){
		
		return this.vol=(this.w*this.h*this.d);
		}
}
class Box{
		public static void main(String []args){
		Box1 b=new Box1(12.15,15.15,20.20);
		double v=b.volume();
		System.out.println("The volume of Box is :"+v);
		}
}

