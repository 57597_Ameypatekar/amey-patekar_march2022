package day8;
import java.util.Scanner;
public class Employee extends Person{

	 double annualSal;
	 int startingYear;
	 String nationalInsuranceNum;
		
	Employee(String name, String v,String d,String s,String c,int a,long m,double annualSal,int startingYear,String nationalInsuranceNum){
		super(name, v,d,s,c, a, m);
		this.annualSal=annualSal;
		this.startingYear=startingYear;
		this.nationalInsuranceNum=nationalInsuranceNum;
		
	}
	

	
	void getData(){
		
		super.getData();
		
		System.out.println("   Annual Salary            :"+annualSal);
		System.out.println("   Starting Year            :"+startingYear);
		System.out.println("   National Insurance Number:"+nationalInsuranceNum);
		System.out.println("*************************************");
		
	}
	
	
	
	
}