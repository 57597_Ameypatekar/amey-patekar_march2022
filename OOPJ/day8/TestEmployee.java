package day8;
import java.util.Scanner;
class TestEmployee{

	public static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your name:");
		String name=sc.nextLine();
		System.out.println("Enter your village:");
		String v=sc.next();
		System.out.println("Enter your District:");
		String d=sc.next();
		System.out.println("Enter your State:");
		String s=sc.next();
		System.out.println("Enter your Country:");
		String c=sc.next();
		System.out.println("Enter your Age:");
		int a=sc.nextInt();
		System.out.println("Enter your Mobile Number:");
		long m=sc.nextLong();
		System.out.println("Enter Annual Salary:");
		double annualSal=sc.nextDouble();
		System.out.println("Enter Starting Year:");
		int startingYear=sc.nextInt();
		System.out.println("Enter National Insurance Number:");
		String nationalInsuranceNum=sc.next();
		
		Employee e=new Employee(name,v,d,s,c,a,m,annualSal,startingYear,nationalInsuranceNum);
		
		e.getData();
	
	}
}

/*
*************************************
   Your Name is             :Amey Patekar
   village                  :Kankavli
   District                 :Sindhudurg
   State                    :Maharashtra
   Country                  :India
   Age                      :27
   Mobile Number            :7083383361
*************************************
   Annual Salary            :900000.0
   Starting Year            :2010
   National Insurance Number:1212s
*************************************
*/