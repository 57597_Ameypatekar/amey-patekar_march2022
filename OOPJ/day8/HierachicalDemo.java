//Hierachical inheritance
/*
	parent A
	 child B  and child C
*/

class A{
	int a=10;
	int b=20;
	
	void m1(){
		System.out.println("Method m1 of Class A");
	}
}

class B extends A{
	int a=100;
	int b=200;
	
	void m2(){
		System.out.println("Method m2 of Class B");
	}
	
	void m1(){
		System.out.println("Method m1 of Class B");
	}
}


class C extends A{
	int a=1000;
	int b=2000;
	int c=3000;
	
	void m3(){
		System.out.println("Method m3 of Class C");
	}
	
	/*void m1(){
		System.out.println("Method m1 of Class C");
	}*/
}


class HierachicalDemo{
	public static void main(String []args){
		
		B b=new B();
		b.m1();//Method m1 of Class B
		
		A a=new A();
		a.m1();//Method m1 of Class A
		
		A a1=new B();
		a1.m1();//Method m1 of Class B
		
		
		//instace variables always resolved on reference type
		System.out.println(b.a);//100
		System.out.println(b.b);//200
		System.out.println(a.a);//10
		System.out.println(a.b);//20
		System.out.println(a1.a);//10
		System.out.println(a1.b);//20
		
		C c=new C();
		//instace method always resolved on object type
		c.m1();//Method m1 of Class A
		System.out.println(c.a);//1000
		System.out.println(c.b);//2000
		System.out.println(c.c);//3000
		
		A a2=new C();
		
		a2.m1();//Method m1 of Class A
		//a2.m3();
		System.out.println(a2.a);//10
		System.out.println(a2.b);//20
		//System.out.println(a2.c);//
		
		//hierachical inheritance
		//C c2=new B(); //CTE
		//B b2=new C(); //CTE
		
	}
}