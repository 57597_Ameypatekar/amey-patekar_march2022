package day8;
class Person{
	String name,village,district,state,country;
	 int age;
	 long mobileNum;
	
	Person(String name, String v,String d,String s,String c,int a,long m){
		this.name=name;
		this.village=v;
		this.district=d;
		this.state=s;
		this.country=c;
		this.age=a;
		this.mobileNum=m;
		
	}
	
	void getData(){
		
		System.out.println("*************************************");
		System.out.println("   Your Name is             :"+name);
		System.out.println("   village                  :"+village);
		System.out.println("   District                 :"+district);
		System.out.println("   State                    :"+state);
		System.out.println("   Country                  :"+country);
		System.out.println("   Age                      :"+age);
		System.out.println("   Mobile Number            :"+mobileNum);
		System.out.println("*************************************");
	}
}

