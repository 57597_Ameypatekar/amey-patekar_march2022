class A{
	
	{
		System.out.println("this is init block of class A");
	}
	
	A(){
	System.out.println("this is constructor of class A");
	}
	
	void printA(){
		System.out.println("this is method of class A");
	}
}

class B extends A{
	{
		System.out.println("this is init block of class B");
	}
	
	B(){
	System.out.println("this is constructor of class B");
	}
	
	void printB(){
		System.out.println("this is method of class B");
	}
}

class C extends A{
	{
		System.out.println("this is init block of class C");
	}
	
	C(){
	System.out.println("this is constructor of class C");
	}
	
	void printC(){
		System.out.println("this is method of class C");
	}
}

class InheritanceDemo2{
	public static void main(String []args){
	
	//A a =new A();//this is init block of class A
                 //this is constructor of class A
	//a.printA();  //this is method of class A
	//B b =new B();
	//C c =new C();
	
	//A b1 =new B();
	/*this is init block of class A
this is constructor of class A
this is init block of class B
this is constructor of class B*/

	//A c= new C();
	/*this is init block of class A
this is constructor of class A
this is init block of class C
this is constructor of class C*/
	
	
	C c1=new C();
	//constructor call super() by defualt 
	/*this is init block of class A
this is constructor of class A
this is init block of class C
this is constructor of class C*/
	
	}
}