//single inheritance only instace variables and methods are inherited
//compiler->reference type
class Parent{
	
	int a=10;
	int b=30;
	void printData(){
			System.out.println(" "+a);
			System.out.println(" "+b);
		}
	void m1(){
		System.out.println("Parent::m1()");
	}
	
}

class Child extends Parent{
	int a=100;
		void printParentData(){
			System.out.println(a);
			System.out.println(b);
			
		}
		
		void m1(){
		System.out.println("child::m1()");
	}
		
		void m2(){
		System.out.println("child::m2()");
	}

}

class SingleInheritanceDemo{
	public static void main(String[] args){
		Child c = new Child();
		c.printParentData();
		
		c.printData();
		c.m2();
		c.m1();
		System.out.println(c.a);
		
		Parent p=new Parent();
		
		System.out.println(p.a);
		
		
		Parent p1=new Child();//  upcasting (child to parent)
		//compiler always check reference type
		p1.m1();
		//p1.m2();  compile error because p1 will not find m2 in Parent
		System.out.println(p1.a);// in inheritance variables are always resolve on reference type
		
		
	
	}
}