class A{
	int p = 10;
	int x = 100;
}
class B extends A{
	int p = 20;
	int q = 30;
	
}
class C extends B{
	int p = 40;
	int q = 50;
	int r = 60;

}
class MultiLevelInheritanceDemo{
	public static void main(String []args){
	A a1=new A();
	A a2=new B();
	A a3=new C();
	
	B b1=new B();
	B b2=new C();
	
	C c=new C();
	
	/*not valid downcasting
	B b=new A();
	C c=new B();
	C c1=new A();*/
	
	System.out.println(a1.p);
	//System.out.println(a1.q);
	//System.out.println(a1.r);
	
	System.out.println(a2.p);
	//System.out.println(a2.q);
	//System.out.println(a2.r);
	
	
	System.out.println(a3.p);
	//System.out.println(a3.q);
	//System.out.println(a3.r);
	
	System.out.println(c.p);
	System.out.println(c.q);
	System.out.println(c.r);
	System.out.println(c.x);
	
	}
}

//instace variables always resolved on reference type
