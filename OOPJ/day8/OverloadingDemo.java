class Test{
	public void m1(int i){
	System.out.println("int-arg");
	}
	public void m1(float f){
	System.out.println("float-arg");
	}
	
	public void m1(String s){
	System.out.println("String");
	}
	
	public void m1(Object o){
	System.out.println("object");
	}
	
	public void m1(StringBuffer sb){
	System.out.println("StringBuffer");
	}
	
}
class OverloadingDemo{
public static void main(String []args){

	Test t=new Test();
	t.m1(10);//int-arg
	t.m1(10.5f);//float-arg
	t.m1('a');//automatic promotion will check by compiler int-arg
	t.m1(10l);//float-arg
	//t.m1(10.5);//CE double is not match
	
	t.m1(new Object());//Object
	t.m1("amey");//String
	//t.m1(null);//String 
	
	t.m1(null);//CE String and StringBuffer ambuigous
	t.m1(new StringBuffer("amey"));//StringBuffer
	
}
}