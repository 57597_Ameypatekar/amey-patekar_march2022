import java.util.Scanner;
class ArrayDemo{
	int a[][] = new int [3][3];
	int b[][] = new int [3][3];
	int result[][] = new int [3][3];
	int table[][]=new int[10][30];
	int max,min;
	void ScanArray(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Array Elements For 1st Matrix :");
		for (int i=0;i<a.length;i++)
		{
			for (int j=0;j<a.length;j++)
			{
				a[i][j]=sc.nextInt();
			}
			System.out.println();
		}
		
		System.out.println();
		System.out.println();
		
		System.out.println("Enter Array Elements For 2nd Matrix :");
		for (int i=0;i<a.length;i++)
		{
			for (int j=0;j<a.length;j++)
			{
				b[i][j]=sc.nextInt();
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}
	void DisplayArray(){
		System.out.println("Array Elements of 1st Matrix :");
		for (int i=0;i<a.length;i++)
		{
			for (int j=0;j<a.length;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		
		System.out.println("Array Elements of 2nd Matrix :");
		for (int i=0;i<a.length;i++)
		{
			for (int j=0;j<a.length;j++)
			{
				System.out.print(b[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	void ResultArray(){
		
		for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				System.out.print(result[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	void AddArray(){
		for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				result[i][j]=a[i][j]+b[i][j];
			}
			
		}
		System.out.println("Addition of Two Matrices :");
		ResultArray();
	}
	
	void SubArray(){
		for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				result[i][j]=a[i][j]-b[i][j];
			}
			
		}
		System.out.println("Substraction of Two Matrices :");
		ResultArray();
	}
	
	void MulArray(){
		for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				result[i][j]=a[i][j]*b[j][i];
			}
			
		}
		System.out.println("Multiplication of Two Matrices :");
		ResultArray();
	}
		
	void TransposeArray(){
		{
			for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				result[i][j]=a[j][i];
			}
		}
		System.out.println("Transpose of 1st Matrix :");
		ResultArray();
		}
		{
		for (int i=0;i<result.length;i++)
		{
			for (int j=0;j<result.length;j++)
			{
				result[i][j]=b[j][i];
			}
		}
		System.out.println("Transpose of 2nd Matrix :");
		ResultArray();
		}
	}
	
	void Table1to30(){
		System.out.println("Printing Tables of 1 to 30 in Array :");
		for (int i=0,n=1;i<table.length;i++,n++)
		{
			for (int j=0,k=1;j<30;j++,k++)
			{
				table[i][j]=n*k;
				System.out.print(String.format("%3d",table[i][j])+" ");
			}
			System.out.println();
			
		}
	}
	
	void MaxArray(){
		int i=0,j=0;
		max=a[i][j];
			for ( i=0;i<a.length;i++)
		{
			for ( j=0;j<a.length;j++)
			{
				
				if(a[i][j]>max)
				{
					max=a[i][j];
				}
			}
			
		}
		System.out.println("Greatest Element in 1st Array is :"+max);
		
		/*max=b[i][j];
		for ( i=0;i<b.length;i++)
		{
			for ( j=0;j<b.length;j++)
			{
				
				if(b[i][j]>max)
				{
					max=b[i][j];
				}
			}
			
		}*/
		System.out.println("Greatest Element in 2nd Array is :"+max);
	}
	public static void main(String []args){
		
		ArrayDemo A = new ArrayDemo();
		
		A.ScanArray();
		A.DisplayArray();
		A.AddArray();
		A.SubArray();
		A.MulArray();
		A.TransposeArray();
		A.Table1to30();
		A.MaxArray();
	
	}
}