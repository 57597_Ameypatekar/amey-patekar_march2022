//Final Variables
class FinalVar{
	static int i;
	int a;
	int b;
}


class FinalVarDemo{
	public static void main(String []args){
		
		/*final int i = 10;  //Compile time constant
		final int j;
		//j = 20;
		int a = 10;
		switch(a){
			case 1:break;
			case i:break;
			case 20:break;
			case j:break;
		}*/
		
		//i= 30;  cannot assign value to final variable i
		
		final FinalVar fv1 = new FinalVar();
		FinalVar fv2 = new FinalVar();
		//fv1=fv2;  will give error fv1 will not allow reinitialize
		//fv1=null;  will give error fv1 will not allow reinitialize
		
		
		fv1.a=10;
		fv1.b=20;
		//only path is final but values can be modify
		System.out.println(fv1.a);
		System.out.println(fv1.b);
		
	}
}

//local , static, instance