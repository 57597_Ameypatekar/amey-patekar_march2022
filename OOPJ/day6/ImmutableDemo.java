//Immutable object 
//create object such that its content will not modify
class Immutable{
	final int a;
	final int b;
	
	Immutable(int a,int b){
		this.a=a;
		
		this.b=b;
	}
}

class ImmutableDemo{

	public static void main(String[] args){
		final Immutable I=new Immutable(10,20);
		//I.a=30;
		//I.b=40;
		//I=null;//not allowed
		
		
		Immutable J=new Immutable(10,20);
		J=null;// allowed
		//J.a=100;//not allowed because a and b are final instace variables
		//J.b=200;//not allowed
	}
}