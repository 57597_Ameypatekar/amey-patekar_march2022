//Interface
//one class can extends one class only 
//but one class can implements many interfaces
//we cannot create object of interface class

//by default interface is abstract
//by default interface methods are public and abstract
//by default interface variables are public, static and final
interface A
{
	void m1();
	
	int a=10;  //public static final
}


class B implements A{
	 @Override
	  public void m1()//public 
	 
	 {
	 System.out.println("m1");
	 }
	 
	
}



class InterfaceDemo{
	public static void main(String [] args){
		B b=new B();
		b.m1();
		
		
	}
}