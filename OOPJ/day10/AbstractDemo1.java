abstract class A{
	abstract void m1();
	abstract void m2();
	abstract void m3();
	abstract void m4();
}


abstract class B extends A{
	 void m1(){
	 System.out.println("m1");
	 }
	 
	 void m2(){
	 System.out.println("m2");
	 }
}

abstract class C extends B{
	void m3(){
	 System.out.println("m3");
	 }
}

class D extends C{
	void m4(){
	 System.out.println("m4");
	 }
}

class AbstractDemo1{
	public static void main(String [] args){
		A d=new D();
		d.m1();
		d.m2();
		d.m3();
		d.m4();
		
		
	}
}