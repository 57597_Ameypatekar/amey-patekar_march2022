abstract class Vehicle{
	abstract void noOfWheels();
	
	abstract void engineCapacity();
}

abstract class TwoWheeler extends Vehicle{
	@Override
	void noOfWheels(){
		System.out.println("2 Wheels");
	}
	
	
}

abstract class FourWheeler extends Vehicle{
	@Override
	void noOfWheels(){
		System.out.println("4 Wheels");
	}
	
}

class Activa125 extends TwoWheeler{
	@Override
	void engineCapacity(){
		System.out.println("Activa:125cc");
	}
}

class I20 extends FourWheeler//concrete class must include unimplemented methods of parent abstract class
{
	@Override
	void engineCapacity(){
		System.out.println("I20:1200cc");
	}
}

class AbstractDemo
{
	public static void main(String [] args)
	{
		//you cannot create object of abstract class
		//but abstract classes do have constructors
				
		
		TwoWheeler a=new Activa125();
		a.noOfWheels();//2 Wheels
		a.engineCapacity();//Activa:125cc
		
		
		Activa125 a1=new Activa125();
		a1.noOfWheels();//2 Wheels
		a1.engineCapacity();//Activa:125cc
		
		
		FourWheeler f=new I20();
		f.noOfWheels();//4 Wheels
		f.engineCapacity();//I20:1200cc
		
		I20 i=new I20();
		i.noOfWheels();//4 Wheels
		i.engineCapacity();//I20:1200cc
	}
}