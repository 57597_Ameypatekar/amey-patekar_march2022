//super
class Parent{
	int a=10;
	void m1(int a,int b){
		System.out.println("Parent:m1");
	}
}

class Child extends Parent{
	int b=20;
	void m1(int c,int d){
		super.m1(a,b);//call parent
		System.out.println("Child:m1");
	}
}

class SuperDemo{
	public static void main(String []args){
	
	Parent p2=new Child();
		
	p2.m1(12,12);
	/*
	Parent:m1
	Child:m1
	*/
}
}
