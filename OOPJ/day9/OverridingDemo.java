/*                      less restricted
private>default>protected>public*/
/*method name must be same
number of arguments and its order must be same
data type must be same
return type must be same
@Override annotate is use to check method is overriden or not 
*/

class A{
		void m1(){
			System.out.println("A:m1()");
		}
		public void m2(int x,double y){
			System.out.println("A:m2()");
		}
		
		private void m3(int x){
			System.out.println("A:m3()");
		}
}
class B extends A{
		
		void m1(){
			System.out.println("B:m1()");
		}
		public void m2(int a,double b){
			System.out.println("B:m2()");
		}
		@Override //if not written compiler will compile
		private void m3(int x)//method does not override or implement a method from a supertype
		{
			System.out.println("A:m3()");
		}
}
class OverridingDemo{
	public static void main(String []args){
		A a=new B();
		a.m1();//B:m1()
		a.m2(10,20.0);//B:m1()
		//a.m1(11);  private in class A CE
		
	}
}