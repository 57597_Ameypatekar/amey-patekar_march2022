//foreach loop
class Employee{}
class ForEachLoopDemo{


public static void main(String []args)
	{
		int a[]=new int[10];
		
		for(int i=0;i<a.length;i++)
		{
			System.out.println(a[i]);
		}
		System.out.println();
		for(int i: a){
			System.out.println(i);  // values of array gets copied in i;
		}
		
		Employee earr[]=new Employee[5];
		
		//for-each loop
		for(Employee e:earr)
		{
			System.out.println(e);
		}
		
		//for-each loop for 2d array
		int b[][]={
					{10,20},
					{1,2},
					{11,22}
				   };
		
		for(int i[]:b){
			for(int j:i){
				System.out.print(j+" ");
			}
			System.out.println();
		}
	}
}