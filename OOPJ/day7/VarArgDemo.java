//Variable Argument
class VarArgDemo{
	//void add (int a,int b)
	//static void add(int a,int... a)
	
	
	static void add(int... a) // a is array in variable  argument
	{
		int result=0;
		for(int i=0;i<a.length;i++){
			result=result+a[i];
		}
		System.out.println(result);
	}
	
	public static void main(String []args)
	{
	
	VarArgDemo.add();
	VarArgDemo.add(10);
	VarArgDemo.add(10,12,2);
	VarArgDemo.add(11,25,1,2,5,3,5);
	
	}
}