class A{
	static int a=100;
	int b=150;
	static void m1(){
	System.out.println("m1");
	System.out.println(b);
	}
	void m2(){
	System.out.println("m2");
	}
}

class B{
	static int c=200;
	int d=250;
	static void m3(){
	System.out.println("m3");
	}
	void m4(){
	System.out.println("m4");
	}
}

class demo4{

public static void main(String [] args){
	
	// static variables and static methods
	System.out.println(A.a);
	A.m1();
	System.out.println(B.c);
	B.m3();
	
	
	//instance variables and instance method
	A i1=new A();
	B i2=new B();
	System.out.println(i1.b);
	i1.m2();
	System.out.println(i2.d);
	i2.m4();
	
	i1.m1();// -->jvm will treat like A.m1() 
	
}
}