// Static variable static block 
class StaticBlock{
	static byte b;
	static short s;
	static int i;
	static long l;
	static float f;
	static double d;
	static char c;
	static boolean bool;
	
	static{
			b=20;
			System.out.println("Demo:SB7");
			s=1;
			i=2;
			l=3;
			
			f=0.1f;
			d=0.2d;
			
			
	}
}

class A{
	static int p=10;
	static{
		System.out.println("Demo:A.SB8");
	}
}

class Demo{
	static{
		System.out.println("Demo:SB1");
	}
	
	static{
		System.out.println("Demo:SB2");
	}
	
	static{
		System.out.println("Demo:SB3");
	}
	
	static{
		System.out.println("Demo:SB4");
		System.out.println(StaticBlock.i);
		System.out.println("Demo:SB5");
	}
	
	static{
		System.out.println("Demo:SB6");
		System.out.println(A.p);
	}
	
	

	public static void main(String []args){
	
		System.out.println("byte default value   :"+StaticBlock.b);
		System.out.println("short default value  :"+StaticBlock.s);
		System.out.println("int default value    :"+StaticBlock.i);
		System.out.println("long default value   :"+StaticBlock.l);
		System.out.println("float default value  :"+StaticBlock.f);
		System.out.println("double default value :"+StaticBlock.d);
		System.out.println("char default value   :"+StaticBlock.c);
		System.out.println("boolean default value:"+StaticBlock.bool);
		StaticBlock.b=10;
		System.out.println("byte default value   :"+StaticBlock.b);
	}
}