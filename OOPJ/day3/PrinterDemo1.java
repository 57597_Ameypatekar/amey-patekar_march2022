//Reference data type/Objects
class PrinterDemo1{

		
	//instace variables and instace methods are part of the object
	int p;
	int q;
	
		void m1(){
		
		System.out.println("Demo:m1");
		System.out.println(p);
		System.out.println(q);
		p=1;
		q=2;
		}
	
	
	public static void main(String [] args){ 
		
		PrinterDemo1 d1 = new PrinterDemo1();
		PrinterDemo1 d2 = new PrinterDemo1(); //p=0  q=0
		PrinterDemo1 d3 = new PrinterDemo1();
		
		
		System.out.println(d1.p);
		System.out.println(d1.q);
		
		System.out.println(d2.p);
		System.out.println(d2.q);
		
		d1.p=100;
		d1.q=200;
		
		System.out.println(d1.p);
		System.out.println(d1.q);
		
		d2.p=300;
		d2.q=400;
		
		System.out.println(d2.p);
		System.out.println(d2.q);
		
		System.out.println(d1.p);
		System.out.println(d1.q);
		
		//d1.m1();
		d2.m1();
		
		d3.m1();
		System.out.println(d3.p);
		System.out.println(d3.q);
		
		System.out.println(d1.p);
		System.out.println(d1.q);
		
	}
	
}