//Reference data type/Objects
class Printer{
	static int i = 10;    //static variable are part of the class gets memory when loads class
	int a = 200;   //instace variables are part of the object
	int b = 100;   // instace variable gets memory when object created
	
	static void m1()//static method
	{
		System.out.println("m1");
	}
    
	
	void print(){
		
		System.out.println("Printing");
		
	}
}



class PrinterDemo{

	static int x;// first gets memory to static 
	static{}
	
	//instace variables and instace methods are part of the object
	int p;
	int q;
	
		static void print1(){
		
		System.out.println("Printing1");
		}
	
	
	public static void main(String [] args){ //after that main will execute
		
		
		int i=10;
		
		/*creating object of class Printer
		ClassName varName = new ClassName();
		LHS(Reference)        RHS(object)
		*/
		Printer p = new Printer();
		
		PrinterDemo d = new PrinterDemo();
		System.out.println(d.p);
		System.out.println(d.q);
		
		System.out.println(p.a);
		System.out.println(p.b);
		
		p.print();
		p.m1();
		
		d.print1();
		//Static method can call directly without object
		print1();
		PrinterDemo.print1();
		
		
	}
	
}