import java.util.HashSet;
class HashSetDemo{
	public static void main(String []args){
	HashSet<Integer> set = new HashSet<>();
	
	//set.add(0,10);//set never works on index position
	set.add(10);
	set.add(10);
	set.add(20);
	set.add(30);
	set.add(20);
	System.out.println(set.size());//3
	System.out.println();
	
	System.out.println(set);//[20, 10, 30] not following ordered
	
	set.remove(10);
	System.out.println(set);//[20, 30]
	
	System.out.println(set.contains(10));//false
	System.out.println(set.contains(20));//true
	
	
	
	}
}