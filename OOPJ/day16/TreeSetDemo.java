import java.util.TreeSet;
import java.util.SortedSet;
class TreeSetDemo{
	public static void main(String []args){
	TreeSet<Integer> set = new TreeSet<>();
	
	//set.add(0,10);//set never works on index position
	set.add(20);
	set.add(10);
	set.add(40);
	set.add(10);
	set.add(10);
	set.add(30);
	set.add(12);
	System.out.println(set.size());//5
	System.out.println();
	
	System.out.println(set);//[10, 12, 20, 30, 40] sorted
	
	System.out.println(set.descendingSet());//[40, 30, 20, 12, 10] descendingSet
	System.out.println(set);
	
	set.remove(10);
	System.out.println(set);//[12, 20, 30, 40]
	
	System.out.println(set.contains(10));//false
	System.out.println(set.contains(20));//true
	
	
	//<-----Bagged Collections---->
	//[12, 20, 30, 40]
	SortedSet<Integer> set1 =set.tailSet(30);
	System.out.println(set);
	System.out.println(set1);//[30, 40]
	set.add(35);
	
	System.out.println(set);//[12, 20, 30, 35, 40]
	System.out.println(set1);//[30, 35, 40]
	
	set.add(15);
	System.out.println(set);//[12, 15, 20, 30, 35, 40]
	System.out.println(set1);//[30, 35, 40]
	
	//set1.add(15);
	/*
	Exception in thread "main" java.lang.IllegalArgumentException: key out of range
        at java.base/java.util.TreeMap$NavigableSubMap.put(TreeMap.java:1795)
        at java.base/java.util.TreeSet.add(TreeSet.java:255)
        at TreeSetDemo.main(TreeSetDemo.java:41)
	*/
	
	
	SortedSet<Integer> set2 =set.headSet(30);
	System.out.println(set2);//[12, 15, 20]
	
	SortedSet<Integer> set3 =set.subSet(15,35);//[15, 20, 30]
	System.out.println(set3);//[15, 20, 30]
	
	set.add(null);
	/*Exception in thread "main" java.lang.NullPointerException
        at java.base/java.util.Objects.requireNonNull(Objects.java:208)
        at java.base/java.util.TreeMap.put(TreeMap.java:809)
        at java.base/java.util.TreeMap.put(TreeMap.java:534)
        at java.base/java.util.TreeSet.add(TreeSet.java:255)
        at TreeSetDemo.main(TreeSetDemo.java:59)*/
	}
}