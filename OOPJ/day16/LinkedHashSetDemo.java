import java.util.LinkedHashSet;
class LinkedHashSetDemo{
	public static void main(String []args){
	LinkedHashSet<Integer> set = new LinkedHashSet<>();
	
	//set.add(0,10);//set never works on index position
	set.add(10);
	set.add(10);
	set.add(20);
	set.add(30);
	set.add(20);
	System.out.println(set.size());//3
	System.out.println();
	
	System.out.println(set);//[10, 20, 30] following ordered
	
	set.remove(10);
	System.out.println(set);//[20, 30]
	
	System.out.println(set.contains(10));//false
	System.out.println(set.contains(20));//true
	
	
	
	}
}