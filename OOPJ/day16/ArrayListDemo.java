//collection
//list
import java.util.ArrayList;
class ArrayListDemo{
	public static void main(String []args){
		
		ArrayList list = new ArrayList();//non-generic collection
		list.add(10);//
		list.add(20);
		list.add(0,5);
		list.add("Abc");
		list.add(new Double("20.0"));
		System.out.println(list.size());
		System.out.println(list.get(0));
		System.out.println(list.get(1));
		System.out.println(list.get(2));
		System.out.println(list.get(3));
		System.out.println(list.get(4));
		
	}
}
