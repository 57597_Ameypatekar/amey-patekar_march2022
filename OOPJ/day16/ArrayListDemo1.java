//collection
//list
import java.util.ArrayList;
class ArrayListDemo1{
	public static void main(String []args){
		
		ArrayList<Integer> list = new ArrayList<>();//generic collection
		ArrayList<String> list1 = new ArrayList<String>();
		list.add(10);//
		list.add(20);
		list.add(0,5);
		//list.add(4,5);//error
		
		//list.remove(0);
		
		System.out.println(list.size());
		System.out.println(list.get(0));
		System.out.println(list.get(1));
		System.out.println(list.get(2));
		System.out.println(list);//[5, 10, 20]
		list.remove(0);
		list.remove(new Integer(20));//remove by object
		System.out.println();
		System.out.println(list);//[10]
		list.add(30);
		list.add(40);
		list.add(50);
		list.add(60);
		
		
		System.out.println(list);//[10, 30, 40, 50, 60]
		
		boolean contains=list.contains(10);
		contains=list.contains(new Integer(10));
		System.out.println(contains);//true
		
		System.out.println();
		list.set(0,100);
		System.out.println(list);//[100, 30, 40, 50, 60]
		
		System.out.println();
		for(int i=0;i<list.size();i++)
		{
			System.out.print(list.get(i)+" ");//100 30 40 50 60
		}	
		
		
		System.out.println();
		System.out.println();
		//enhance for loop
		for(Integer i : list){
			System.out.print(i+" ");//100 30 40 50 60
		}
		
		
		
		System.out.println();	
		list1.add("TEAM 2");
		list1.add("Amey");
		list1.add("CDAC");
		list1.add("MUMBAI");
		list1.add(4,"JUHU");
		System.out.println(list1.size());//5
		
		System.out.println(list1);//[TEAM 2, Amey, CDAC, MUMBAI, JUHU]
		
		list1.remove(0);
		list1.remove("MUMBAI");
		System.out.println(list1);//[Amey, CDAC, JUHU]
		
		System.out.println();	
		list1.add("Amey");
		System.out.println(list1.indexOf("Amey"));//0
		System.out.println(list1.lastIndexOf("Amey"));//3
		System.out.println(list1.contains("CDAC"));	//true
	}
}
