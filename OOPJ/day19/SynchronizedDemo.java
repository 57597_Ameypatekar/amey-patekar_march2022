class Sync{

		synchronized void m1(){
		System.out.println(Thread.currentThread().getName()+" m1 started");
		try{
			Thread.sleep(10000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m1 ended");
		}
		
		synchronized void m2(){
		System.out.println(Thread.currentThread().getName()+" m2 started");
		try{
			Thread.sleep(10000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m2 ended");
		}
		
		void m3(){
		System.out.println(Thread.currentThread().getName()+" m3 started");
		try{
			Thread.sleep(10000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m3 ended");
		}
		
		static synchronized void m4(){
		System.out.println(Thread.currentThread().getName()+" m4 started");
		try{
			Thread.sleep(6000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m4 ended");
		}
		
		static synchronized void m5(){
		System.out.println(Thread.currentThread().getName()+" m4 started");
		try{
			Thread.sleep(6000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m4 ended");
		}
		
		static void m6(){
		System.out.println(Thread.currentThread().getName()+" m4 started");
		try{
			Thread.sleep(6000);
		}//running state
		catch(InterruptedException e){}
		System.out.println(Thread.currentThread().getName()+" m4 ended");
		}
}

class SynchronizedDemo{
	public static void main(String[]args){
		Sync s1=new Sync();
		Sync s2=new Sync();
		
		
		Thread t1=new Thread("T1"){
			public void run(){
				//s1.m1();//lock is applied on s1 beacause m1 method is synchronized
				Sync.m4();
			}
		};//new
		
		Thread t2=new Thread("T2"){
			public void run(){
				//s1.m1();//scenario 1
				//s1.m1();//scenario 2
				//s1.m2();
				//s2.m1();//scenario 3
				//s1.m3();//scenario 4 : m3 is not synchronized method
				//Sync.m4();//static scenario 1 class load then creates object of class Class
				//Sync.m5();//static scenario 2
				//Sync.m6();//static scenario 3: m6 is not synchronized
			}
		};//new state
		
		
		t1.start();
		
		try{
			Thread.sleep(20);
		}//running state
		catch(InterruptedException e){}
		
		t2.start();
		
		
		
		
	}
}