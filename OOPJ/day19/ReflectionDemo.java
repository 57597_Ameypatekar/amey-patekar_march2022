import java.lang.reflect.*;
class A{
	int x;
	int z;
	private int y;
	
	static{
		System.out.println("static block invoked");
	}
	
	A(){
		System.out.println("constructor invoked");
	}
	
	void m1(){
		System.out.println("m1() invoked");
	}
	
	private void m2(){
		System.out.println("m2() invoked");
	}
	
	private void m3(int a){
		System.out.println("m3() invoked");
	}
}

class ReflectionDemo{
	public static void main(String []args)throws ClassNotFoundException,
	InstantiationException,IllegalAccessException,InvocationTargetException,NoSuchMethodException
	{
		
		Class aClass = Class.forName("A");//class A get load return Class<A> object
		Field[] fields=aClass.getDeclaredFields();
		
		for(Field f: fields)
		{
			System.out.println(f.getName());//x y z
		}
		
		System.out.println();
		
		Method[] methods=aClass.getDeclaredMethods();
		for(Method m:methods)
		{
			System.out.println(m.getName());//m2 m1 m3
		}
		
		System.out.println();
		
		Constructor[] constructors=aClass.getDeclaredConstructors();
		for(Constructor c:constructors)
		{
			System.out.println(c.getName());//A
		}
		
		A a=(A) aClass.newInstance();//create new object without using new keyword
		/*output
		static block invoked
		constructor invoked
		*/
		System.out.println();
		System.out.println();
		
		Method m1=aClass.getDeclaredMethod("m1");
		m1.invoke(a);//m1() invoked
		
		Method m2=aClass.getDeclaredMethod("m2");
		m2.setAccessible(true);//for private access 
		m2.invoke(a);//m2() invoked
		
		//A a=new A();
		//a.m1();
		//a.m2();//CE
		//a.m3(10);//CE
		
		
		
	}
}