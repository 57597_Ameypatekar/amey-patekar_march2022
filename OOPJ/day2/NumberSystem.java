//1. Assign decimal, octal, hexadecimal values to variables and print.
//2. Assign unicode value to char variable and print.
class NumberSystem
{
	public static void main(String []args)
	{
		int a=017;//0-7
		int b=0x1f;//0-9 a-f
		int d=28;//0-9   
		char c='\u0061';  // \u0000   \uffff
		System.out.println(a);
		System.out.println(b);
		System.out.println(d);
		System.out.println(c);
	}

}