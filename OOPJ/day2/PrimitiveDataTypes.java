//4. WAP to declare static variables of all primitive data types and print
//their default value.
class PrimitiveDataTypes
{
	static byte b;
	static short s;
	static int i;
	static long l;
	static float f;
	static double d;
	static char c;
	static boolean bool;
		
	public static void main(String []args)
	{
		System.out.println("byte default value   :"+b);
		System.out.println("short default value  :"+s);
		System.out.println("int default value    :"+i);
		System.out.println("long default value   :"+l);
		System.out.println("float default value  :"+f);
		System.out.println("double default value :"+d);
		System.out.println("char default value   :"+c);
		System.out.println("boolean default value:"+bool);
	}
}