/*8. WAP to print * patterns using for loop and while loop. (Right angle
triangle, Equilateral triangle, K pattern, X pattern*/
class Pattern{

		static void RightAngleTriangle(){
			for(int i=1;i<=10;i++)
			{
					for(int s=1;s<=10;s++)
					{
						System.out.print(" ");
					}
					for (int j=1;j<=i;j++)
					{
							System.out.print("* ");
					}
					System.out.println();
			}
		}
		
		static void EquilateralTriangle(){
			for(int i=1;i<=10;i++)
			{	
					for(int s=1;s<=6;s++)
					{
						System.out.print(" ");
					}
					for(int s=1;s<=10-i;s++)
					{
						System.out.print(" ");
					}
					for (int j=1;j<=i;j++)
					{
							System.out.print("* ");
					}
					System.out.println();
			}
		}
		
		static void KPattern(){
				
			for(int i=6;i>0;i--)
			{	
					for(int s=1;s<=6;s++)
					{
						System.out.print(" ");
					}
					for (int j=1;j<=i;j++)
					{
						    if(j==1 || j==i)
							System.out.print("* ");
							else
								System.out.print("  ");
					}
					System.out.println();
			}
			
			
			for(int i=2;i<=6;i++)
			{	
					for(int s=1;s<=6;s++)
					{
						System.out.print(" ");
					}
					for (int j=1;j<=i;j++)
					{
							if(j==1 || j==i)
							System.out.print("* ");
							else
								System.out.print("  ");
					}
					System.out.println();
			}
		}
		
		static void XPattern(){
		for(int i=1;i<=15;i++)  
		{  
			for(int s=1;s<=6;s++)
					{
						System.out.print(" ");
					}
			for(int j=1;j<=15;j++)  
			{  
				if(i==j || j==(15-i+1))  
					{  
					System.out.print("*");  
					}  
				else  
					System.out.print(" ");  
			}  
    
    System.out.println();  
    }  
			
			
			
			
		}
		
		public static void main(String []args){
			System.out.println(" RightAngleTriangle");
			RightAngleTriangle();
			
			
			
			System.out.println();
			System.out.println();
			System.out.println(" EquilateralTriangle");
			
			
			EquilateralTriangle();
			
			
			System.out.println();
			System.out.println();
			System.out.println(" KPattern");
			KPattern();
			
			System.out.println();
			System.out.println();
			System.out.println(" XPattern");
			XPattern();
		}
}
