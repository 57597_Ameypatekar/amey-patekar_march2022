//3. WAP to access/invoke Static variable and static method.
class AccessStatic
{
	static int a=10;
	static int b=20;
	
	static void display()
	{
		System.out.println("Displaying Static method");
		
	}
	
	public static void main(String []args)
	{
		System.out.println(a);
		System.out.println(b);
		display();
		
	}

}