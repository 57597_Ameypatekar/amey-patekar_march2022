/*5. WAP  to print tables of 1-30 using 2 for loops and also try to print
using single for loop.
*/
class MultTable
{
	public static void main (String [] args)
	{
		for (int i=1,j=1;i<=10;i++)
		{
			
			System.out.print(String.format("%2d",(j))+"  X "+String.format("%2d",(i))+" = "+String.format("%3d",(i*j))+"  ");
			
			
			if (i==10)
			{
				j++;
				i=0;
				System.out.println();
				
				if(j==31)
				{
					break;
				}
			}
		}
	}
}