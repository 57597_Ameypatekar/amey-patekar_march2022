//static method
class A{
	public static void m1(){}
	public static void m3(){}
}


class B extends A{
	public static void m1(){} //no error at this line
	
	@Override//cannot override static method
	public static void m3(){}
}

class StaticMethodDemo{
	
	public static void main(String []args){}
}

/*
StaticMethodDemo.java:8: error: static methods cannot be annotated with @Override
        @Override//cannot override static method
        ^
1 error
*/