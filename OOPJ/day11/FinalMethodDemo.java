//final method
final class A{
	final void m1(){}
}


class B extends A{
	void m1(){}
}

class FinalMethodDemo{
	
public static void main(String []args){}
}

/*
FinalMethodDemo.java:7: error: cannot inherit from final A
class B extends A{
                ^
FinalMethodDemo.java:8: error: m1() in B cannot override m1() in A
        void m1(){}
             ^
  overridden method is final
2 errors
*/