//final abstract method
abstract class A{
	final void m1();//you cannot override final methods
}


class B extends A{
	void m1(){}
}

class FinalAbstractMethodDemo{
	
public static void main(String []args){}
}

/*
FinalAbstractMethodDemo.java:3: error: illegal combination of modifiers: abstract and final
        final abstract void m1();
                            ^
FinalAbstractMethodDemo.java:8: error: m1() in B cannot override m1() in A
        void m1(){}
             ^
  overridden method is final
2 errors
*/