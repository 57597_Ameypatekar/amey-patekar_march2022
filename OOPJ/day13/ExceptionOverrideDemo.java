import java.io.*;
class A{
	void m1()throws FileNotFoundException{
	
	}
}
class B extends A{
	@Override
	//void m1()throws ClassNotFoundException{
	void m1(){
	//void m1()throws FileNotFoundException{ //no error
		
		
		
	}
}
class ExceptionOverrideDemo{
 public static void main(String []args) {
	A a=new A();
	//a.m1();
	
	
 }
}


/*
output for line no.9
ExceptionOverrideDemo.java:9: error: m1() in B cannot override m1() in A
        void m1()throws ClassNotFoundException{
             ^
  overridden method does not throw ClassNotFoundException
1 error
*/

/*

*/