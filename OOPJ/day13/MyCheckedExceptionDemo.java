import java.io.*;
class MyCheckedException extends Exception{
	MyCheckedException(String errorMessage){
		super(errorMessage);
	}
}

class MyUncheckedException extends RuntimeException{
	
}

class MyCheckedExceptionDemo{
	
	static void m1()throws MyCheckedException
	{
		
	}
	
	static void m2()throws MyUncheckedException
	{
		
	}
	public static void main(String []args)throws MyCheckedException{
		
		m1();
		m2();
		
	}
}