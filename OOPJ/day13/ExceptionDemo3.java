//try with multicatch
import java.io.*;
class ExceptionDemo3{
		
	public static void main(String []args){
		
		
		try{
			int a[]=new int[5];
			a[6]=10;
	}catch(ArrayIndexOutOfBoundsException | StringIndexOutOfBoundsException | NullPointerException e){
		//cannot declare parent in multicatch eg.IndexOutOfBoundsException will give error in this case	
			{
				
				System.out.println("1");
			}
		
	
		}
	}
}	
/*

*/