class MyUncheckedException extends RuntimeException{
	MyUncheckedException(String errorMessage){
		super(errorMessage);
	}
}

class MyUncheckedExceptionDemo{
	
	public static void main(String []args){
		try{
			int i=10/0;
			
		}catch(ArithmeticException e){
			throw new MyUncheckedException("Divide by zero is nifinity. Please dont do this.");
			
		}
	}
}