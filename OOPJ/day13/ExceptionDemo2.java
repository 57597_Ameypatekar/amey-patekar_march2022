import java.io.*;
class ExceptionDemo2{
	static void m1() throws FileNotFoundException{
	
	}
	static int m2(){
	try{
		
		int i=1/0;
		return 10;
	}catch(ArithmeticException e){
		return 20;
	}
	finally{
		return 30;
	}
	//return 40; // ExceptionDemo2.java:15: error: unreachable statement
	}
	
	
	public static void main(String []args){
		
		System.out.println(m2());
	}
}

/*
will return finally
output -30
*/


