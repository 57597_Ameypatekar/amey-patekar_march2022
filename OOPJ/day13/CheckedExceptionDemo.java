//Checked Exception
//checked Exceptions will not propogate automatically
import java.io.*;



class CheckedExceptionDemo{
	public static void main(String [] args){
		File f = new File("abc.txt");
		FileReader fr = new FileReader(f);
	}
}

/*
CheckedExceptionDemo.java:10: error: unreported exception FileNotFoundException; must be caught or declared to be thrown
                FileReader fr=new FileReader(f);
                              ^
1 error
*/