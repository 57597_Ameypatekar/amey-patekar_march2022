//try-catch approach

import java.io.*;

class CheckedExceptionDemo1{
	public static void main(String [] args){
		File f = new File("abc.txt");
		
		try{
			FileReader fr = new FileReader(f);
		}catch(FileNotFoundException e){}
	}
}