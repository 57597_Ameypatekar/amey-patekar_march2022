//trows approach
//throws keyword is written at method level
import java.io.*;
	
class CheckedExceptionDemo3{
	static void m()throws FileNotFoundException{
		System.out.println("Before");
		File f = new File("abc.txt");
		//File f = new File("abcd.txt");
		FileReader fr = new FileReader(f);//throw new FileNotFoundException();
		System.out.println("After");
		
	}
public static void main(String [] args)throws FileNotFoundException {
		m();
		System.out.println("main");
	}
}

/*
Before
After
main
*/

/*
output for line no.9
Before
Exception in thread "main" java.io.FileNotFoundException: abcd.txt (The system cannot find the file specified)
        at java.base/java.io.FileInputStream.open0(Native Method)
        at java.base/java.io.FileInputStream.open(FileInputStream.java:216)
        at java.base/java.io.FileInputStream.<init>(FileInputStream.java:157)
        at java.base/java.io.FileReader.<init>(FileReader.java:75)
        at CheckedExceptionDemo3.m(CheckedExceptionDemo3.java:10)
        at CheckedExceptionDemo3.main(CheckedExceptionDemo3.java:15)*/