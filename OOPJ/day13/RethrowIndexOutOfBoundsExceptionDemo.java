//rethrow
class RethrowIndexOutOfBoundsExceptionDemo{
	public static void main(String [] args){
	System.out.println("Before");
	int a[]=new int[5];
	String str="abcd";
	try{
			try{
			//a[6]=10;//throw new ArrayIndexOutOfBoundsException();
			char c=str.charAt(6);
			System.out.println("this line will not print");
		}catch(ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("1");
			throw e;//rethrow   throw ref var
		}catch(StringIndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("2 ");
			throw e;//rethrow 
		}catch(IndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("3");
		}finally{
			System.out.println("finally...!!!");
		}
	
		System.out.println("After");//this line will not print
		
	}catch(StringIndexOutOfBoundsException e){
		
		System.out.println("Rethrow catch");
	}
	}
	
	
}