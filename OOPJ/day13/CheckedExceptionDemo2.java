//trows approach
//throws keyword is written at method level
import java.io.*;

class CheckedExceptionDemo2{

public static void main(String [] args) throws FileNotFoundException{
		System.out.println("Before");
		File f = new File("abc.txt");
			
		FileReader fr = new FileReader(f);//throw new FileNotFoundException();
		System.out.println("After");
		
	}
}