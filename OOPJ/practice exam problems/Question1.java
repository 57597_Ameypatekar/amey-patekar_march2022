/*
1. WAP with Parent class as Shape and derive a class Circle, Rectangle considering
area() as function which can be overloaded and overrided.
*/
import java.util.Scanner;
class Shape{
	void area(){
		System.out.println("Class Shape");
		
	}
}

class Circle extends Shape{
	void area(double d){
		
		double area=0;
		area = 3.14*d*d;
		System.out.println("Area of Circle :"+area);
		
	}
	
}

class Rectangle extends Shape{
	@Override
	void area(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter length of Rectangle");
		double l=sc.nextDouble();
		System.out.println("Enter Breadth of Rectangle");
		double b=sc.nextDouble();
		double area=0;
		area=l*b;
		System.out.println("Area of Rectangle :"+area);
	}
	
}

class Question1{

	public static void main(String []args){
		Circle c=new Circle();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter radius of circle");
		double r=sc.nextDouble();
		c.area(r);
		
		Rectangle R=new Rectangle();
		R.area();
	}
}
