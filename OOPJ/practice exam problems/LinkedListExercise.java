//LinkedList Exercise
import java.util.*;
import java.util.Collections;
class LinkedListExercise{
	public static void main(String []agrs){
		LinkedList<String> list=new LinkedList<>();
		LinkedList<String> list1=new LinkedList<>();
		list.add("IRON-MAN");
		list.add("SPIDER-MAN");
		list.add("BAT-MAN");
		list.add("SUPER-MAN");
		list.add("ANT-MAN");
		
		list1.add("IRON-MAN");
		list1.add("SPIDER-MAN");
		list1.add("BAT-MAN");
		list1.add("SUPER-MAN");
		list1.add("ANT-MAN");
		
		//11-compare
		Collections.sort(list);
		Collections.sort(list1);
		System.out.println(list.equals(list1));
		
		
		System.out.println(list);
		//1
		Iterator x=list.listIterator(2);
		while(x.hasNext())
		{
			System.out.println(x.next());
		}
		//2
		Collections.reverse(list);
		System.out.println(list);
		//3
		list.set(2,"AVENGERS");
		System.out.println(list);
		//4
		list.addFirst("INFINITY-WARS");
		list.addLast("DR.STRANGE");
		System.out.println(list);
		//5
		list.add(0,"HULK");
		System.out.println(list);
		
		//6
		list.add(3,"BLACK WIDOW");
		System.out.println(list);
		
		//7
		System.out.println(list.indexOf("HULK"));
		System.out.println(list.lastIndexOf("DR.STRANGE"));
		
		System.out.println(list.getFirst());
		System.out.println(list.getLast());
		
		
		//8
		System.out.println(list.removeFirst());
		System.out.println(list.removeLast());
		System.out.println(list);
		
		//swap
		Collections.swap(list,2,5);
		System.out.println(list);
		
		//join
		LinkedList<String> joinlist=new LinkedList<>();
		joinlist.addAll(list);
		joinlist.addAll(list1);
		System.out.println(joinlist);
		
		//9
		boolean c=list.contains("HULK");
		System.out.println(c);
		
		//10
		List<String> a=new ArrayList<>(list);
		System.out.println(a);
		
		//11
		Collections.sort(list);
		Collections.sort(list1);
		System.out.println(list.equals(list1));
		
		//12
		list1.clear();
		System.out.println(list.isEmpty());
		System.out.println(list1.isEmpty());
		
		//13
		System.out.println(list);
		list.set(2,"IRON-MAN2");
		System.out.println(list);
		
	}
}