/*
3. WAP to implement an example that represent all in built exceptions
a)Arithmetic
b)Number Format
c)Array Index out of Bound
d)etc
*/

class A{}

class B extends A{}

class Question3{
	public static void main(String []args){
		//1.Arithmetic Exception
		try{
			int i=10/0;
			}catch(ArithmeticException e){
			System.out.println("Arithmetic Exception");
		}
		
		
		//2.Number Format exception
		try{
			String s="aaa";
			int i=Integer.parseInt(s);
		}catch(NumberFormatException e){
			System.out.println("Number Format Exception");
		}
		
		
		//3.ArrayIndexOutOfBoundsException
		try{
			int a[]=new int[5];
			a[5]=10;
		}catch(ArrayIndexOutOfBoundsException e){
			System.out.println("Array Index Out Of Bounds Exception");
		}
		
		//4.StringIndexOutOfBoundsException
		try{
			String str="Amey Patekar";
			str.charAt(15);
		}catch(StringIndexOutOfBoundsException e){
			System.out.println("String Index Out Of Bounds Exception");
		}
		
		//5.NullPointer Exception
		try{
		String s=null;  
		System.out.println(s.length());
		}catch(NullPointerException e){
			System.out.println("Null Pointer Exception 1");
		}
		
		//6.NullPointerException
		try{
		int arr[]=null;
		System.out.println(arr.length);
		}catch(NullPointerException e){
			System.out.println("NullPointer Exception 2");
		}
		
		//7.ClassCastException
		try{
			A a=new A();
			B b=(B)a;
		}catch(ClassCastException e){
			System.out.println("Class Cast Exception");
		}
		
		
	}

}