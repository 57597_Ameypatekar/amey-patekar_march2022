/*
4. WAP to implement user defined exception when
I )Incorrect password entered
ii)number not found in a list , is not a palindrome
*/
import java.util.Scanner;
class IncorrectPasswordException extends Exception{
	public IncorrectPasswordException(String str){
		super(str);
	}
}
class Question4{
	public static void main(String[]args){
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Set your Password :");
		String str1=sc.nextLine();
		String str2=null;
		System.out.print("Enter Password :");
		try{
		str2=sc.nextLine();
		if(str1.equals(str2))
		{
			System.out.print("correct password");
		}
		else{
			throw new IncorrectPasswordException("Incorrect password entered");
		}
		}catch(IncorrectPasswordException e){
			
			System.out.println(" "+e);
		}
		
	}
}