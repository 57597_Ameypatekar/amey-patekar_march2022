import java.util.*;
class HashSetExercise{
	public static void main(String []args){
	HashSet<String> set=new HashSet<>();
	set.add("104");
	set.add("101");
	set.add("102");
	set.add("103");
	
	set.add("AMEY");
	set.add("AKSHAY");
	set.add("AKASH");
	set.add("AJAY");
	System.out.println("Size of HashSet : "+set.size());
	System.out.println(set);
	System.out.println();
	
	//Array
	int n=set.size();
	String a[]=new String[n];
	int i=0;
	for(String s:set)
	{
		a[i++]=s;
	}
	for(String str:a)
	{
		System.out.println(str);
	}
	System.out.println();
	
	//Arraylist
	ArrayList<String> arr=new ArrayList<>(set);
	System.out.println(arr);
	System.out.println();
	//LinkedList
	LinkedList<String> set1=new LinkedList<>(set);
	System.out.println(set1);
	System.out.println();
	//TreeSet
	TreeSet<String> tset=new TreeSet<>(set);
	System.out.println(tset);
	System.out.println();
	//remove all elements
	set.clear();
	System.out.println(set);
	
	
	}
}