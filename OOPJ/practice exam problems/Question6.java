/*
6. WAP to implement one thread that print random number and second thread that
print fibo series.
*/
import java.lang.Math;
class Question6{
	public static void main(String []args){
		
		System.out.println(Thread.currentThread().getName()+" started");
		
		Thread t1=new Thread ("Thread 1"){
			public void run(){
				
				System.out.println("Random number :"+Math.random());
				
			}
		};
		
		Thread t2=new Thread ("Thread 2"){
			public void run(){
				int n1=0,n2=1,n3,num=10;
				System.out.print("Fibonacci Number series is : ");
				System.out.print(n1+" "+n2);
				for(int i=0;i<num;i++)
				{
					n3=n1+n2;
					System.out.print(" "+n3);
					n1=n2;
					n2=n3;
				}
			
			}
		};
		
		t1.start();
		try{
			t1.join();
			}catch(InterruptedException e){}
		t2.start();
	}
}