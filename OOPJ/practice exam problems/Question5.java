/*5. WAP to implement Multithreading where one thread is printing Hello and other
is printing CDAC Mumbai*/
class Question5 {
	public static void main(String []args){
		
		System.out.println(Thread.currentThread().getName()+" started");
		
		Thread t1=new Thread ("Thread 1"){
			public void run(){
				
				System.out.println("Hello");
				
			}
		};
		
		Thread t2=new Thread ("Thread 2"){
			public void run(){
			
				System.out.println("CDAC Mumbai");
			
			}
		};
		
		t1.start();
		t2.start();
		
	}
}