import java.util.*;
class Sort implements Comparator<Person>{
	public int compare(Person p,Person p1){
		System.out.println("compare of class sort invoked...!!!");
		return -p.name.compareTo(p1.name);
		
		
	}
}

class Person implements Comparable<Person>{
	String name,gender,city,state,PAN,address;
	Integer age,pincode;
	String aadhar;
	
	Person(String name,String gender,String city,String state,String PAN,String address, Integer age,Integer pincode,String aadhar){
		this.name=name;
		this.gender=gender;
		this.city=city;
		this.state=state;
		this.PAN=PAN;
		this.address=address;
		this.age=age;
		this.pincode=pincode;
		this.aadhar=aadhar;
	}
	
	//compareTo method
	@Override
	public int compareTo(Person p){
		System.out.println("compareTo invoked...!!!");
		return this.name.compareTo(p.name);
		
	}
	
	//toString method
	@Override
	public String toString(){
		System.out.println("toString invoked...!!!");
		return this.name+" , "+this.gender+" , "+this.city+" , "+this.state+" , "+this.PAN+" , "+this.address+" , "+this.age+" , "+this.pincode+" , "+this.aadhar+" \n ";
	}
	
	//equals method
	@Override
	public boolean equals(Object o){
		System.out.println("equals invoked...!!!");
		if(o instanceof Person)
		{
			Person p=(Person)o;
			if((this.name.equals(p.name)) &&
				(this.gender.equals(p.gender)) &&
				(this.city.equals(p.city)) &&
				(this.state.equals(p.state)) &&
				(this.PAN.equals(p.PAN)) &&
				(this.address.equals(p.address)) &&
				(this.age.equals(p.age)) &&
				(this.pincode.equals(p.pincode)) &&
				(this.aadhar.equals(p.aadhar))) 
				{
					return true;
				}
			else{
				return false;
			}
			
		}
		return false;
	}
	
	//hashCode method
	@Override
	public int hashCode(){
		System.out.println("hashCode invoked...!!!");
		int num=0;
		num=num+this.name.hashCode();
		num=num+this.gender.hashCode();
		num=num+this.city.hashCode();
		num=num+this.state.hashCode();
		num=num+this.PAN.hashCode();
		num=num+this.address.hashCode();
		num=num+this.age.hashCode();
		num=num+this.pincode.hashCode();
		num=num+this.aadhar.hashCode();
		return num;
	}
}
class PersonDemo{
	public static void main(String []args){
		
		Person p1=new Person("Amey","Male","Kankavli","Maharashtra","CVSPP0000C","abc",27,416602,"111111112222");
		Person p2=new Person("Akshay","Male","Jalgaon","Maharashtra","CVSPP1111C","abc",23,410204,"222211112222");
		Person p3=new Person("Ajay","Male","Khopoli","Maharashtra","CVSPP2222C","abc",24,410203,"333311112222");
		Person p4=new Person("Akash","Male","Satara","Maharashtra","CVSPP3333C","abc",24,416013,"444411112222");
		Person p5=new Person("Amey","Male","Kankavli","Maharashtra","CVSPP4444C","abc",26,416602,"555511112222");
		Person p6=new Person("Amey","Male","Kankavli","Maharashtra","CVSPP0000C","abc",27,416602,"111111112222");
		
		ArrayList<Person> a=new ArrayList<>();
		a.add(p1);
		a.add(p2);
		a.add(p3);
		a.add(p4);
		a.add(p5);
		a.add(p6);
		
		System.out.println(a.size());
		System.out.println(a);
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p6));
		System.out.println(p1.hashCode());
		System.out.println(p6.hashCode());
		
		
		Collections.sort(a);
		System.out.println(a);
		System.out.println();
		System.out.println();
		System.out.println();
		
		Sort s=new Sort();
		
		Collections.sort(a,s);//list,class object
		System.out.println(a);
		
		/*HashSet<Person> h=new HashSet<>(a);
		System.out.println(h);
		
		TreeSet<Person> t=new TreeSet<>(a);
		System.out.println(t);
		*/
		
	}
}