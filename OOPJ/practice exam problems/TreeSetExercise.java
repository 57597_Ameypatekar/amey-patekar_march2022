import java.util.*;
class TreeSetExercise{
	public static void main(String []args){
	TreeSet<String> set=new TreeSet<>();
	set.add("Mango");
	set.add("Apple");
	set.add("Banana");
	set.add("Strawberry");
	set.add("Strawberry");
	
	System.out.println("Size of TreeSet : "+set.size());
	System.out.println(set);
	System.out.println();
	
	//iterate elements
	for(String s:set)
	{
		System.out.print(s+"  ");
	}
	System.out.println();
	System.out.println();

	
	//add all elements
	TreeSet<String> set1=new TreeSet<>();
	set1.addAll(set);
	System.out.println(set1);
	System.out.println();
	
	
	System.out.println(set.descendingSet());
	System.out.println();
	
	TreeSet<Integer> setnum=new TreeSet<>();	
	setnum.add(1);
	setnum.add(2);
	setnum.add(3);
	setnum.add(4);
	setnum.add(7);
	setnum.add(8);
	setnum.add(9);
	setnum.add(6);
	System.out.println(setnum.size());
	System.out.println(setnum);
	System.out.println();
	//numbers less than 7
	System.out.println(setnum.headSet(7));
	
	
	}
}