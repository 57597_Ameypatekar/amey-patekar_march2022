/*2. WAP to represent a multiple inheritance by calculating salary of employee where
there is a class employee which implements interface Attendance, interface
salarydetail.*/

import java.util.Scanner;
interface Attendance{
	void Attendance(String n,double sal,double days);
}

interface Salarydetail{
	void Salarydetail();
	
}
class Employee implements Attendance, Salarydetail{
	double salary,numOfDay,totalSal;
	String name;
	@Override
	public void Attendance(String n,double sal,double days){
		this.salary=sal;
		this.numOfDay=days;
		this.name=n;
		System.out.println("Employee "+name+" is present for "+days+" days");
	}
	
	@Override
	public void Salarydetail(){
		totalSal=salary*numOfDay;
		System.out.println("Employee "+name+"'s Total Salary is "+totalSal);
	}
}

class Question2{
	public static void main(String []args){
		Employee e=new Employee();
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter your name : ");
		String n=sc.nextLine();
		System.out.println("Enter your salary per day");
		double sal=sc.nextDouble();
		System.out.println("Enter number of days you are present");
		double days=sc.nextDouble();
		
		e.Attendance(n,sal,days);
		
		e.Salarydetail();
		
	
	}
}