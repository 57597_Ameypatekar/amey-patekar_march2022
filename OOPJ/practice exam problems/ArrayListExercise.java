//ArrayList Exercise
import java.util.ArrayList;
import java.util.Collections;
class ArrayListExercise{
	public static void main(String []agrs){
		ArrayList<String> list=new ArrayList<>();
		
		list.add("IRON-MAN");
		list.add("SPIDER-MAN");
		list.add("BAT-MAN");
		list.add("SUPER-MAN");
		list.add("ANT-MAN");
		System.out.println(list);
		//1
		list.add(1,"AVENGERS");
		System.out.println(list);
		//2
		System.out.println(list.get(2));
		//3
		list.set(3,"INFINITY-WARS");
		System.out.println(list);
		//4
		list.remove(2);
		System.out.println(list);
		//5
		boolean contains=list.contains("ANT-MAN");
		System.out.println(contains);//true
		//6
		Collections.sort(list);
		System.out.println(list);
		//7
		Collections.reverse(list);
		System.out.println(list);
		//8
		System.out.println(list.indexOf("ANT-MAN"));
		//9
		list.clear();
		System.out.println(list);
		
	}
}