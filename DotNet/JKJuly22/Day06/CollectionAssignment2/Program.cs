﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionAssignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] emp = new Employee[3];
            emp[0] = new Employee(101, "Amey", 50000);
            emp[1] = new Employee(102, "Ajay", 55000);
            emp[2] = new Employee(103, "Akshay", 60000);

            List<Employee> empList = new List<Employee>();
            empList.AddRange(emp);

            foreach (Employee item in empList)
            {
                Console.WriteLine("Employee Number : " + item.Empno + "\n" +
                                  "Employee Name   : " + item.Name + "\n" +
                                  "Employee Salary : " + item.Sal + "\n");
            }

            
            Console.ReadLine();
        }
    }

    class Employee
    {
        private int empno;
        private string name;
        private decimal sal;
        public Employee(int Empno, string Name, decimal Sal)
        {
            this.Empno = Empno;
            this.Name = Name;
            this.Sal = Sal;
        }

        public int Empno
        {
            set { empno = value; }
            get { return empno; }
        }

        public string Name
        {
            set
            {
                if (value != "")
                    name = value;
                else
                    Console.WriteLine("no blank name is allowed");
            }
            get { return name; }
        }

        public decimal Sal
        {
            set
            {
                if (value > 0)
                    sal = value;
                else
                    Console.WriteLine("sal must be greater than zero");
            }
            get { return sal; }
        }
    }
}
