﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace CollectionAssignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, Employee> obj = new Dictionary<int, Employee>();
            int i = 1;
            string s = "yes";
            while (s=="yes")
            {
                int empno = 0;
                string name="";
                decimal sal = 0;
                Console.Write("Add Employee Number : ");
                empno = int.Parse(Console.ReadLine());
                Console.Write("Add Employee Name : ");
                name = Console.ReadLine();
                Console.Write("Add Employee Salary : ");
                sal = decimal.Parse(Console.ReadLine());
                obj.Add(i, new Employee(empno, name,sal));
                Console.Write("Do you want to continue(yes/no) :");
                s = Console.ReadLine();
                i++;
            }

            Console.WriteLine("---------------------------------------------------");

            foreach (KeyValuePair<int, Employee> item in obj)
            {
                Console.WriteLine("Employee Number: "+item.Value.Empno+"\n"+ "Employee Name: " + item.Value.Name+"\n"+ "Employee Salary: " + item.Value.Sal+"\n");
            }

            Console.ReadLine();
        }
    }

    class Employee
    {
        private int empno;
        private string name;
        private decimal sal;
        public Employee(int Empno,string Name,decimal Sal)
        {
            this.Empno = Empno;
            this.Name = Name;
            this.Sal = Sal;
        }

        public int Empno
        {
            set { empno = value; }
            get { return empno; }
        }

        public string Name
        {
            set {
                if (value != "")
                    name = value;
                else
                    Console.WriteLine("no blank name is allowed");
            }
            get { return name; }
        }

        public decimal Sal
        {
            set {
                if (value > 0)
                    sal = value;
                else
                    Console.WriteLine("sal must be greater than zero");
            }
            get { return sal; }
        }
    }
}
