﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Program
    {
        //step 1 : create a delegate class having the same signature as the funcion to call
        public delegate void del();
        public delegate int addDel(int a, int b);
        static void Main1(string[] args)
        {
            //step 2: create an object of the delegate class, passing it function name as a parameter
            del objDel = new del(Display);
            objDel();


            objDel = new del(show);
            objDel();

            Console.ReadLine();
        }

        static void Main2()
        {
            del objDel = Display;

            objDel += show;
            objDel -= show;

            objDel();
            Console.ReadLine();
        }

        static void Main3()
        {
            del objDel = (del)Delegate.Combine(new del(Display), new del(show));


            objDel = (del)Delegate.Combine(objDel, new del(show));

            objDel = (del)Delegate.Remove(objDel, new del(show));
            objDel = (del)Delegate.RemoveAll(objDel, new del(Display));
            objDel();
            Console.ReadLine();
        }

        static void Main4()
        {
            addDel objAddDel = Add;
            int ans;
            ans = objAddDel(10, 20);
            Console.WriteLine("Addition : " + ans);

            Console.ReadLine();
        }

        static void Main()
        {
            del obj = Class1.Display;
            obj();

            Class1 oclass = new Class1();
            obj = oclass.Show;
            obj();

            Console.ReadLine();
        }

        static void Display()
        {
            Console.WriteLine("display");
        }

        static void show()
        {
            Console.WriteLine("show");
        }

        static int Add(int a, int b)
        {
            return a + b;
        }

    }

    public class Class1
    {
        public static void Display()
        {
            Console.WriteLine("display");
        }
        public void Show()
        {
            Console.WriteLine("show");
        }
    }
}
