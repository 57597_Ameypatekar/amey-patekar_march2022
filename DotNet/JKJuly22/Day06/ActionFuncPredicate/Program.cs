﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActionFuncPredicate
{
    class Program
    {
        static void Main1(string[] args)
        {
            Action obj = Display;
            obj();

            Action<string> obj1 = Display;
            obj1("Amey");

            Action<string, int> obj2 = Display;
            obj2("Amey", 28);

            Console.ReadLine();
        }

        static void Main()
        {
            Func<int, int, int> obj = Add;
            Console.WriteLine(obj(20, 30));

            Func<string, decimal, int> obj2 = DoSomething;
            Console.WriteLine(obj2("Amey",25.2m));

            Func<int, bool> obj3 = IsEven;
            Console.WriteLine(obj3(10));
            Console.WriteLine(obj3(11));

            Predicate<int> o4 = IsEven;
            Console.WriteLine(o4(10));

            Console.ReadLine();
        }

        static void Display()
        {
            Console.WriteLine("display");
        }
        static void Display(string s)
        {
            Console.WriteLine("display : " + s);
        }
        static void Display(string s, int i)
        {
            Console.WriteLine("display " + s +" "+ i);
        }
        static int Add(int a, int b)
        {
            return a + b;
        }
        static int DoSomething(string a, decimal b)
        {
            return a.Length;
        }
        static bool IsEven(int a)
        {
            if (a % 2 == 0)
                return true;
            else
                return false;
        }
    }
}
