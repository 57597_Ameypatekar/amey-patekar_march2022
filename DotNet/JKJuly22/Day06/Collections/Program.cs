﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Collections
{
    class Program
    {
        static void Main1(string[] args)
        {
            object o1;
            int i = 100;
            o1 = i;
            int j;
            j = (int)o1;

            ArrayList a = new ArrayList();
            a.Add(10);

            Hashtable ht = new Hashtable();
            ht.Add(1, "Amey");
            ht.Add(2, "Akshay");
            ht.Add(3, "Ajay");
            ht.Add(4, "Akash");
            ht.Add(5, "Ajinkya");

            SortedList o = new SortedList();

            o.Add(1, "Amey");
            //o.Add(1, "Amey");
            o.Add(2, "Akshay");
            o.Add(3, "Ajay");
            o.Add(4, "Akash");
            o.Add(5, "Ajinkya");

            o[1] = "Amey patekar";
            o[3] = "Ajay yadav";

            o.Remove(2);
            o.RemoveAt(0);
            //bool ans= o.Contains(5);
            bool ans = o.ContainsKey(5);
            Console.WriteLine(ans);
            

            bool ans1 = o.ContainsValue("Ajinkya");
            Console.WriteLine(ans1);

            Console.WriteLine(o.GetByIndex(2));
            Console.WriteLine(o.GetKey(2));
            IList olist = o.GetValueList();
            IList olist1 = o.GetKeyList();

            o.IndexOfKey(1);
            o.IndexOfValue("Ajinkya");
            //o.Keys;
            //o.Values;
            o.SetByIndex(0, "Amey");

            foreach (DictionaryEntry item in o)
            {
                Console.WriteLine(item.Key+" : "+item.Value);
            }

            Console.ReadLine();
        }

        static void Main2(string[] args)
        {
            Stack s = new Stack();
            s.Push("Amey");
            s.Push("Ajay");
            s.Push("Akash");

            Console.WriteLine(s.Pop());
            Console.WriteLine(s.Peek());

            Queue q = new Queue();
            q.Enqueue("Amey");
            q.Enqueue("Ajay");
            q.Enqueue("Ajinkya");

            Console.WriteLine(q.Peek());
            Console.WriteLine(q.Dequeue());


            Console.ReadLine();
        }

        static void Main3(string[] args)
        {
            List<long> list = new List<long>();
            //List<int> list = new List<int>();
            list.Add(10);
            list.Add(20);
            list.Add(30);
            list.Add(30);
            list.Add(30);
            list.Add(30);
            list.Remove(20);
            Console.WriteLine(list.Count());

            foreach (object item in list)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        static void Main4(string[] args)
        {
            SortedList<string, int> slist = new SortedList<string, int>();
            slist.Add("pincode", 416602);
            slist.Add("RollNumber", 101);
           
            Console.WriteLine(slist.Count);
            slist.Remove("pincode");
            foreach (KeyValuePair<string, int> item in slist)
            {
                Console.WriteLine(item.Key+"  "+item.Value);
            }

            Console.ReadLine();
        }

        static void Main5(string[] args)
        {
            Stack<string> s = new Stack<string>();
            s.Push("Amey");
            s.Push("Ajay");
            s.Push("Akash");

            Console.WriteLine(s.Pop());
            Console.WriteLine(s.Peek());

            Queue<int> q = new Queue<int>();
            q.Enqueue(10);
            q.Enqueue(20);
            q.Enqueue(30);

            Console.WriteLine(q.Peek());
            Console.WriteLine(q.Dequeue());

            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            List<Employee> empList = new List<Employee>();
            empList.Add(new Employee { Empno = 1, Name = "Amey" });
            empList.Add(new Employee { Empno = 2, Name = "Akash" });
            empList.Add(new Employee { Empno = 3, Name = "Ajinkya" });
            empList.Add(new Employee { Empno = 4, Name = "Akshay" });
            empList.Add(new Employee { Empno = 5, Name = "Ajay" });

            foreach (Employee item in empList)
            {
                Console.WriteLine(item.Empno+" "+item.Name);
            }

            Console.ReadLine();
        }
    }

    public class Employee
    {
        public int Empno { get; set; }
        public string Name { get; set; }
    }
}
