﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonMethodsAndLambdas1
{
    class Program
    {
        static void Main1(string[] args)
        {
            int x = 100;
            Action obj = delegate ()
            {
                Console.WriteLine(x);
                Console.WriteLine("Anonymous method called");
            };

            obj();

            Func<int, int, int> obj1 = delegate (int a, int b)
              {
                  return a + b;
              };
            Console.WriteLine(obj1(10, 30));


            Predicate<int> obj2 = delegate (int a)
              {
                  if (a % 2 == 0)
                      return true;
                  else
                      return false;
              };
            Console.WriteLine(obj2(11));
            Console.WriteLine(obj2(12));
            Console.ReadLine();
        }
    }
}

namespace AnonMethodsAndLambdas2
{
    class Program
    {
        static void Main(string[] args)
        {

            Func<int, int> obj = MakeDouble;
           

            Func<int, int> obj1 = a =>  a*2 ;
            Console.WriteLine(obj1(10));

            Func<int, int, int> obj2 = (i, j) => i - j ;
            Console.WriteLine(obj2(30,20));

            Action obj3 = () => Console.WriteLine("hello Lambda");
            obj3();

            Predicate<int> obj4 = a => a % 2 == 0;
            
            Console.WriteLine(obj4(20));
            Console.WriteLine(obj4(21));

            Console.ReadLine();
        }

        static int MakeDouble(int a)
        {
            return a * 2;
        }
    }
}