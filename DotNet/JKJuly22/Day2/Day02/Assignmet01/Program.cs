﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignmet01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Employee o1 = new Employee(1, "Amol", 123465, 10);
            //Employee o2 = new Employee(1, "Amol", 123465);
            //Employee o3 = new Employee(1, "Amol");
            //Employee o4 = new Employee(1);
            Employee o1 = new Employee(0,"",5000,0);
            //Employee o1 = new Employee(0, "", 5000);
            //Employee o1 = new Employee(0, "");
            //Employee o1 = new Employee(0);
            //Employee o1 = new Employee();

            decimal netSal=o1.getNetSalary();

            if (o1.EmpNo > 0 && o1.Name != "" && o1.DeptNo > 0 && o1.Basic >= 10000 && o1.Basic < 100000)
                Console.WriteLine("Employee Number       : " + o1.EmpNo + "\n" +
                                  "Employee Name         : " + o1.Name + "\n" +
                                  "Employee Basic        : " + o1.Basic + "\n" +
                                  "Employee Dept Number  : " + o1.DeptNo + "\n" +
                                  "Employee Net Salary   : " + netSal);
            else
                Console.WriteLine("\nEmployee properties are wrongly entered\n");
        }
    }

    class Employee
    {
        private string name;
        private int empNo;
        private decimal basic;
        private short deptNo;

        private decimal netSalary;
        private decimal da;
        private decimal hra;
         

        public Employee(int EmpNo=1, string Name= "Amol", decimal Basic=10000,short DeptNo=10)
        {
            this.EmpNo = EmpNo;
            this.Name = Name;
            this.Basic = Basic;
            this.DeptNo = DeptNo;
        }

        public decimal getNetSalary()
        {
            da = (100 * basic) / 100;
            hra = (120 * basic) / 100;
            netSalary = basic + da + hra;
            return netSalary;
        }


        public string Name
        {
            set
            {
                if (value != "")
                    name = value;
                else
                    Console.WriteLine("Blank names should not be allowed");
            }
            get
            {
                return name;
            }
        }

        public int EmpNo
        {
            set
            {
                if (value > 0)
                    empNo = value;
                else
                    Console.WriteLine("Employee Number Must be greater than zero");
            }
            get
            {
                return empNo;
            }
        }

        public decimal Basic
        {
            set
            {
                if (value >= 10000 && value < 100000)
                    basic = value;
                else
                    Console.WriteLine("Basic must be in range of 10k to 100k");
            }
            get
            {
                return basic;
            }
        }

        public short DeptNo
        {
            set
            {
                if (value > 0)
                    deptNo = value;
                else
                    Console.WriteLine("Department Number Must be greater than zero");
            }

            get
            {
                return deptNo;
            }
        }


    }
}
