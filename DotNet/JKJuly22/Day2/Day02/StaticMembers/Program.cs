﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticMembers
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 obj = new Class1();
            Console.WriteLine(obj.i);
            Console.WriteLine(Class1.static_i);
            Class1.static_i = 2000;
            obj.Display();

            Class1.static_Display();

            Class1.P2 = 100;
            Console.WriteLine(Class1.P2);

            //Console.BackgroundColor = ConsoleColor.DarkMagenta;
        }

        static void Main()
        {
            // Class2 obj = new Class2();

            Class2.I = 10;
            Class2.J = 11;
            Console.WriteLine(Class2.I + "\n" + Class2.J);
            Class2.Display();
        }
    }

    public class Class1
    {
        public int i;
        public static int static_i;
        static Class1()
        {
            Console.WriteLine("Static Constructor is Called");
            static_i = 1000;
        }

        public void Display()
        {
            Console.WriteLine(i);
            Console.WriteLine(static_i);
            Console.WriteLine("Display");
        }

        public static void static_Display()
        {
            //Console.WriteLine(i);
            Console.WriteLine(static_i);
            Console.WriteLine("Display");
        }

        private static int p2;
        public static int P2
        {
            set { p2 = value; }
            get { return p2; }
        }
    }

    public static class Class2
    {
        private static int i;
        private static int j;

        public static int I
        {
            set
            {
                i = value;
            }
            get
            {
                return i;
            }
        }

        public static int J
        {
            set
            {
                j = value;
            }
            get
            {
                return j;
            }
        }

        public static void Display()
        {
            Console.WriteLine("Static class Display method");
        }
    }
}
