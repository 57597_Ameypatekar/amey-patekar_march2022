﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties1
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 obj = new Class1();

            obj.setx(100);
            Console.WriteLine(obj.getx());
            obj.setx(10);
            Console.WriteLine(obj.getx());
            obj.i = ++obj.i + ++obj.i;

            Console.WriteLine(obj.i);
        }

        static void Main(string[] args)
        {
            Class2 obj = new Class2();
            obj.X = 28;
            Console.WriteLine(obj.X = 30);

            Console.WriteLine("Read Only Name : " + obj.Str);

            obj.P2 = 10;
            Console.WriteLine("value of P2 : " + obj.P2);

            obj.P4 = 11;
            Console.WriteLine("value of P4 : " + obj.P4);

            Console.WriteLine("value of P5 : " + obj.P5);
        }

        public class Class1
        {
            public int i;
            private int x;
            public void setx(int value)
            {
                if (value < 100)
                    x = value;
                else
                    Console.WriteLine("invalid value for x");
            }

            public int getx()
            {
                return x;
            }
        }

        public class Class2
        {
            private int x;
            public int X
            {
                set
                {
                    if (value < 100)
                        x = value;
                    else
                        Console.WriteLine("invalid value for x");
                }

                get
                {
                    return x;
                }
            }


            private string str = "Amey";
            public string Str
            {
                get
                {
                    return str;
                }
            }


            private int p2;
            public int P2
            {
                set
                {
                    p2 = value;
                }
                get
                {
                    return p2;
                }
            }


            public int p3;
            public int P4 { get; set; }

            private int p5;

            public int P5
            {
                private set
                {
                    p5 = value;
                }
                get
                {
                    return p5;
                }
            }


        }
    }
}
