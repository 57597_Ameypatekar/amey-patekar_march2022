﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorsAndObjectInitializers
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class2 obj = new Class2();
            Class2 obj1 = new Class2(10,"amey",12,13,14);
            Console.WriteLine(obj.P4);
            Console.WriteLine(obj1.P4);


        }

        static void Main(string[] args)
        {

            /*Class3 obj = new Class3(102);
            Console.WriteLine(obj.RollNumber+" "+obj.Name);
            Class3 obj1 = new Class3();
            Console.WriteLine(obj1.RollNumber + " " + obj1.Name);
            Class3 obj2 = new Class3(103,"Sai");
            Console.WriteLine(obj2.RollNumber + " " + obj2.Name);*/

            Class3 obj = new Class3() {RollNumber=100,Name="amey" };
            Console.WriteLine(obj.RollNumber + " " + obj.Name);

            Class3 obj1 = new Class3() { RollNumber = 101 };
            Console.WriteLine(obj1.RollNumber + " " + obj1.Name);

            Class3 obj2 = new Class3 { RollNumber = 103 };
            Console.WriteLine(obj2.RollNumber + " " + obj2.Name);



        }
    }

    class Class2
    {
        public Class2()
        {
            X = 10;
            str = "Amey";
            p2 = 100;
            p3 = 99;
            P4 = 40;
            Console.WriteLine("no parameter constructor is called");
        }

        public Class2(int X,string Str,int P2,int P3,int P4)
        {
            this.X = X;
            this.str = Str;
            this.p2 = P2;
            this.p3 = P3;
            this.P4 = P4;
            Console.WriteLine(" parameter constructor is called");
        }


        private int x;
        public int X
        {
            set
            {
                if (value < 100)
                    x = value;
                else
                    Console.WriteLine("invalid value for x");
            }

            get
            {
                return x;
            }
        }


        private string str;
        public string Str
        {
            get
            {
                return str;
            }
        }


        private int p2;
        public int P2
        {
            set
            {
                p2 = value;
            }
            get
            {
                return p2;
            }
        }


        public int p3;
        public int P4 { get; set; }

        private int p5;

        public int P5
        {
            private set
            {
                p5 = value;
            }
            get
            {
                return p5;
            }
        }
    }

    class Class3
    {
        
        public int RollNumber { get; set; }
        public string Name { get; set; }

        /*public Class3(int RollNumber=101,string Name="amey")
        {
            this.RollNumber = RollNumber;
            this.Name = Name;
        }*/
        public Class3()
        {
            Console.WriteLine("no  parameter constuctor is called");
        }

        

    }
}
