﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
        static void Main1(string[] args)
        {
            DerivedClass dc = new DerivedClass();
            dc.i = 10;
            dc.j = 11;

            BaseClass b = new BaseClass();
            b.i = 20;
            //b.j = 11; //error
        }

        static void Main(string[] args)
        {
            TestAccessSpecifiers.BaseClass o2 = new TestAccessSpecifiers.BaseClass();
            //o2.  //only public
        }

    }
    public class BaseClass
    {
        public int i;

    }

    public class DerivedClass : BaseClass
    {
        public int j;
    }

    public class BaseClass1
    {
      
        public int PUBLIC; //public -> everywhere
        private int PRIVATE; //private -> same class
        protected int PROTECTED; //protected -> same class, derived class
        internal int INTERNAL;  //internal -> same class, same assembly(same project)
        protected internal int PROTECTED_INTERNAL;   //protected internal -> same class, derived class, same assembly
        private protected int PRIVATE_PROTECTED;  //private protected - same class, derived class which is in the same assembly
        int x;

    }

    /* public class DerivedClass1 : BaseClass1
     {
         void DoNothing()
         {
           //  this.
         }
     }*/

    public class DerivedClass1 : TestAccessSpecifiers.BaseClass
    {
        void DoNothing()
        {
             // this.
        }
    }






}
