﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicClassConcepts
{
    class Program
    {
        static void Main1()
        {
            Console.Write("hello ");
            Console.WriteLine("World");
        }

        static void Main2()
        {
            Class1 obj = new Class1();

            Console.ReadLine();
            obj.Display();

            Console.ReadLine();
        }

        static void Main3()
        {
            Class1 obj = new Class1();

            obj.Display();
            obj.Display("Amey");

            
        }

        static void Main4()
        {
            Class1 obj = new Class1();

            obj.Display();
            obj.Display("Amey");


        }

        static void Main5()
        {
            Class1 obj = new Class1();

            obj.Add(10, 11, 12);

            obj.Add(10, 11);

            obj.Add(10,11, c:11);

            obj.Add(10);

            obj.Add();

            obj.Add(11, 11, 11);

        }

        static void Main()
        {
            Display();
            int i = 10;
            i++;   //     11
            localFunction();//   11
            i++;   //  13
            void localFunction()
            {
                Console.WriteLine("Value of i : " + i++);
            }
            localFunction();// 13
            ++i;//  15
            localFunction();//  15

            
        }

        static void Display() => Console.WriteLine("Hello Display");

    }

    public class Class1
    {
        private int x;
       

        public void Display()
        {
            Console.WriteLine("Display");
        }

        public void Display(String str)
        {
            Console.WriteLine("Name : "+str);
        }

        public void Add(int a=0, int b=0, int c=0)
        {
            Console.WriteLine("Addition : " + (a + b + c));

        }
    }


}
