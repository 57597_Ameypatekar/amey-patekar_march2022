﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientForSharedAssembly
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassLibrary1.Class1 o = new ClassLibrary1.Class1();
            Console.WriteLine(o.Hello());

        }
    }
}


/*
 Assembly must be signed with a key pair
vis studio- project properties-signing

Assembly must have a strong name
build the assembly

MCQ Question:
install it in the GAC (Global Assembly Cache)
gucatil /i ClassLibrary1.Dll

uninstall it in the GAC (Global Assembly Cache)
gucatil /u ClassLibrary1.Dll

 
 
 */