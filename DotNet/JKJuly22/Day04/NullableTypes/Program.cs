﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            int? i;
            i = null;
            //i = 10;
            Console.WriteLine("i = "+i);
            

            int j;
            if (i != null)
                j =(int) i;
            else
                j = 0;

            if (i.HasValue)
                j = i.Value;
            else
                j = 0;

            j = i.GetValueOrDefault();//default value 0
            j = i.GetValueOrDefault(100);

            j = i ?? 11; //null coalescing operator

            Console.WriteLine("j = " + j);
        }
    }
}
