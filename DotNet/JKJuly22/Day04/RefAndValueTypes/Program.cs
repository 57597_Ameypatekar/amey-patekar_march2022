﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefAndValueTypes
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 o1 = new Class1();
            Class1 o2 = new Class1();

            o1.i = 100;
            o2.i = 200;
            //o2 = o1;         
            o1 = o2;
            o1.i = 300;
            Console.WriteLine(o1.i);//300
            Console.WriteLine(o2.i);//300
        }

        static void Main2(string[] args)
        {
            int n1, n2;
            n1 = 10;
            n2 = 20;
            n1 = n2;
            n2 = 30;
            Console.WriteLine(n1);//20
            Console.WriteLine(n2);//30
        }

        static void Main3(string[] args)
        {
            string s1, s2,s3;
            s1 = "10";
            s2 = "20";
            s1 = s2;
            s2 = "30";
            s3 = s1 + s2;
            Console.WriteLine(s1);//20
            Console.WriteLine(s2);//30
            Console.WriteLine(s3);//2030
        }

        static void Main4(string[] args)
        {
            bool bol=true;
            char ch='a';
            byte b = 150;
            sbyte sb = 127;  //127 to -128
            short s = 12;
            ushort us = 13;
            int i = 14;
            uint ui = 15;
            long l = 16;
            ulong ul = 17;
            float f = 1.18f;
            double d = 1.19;
            decimal dec = 1.206m;

            string str = "name";
            object o = "amey";

            Console.WriteLine(bol);
            Console.WriteLine(ch);
            Console.WriteLine(b);
            Console.WriteLine(sb);
            Console.WriteLine(s);
            Console.WriteLine(us);
            Console.WriteLine(i);
            Console.WriteLine(ui);
            Console.WriteLine(l);
            Console.WriteLine(ul);
            Console.WriteLine(f);
            Console.WriteLine(d);
            Console.WriteLine(dec);
            Console.WriteLine(str);
            Console.WriteLine(o);

        }


    }

    public class Class1
    {
        public int i;
    }

}

namespace RefAndValueTypes1
{
    class Program
    {
        static void Main1()
        {
            int a, b;
            a = 10;
            b = 20;
            //swap(ref a, ref b);
            Init(out a, out b);
            //print(a);
            //print(b);
            Console.WriteLine(a);
            Console.WriteLine(b);

        }

        public static void swap(ref int i, ref int j)
        {
            int temp = i;
            i = j;
            j = temp;
        }

        public static void Init(out int i,out int j)
        {
            i = 100;
            j = 200;
            Console.WriteLine(i);
            Console.WriteLine(j);
        }

        static void print(in int i)
        {
           
            Console.WriteLine(i);
        }
    }
   
}

namespace RefAndValueTypes2
{
    class Program
    {
        static void Main()
        {
            Class1 o = new Class1();
            o.i = 20;
            doSomething3(ref o);
            Console.WriteLine(o.i);
        }
               
        static void doSomething(Class1 obj)
        {
            obj.i = 10;
        }
        static void doSomething2(Class1 obj)
        {
            obj = new Class1();
            obj.i = 10;
        }

        static void doSomething3(ref Class1 obj)
        {
            obj = new Class1();
            obj.i = 10;
        }
    }


    public class Class1
    {
        public int i;
    }
}