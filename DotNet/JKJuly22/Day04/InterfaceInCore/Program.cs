﻿using System;

namespace InterfaceInCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Class1 o = new Class1();

            o.Delete();
            
            IDbfunctions oIdb;
            oIdb = o;
            oIdb.Delete();
            oIdb.DefMethod();
        }
    }

    public interface IDbfunctions
    {
        void Insert();
        void Update();
        void Delete();

        //int a;  variables are not allowed in interface
        static int a; //static variables are allowed in interface

        void DefMethod()
        {
            Console.WriteLine("Default implementation for interface method");
        }

    }

    public class Class1 : IDbfunctions
    {
        public void Delete()
        {
            Console.WriteLine("Class1 - IDb.Delete");
        }

        public void Display()
        {
            Console.WriteLine("Display");
        }

        public void Insert()
        {
            Console.WriteLine("Class1 - IDb.Insert");
        }

        public void Update()
        {
            Console.WriteLine("Class1 - IDb.Update");
        }

        /*public void DefMethod()
        {
            Console.WriteLine("class1 code for def method");
        }*/
       /* void IDbfunctions.DefMethod()
        {
            Console.WriteLine("class1 code for def method - IDbfunctions");
        }*/
    }
}
