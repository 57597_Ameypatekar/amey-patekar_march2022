﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDisposableExample
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 o = new Class1();
            o.Display();
            o.Dispose();

        }

        static void Main(string[] args)
        {
            using (Class1 o = new Class1())
            {
                o.Display();
                Console.WriteLine("this is using block");
            } 
        
        }
    }

    public class Class1 : IDisposable
    {
        public Class1()
        {
            Console.WriteLine("class1 constructor");
        }
        public void Display()
        {
            Console.WriteLine("Display called");
        }
        public void Dispose()
        {
            Console.WriteLine("Dispose code called. Write code here instead of Destructor");
        }
    }
}
