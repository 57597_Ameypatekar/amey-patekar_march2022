﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexers
{
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 o = new Class1();

            o[0] = 5;
            o[1] = 10;
            o[2] = 20;

            for (int i = 0; i < o.Length; i++)
            {
                Console.WriteLine(o[i]);
            }
        }

        static void Main()
        {
            Class1a o = new Class1a(3,100);
            o[100] = 10;
            o[101] = 20;
            o[102] = 30;

            
                Console.WriteLine(o[102]);
            
        }
    }

    class Class1
    {
        private int[] arr=new int[3];
        public int this[int i]
        {
            get 
            {
                return arr[i]; 
            }
            set 
            {
                arr[i] = value;
            }
        }


        public int Length
        {
            get
            {
                return arr.Length;
            }
            
        }
    }

    class Class1a
    {
        private int[] arr;
        int start;
        public Class1a(int Size,int start)
        {
            this.start = start;
            arr = new int[Size];
        }
        public int this[int index]
        {
            get
            {
                return arr[index - start];
            }

            set
            {
                arr[index - start] = value;
            }
        }
        
    }
}
