﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace AsyncCodeWithDelegates
{
    class Program
    {
        static void Main1(string[] args)
        {
            Console.WriteLine("before");
            Action o = Display;
            //o();
            o.BeginInvoke(null, null); //create thread async call
            Console.WriteLine("after");
            
            Console.ReadLine();
        }

        static void Display()
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display");
        }
    }
}

namespace AsyncCodeWithDelegates1
{
    class Program
    {
        static void Main1()
        {
            Console.WriteLine("before");
            Func<string, string> o = Display;
            o.BeginInvoke("Amey", CallbackFunction, null); //async call
            Console.WriteLine("after");

            Console.ReadLine();
        }
        static string Display(string s)
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display : " + s);
            return s.ToUpper();
        }
        static void CallbackFunction(IAsyncResult ar)
        {
            Console.WriteLine("callback func called");
        }
    }
}

namespace AsyncCodeWithDelegates2
{
    class Program
    {
        static Func<string, string> o ;
        static void Main1()
        {
            o = Display;
            Console.WriteLine("before");
            
            IAsyncResult ar=o.BeginInvoke("Amey", CallbackFunction, null); //async call
            
            Console.WriteLine("after");

            Console.ReadLine();
        }
        static string Display(string s)
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display : " + s);
            return s.ToUpper();
        }
        static void CallbackFunction(IAsyncResult ar)
        {
            Console.WriteLine("callback func called");
            string retval;
            retval = o.EndInvoke(ar);
            Console.WriteLine(retval);
        }
    }
}

namespace AsyncCodeWithDelegates3
{
    class Program
    {
        
        static void Main1()
        {
            Func<string, string> o;
            o = Display;
            Console.WriteLine("before");

            o.BeginInvoke("Amey", delegate (IAsyncResult ar)
            {
                Console.WriteLine("callback func called");
                string retval;
                retval = o.EndInvoke(ar);
                Console.WriteLine(retval);
            }, null); //async call

            Console.WriteLine("after");

            Console.ReadLine();
        }
        static string Display(string s)
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display : " + s);
            return s.ToUpper();
        }
        
    }
}

namespace AsyncCodeWithDelegates4
{
    class Program
    {
        
        static void Main1()
        {
             Func<string, string> o;
            o = Display;
            Console.WriteLine("before");

            //o.BeginInvoke("Amey", CallbackFunction, "extra info"); //async call
            o.BeginInvoke("Amey", CallbackFunction, o);
            Console.WriteLine("after");

            Console.ReadLine();
        }
        static string Display(string s)
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display : " + s);
            return s.ToUpper();
        }
        static void CallbackFunction(IAsyncResult ar)
        {
            Console.WriteLine("callback func called");

            //string lastParam=ar.AsyncState.ToString();
            //Console.WriteLine(lastParam);

            Func<string, string> o =(Func<string, string>) ar.AsyncState;

            string retval;
            retval = o.EndInvoke(ar);
            Console.WriteLine(retval);


        }
    }
}

namespace AsyncCodeWithDelegates5
{
    class Program
    {

        static void Main()
        {
            Func<string, string> o;
            o = Display;
            Console.WriteLine("before");

            
            o.BeginInvoke("Amey", CallbackFunction, o);
            Console.WriteLine("after");

            Console.ReadLine();
        }
        static string Display(string s)
        {
            System.Threading.Thread.Sleep(3000);
            Console.WriteLine("display : " + s);
            return s.ToUpper();
        }
        static void CallbackFunction(IAsyncResult ar)
        {
            Console.WriteLine("callback func called");

            AsyncResult objAr = (AsyncResult)ar;
            Func<string, string> o = (Func<string, string>)objAr.AsyncDelegate;

            string retval;
            retval = o.EndInvoke(ar);
            Console.WriteLine(retval);

        }
    }
}
