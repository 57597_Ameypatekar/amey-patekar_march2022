﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces3
{
    
    
    class Program
    {
        static void Main1(string[] args)
        {
            Class1 obj1 = new Class1();
            Class2 obj2 = new Class2();
            
            IDbFunctions oIdb;
            oIdb = obj1;
            oIdb.Delete();

            oIdb = obj2;
            oIdb.Delete();
        }

        static void Main()
        {
            Class1 obj1 = new Class1();
            Class2 obj2 = new Class2();

            InsertMethod(obj1);
            InsertMethod(obj2);
          
        }

        static void InsertMethod(IDbFunctions oidb)
        {
            oidb.Insert();
            oidb.Update();
            oidb.Delete();
        }
    }

    public interface IDbFunctions
    {
        void Insert();
        void Update();
        void Delete();

    }

    public class Class1 : IDbFunctions
    {
        public void Delete()
        {
            Console.WriteLine("Class1 - IDb.Delete");
        }

        public void Display()
        {
            Console.WriteLine("Display");
        }

        public void Insert()
        {
            Console.WriteLine("Class1 - IDb.Insert");
        }

        public void Update()
        {
            Console.WriteLine("Class1 - IDb.Update");
        }
    }

    public class Class2 : IDbFunctions
    {
        public void Delete()
        {
            Console.WriteLine("Class2 - IDb.Delete");
        }

        public void Display()
        {
            Console.WriteLine("Display");
        }

        public void Insert()
        {
            Console.WriteLine("Class2 - IDb.Insert");
        }

        public void Update()
        {
            Console.WriteLine("Class2 - IDb.Update");
        }

    }
}
