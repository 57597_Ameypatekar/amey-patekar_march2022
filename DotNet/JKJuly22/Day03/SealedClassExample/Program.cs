﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SealedClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            //you can instantiate the sealedclass
            SealedClass o = new SealedClass();
            o.Dispaly();
        }
    }

    public abstract class AbstractClass
    {
        public abstract void Dispaly();
    }

    

    public class DerivedClass : AbstractClass
    {
        public override void Dispaly()
        {
            Console.WriteLine(" Display");
        }
    }

    public sealed class SealedClass  : DerivedClass
    {
        public override void Dispaly()
        {
            Console.WriteLine("sealed class Display");
        }
    }

    //cannot inherit from sealed class 
   /* public sealed class subSealedClass : SealedClass
    {
        
    }*/
}

