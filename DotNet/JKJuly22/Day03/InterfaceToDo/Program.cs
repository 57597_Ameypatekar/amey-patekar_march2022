﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceToDo
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 o = new Class1();
            o.Dispaly();

            (o as I1).A();
            (o as I1).B();

            (o as I2).A();
            (o as I2).B();
            (o as I2).C();
            (o as I2).D();
        }
    }

    public interface I1
    {
        void A();
        void B();

    }
    public interface I2: I1
    {
        void C();
        void D();
    }

    public class Class1 : I2
    {
        public void Dispaly()
        {
            Console.WriteLine("Display : class1");
        }
        public void A()
        {
            Console.WriteLine("interface I1 : A");
        }

        public void B()
        {
            Console.WriteLine("interface I1 : B");

        }

        public void C()
        {
            Console.WriteLine("interface I2 : C");

        }

        public void D()
        {
            Console.WriteLine("interface I2 : D");

        }
    }

}
