﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces2
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 obj = new Class1();

            (obj as IDbFunctions).Delete();

            IFileFunctions oIFile;
            oIFile = obj;
            oIFile.Delete();
            
        }
    }

    public interface IDbFunctions
    {
        void Insert();
        void Update();
        void Delete();

    }

    public interface IFileFunctions
    {
        void Open();
        void Close();
        void Delete();

    }

    public class Class1 : IDbFunctions, IFileFunctions
    {
        public void Display()
        {
            Console.WriteLine("Display");
        }

        public void Close()
        {
            Console.WriteLine("Class1 - IFile.Close");
        }

        public void Delete()
        {
            Console.WriteLine("Class1 - IDb.Delete");
        }

        public void Insert()
        {
            Console.WriteLine("Class1 - IDb.Insert");
        }

        public void Open()
        {
            Console.WriteLine("Class1 - IFile.Open");
        }

        public void Update()
        {
            Console.WriteLine("Class1 - IDb.Update");
        }
        
        void IFileFunctions.Delete()
        {
            Console.WriteLine("Class1 - IFile.Delete");
        }

        
    }
}
