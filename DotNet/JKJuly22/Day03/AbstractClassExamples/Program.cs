﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            // AbstractClass obj = new AbstractClass(); cannot create instance of abstract class

            DerivedClass2 obj = new DerivedClass2();
            obj.display();
            obj.show();

        }
    }

    public abstract class AbstractClass
    {
        public void display()
        {
            Console.WriteLine("display from AbstractClass");
        }

    }

    public class DerivedClass : AbstractClass
    {
        public void show()
        {
            Console.WriteLine("show");
        }
    }

    public abstract class AbstractClass2
    {
        
        public abstract void display();
        public abstract void show();
    }

    public class DerivedClass2 : AbstractClass2
    {
        public override void display()
        {
            Console.WriteLine("display from AbstractClass 2");
        }

        public override void show()
        {
            Console.WriteLine("show 2");
        }
    }

}
