﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {

            Class1 obj = new Class1();
            //method 1
            obj.Display();
            obj.Insert();
            obj.Update();

            //method 2
            IDbFunctions oIDb;
            oIDb = obj;

            oIDb.Update();

            //method 3
            ((IDbFunctions)obj).Delete();

            //method 4
            (obj as IDbFunctions).Insert();
        }
    }

    public interface IDbFunctions
    {
        void Insert();
        void Update();
        void Delete();

    }

    public class Class1 : IDbFunctions
    {
        public void Display()
        {
            Console.WriteLine("Display");
        }
        public void Delete()
        {
            Console.WriteLine("Class1 - IDb.Delete");
        }

        public void Insert()
        {
            Console.WriteLine("Class1 - IDb.Insert");
        }

        public void Update()
        {
            Console.WriteLine("Class1 - IDb.Update");
        }
    }
}
