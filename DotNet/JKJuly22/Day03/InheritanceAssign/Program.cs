﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceAssign
{
    class Program
    {
        static void Main(string[] args)
        {

            Manager o1 = new Manager("Manager", 25000, 10, "Exports");
            
            if (o1.EmpNo > 0 && o1.Name != "" && o1.DeptNo > 0 && o1.Basic >= 20000 && o1.Basic < 50000)
                Console.WriteLine("Employee Number       : " + o1.EmpNo + "\n" +
                                  "Employee Name         : " + o1.Name + "\n" +
                                  "Employee Basic        : " + o1.Basic + "\n" +
                                  "Employee Dept Number  : " + o1.DeptNo + "\n" +
                                  "Employee Designation  : " + o1.Designation+"\n"+
                                  "Employee Net Salary   : " + o1.getNetSalary());
            else
                Console.WriteLine("\nEmployee properties are wrongly entered\n");
            Console.WriteLine();


            GeneralManager o = new GeneralManager("General Manager", 51000, 11, "Exports","Bonus");

            if (o.EmpNo > 0 && o.Name != "" && o.DeptNo > 0 && o.Basic >= 50000 && o.Basic < 70000)
                Console.WriteLine("Employee Number       : " + o.EmpNo + "\n" +
                                  "Employee Name         : " + o.Name + "\n" +
                                  "Employee Basic        : " + o.Basic + "\n" +
                                  "Employee Dept Number  : " + o.DeptNo + "\n" +
                                  "Employee Designation  : " + o.Designation + "\n" +
                                  "Employee Perks        : " + o.Perks + "\n" +
                                  "Employee Net Salary   : " + o.getNetSalary());
            else
                Console.WriteLine("\nEmployee properties are wrongly entered\n");
            Console.WriteLine();


            CEO o2= new CEO("CEO", 90000, 12);

            if (o2.EmpNo > 0 && o2.Name != "" && o2.DeptNo > 0 && o2.Basic >= 70000 && o2.Basic < 100000)
                Console.WriteLine("Employee Number       : " + o2.EmpNo + "\n" +
                                  "Employee Name         : " + o2.Name + "\n" +
                                  "Employee Basic        : " + o2.Basic + "\n" +
                                  "Employee Dept Number  : " + o2.DeptNo + "\n" +
                                  "Employee Net Salary   : " + o2.getNetSalary());
            else
                Console.WriteLine("\nEmployee properties are wrongly entered\n");


        }
    }

    public abstract class Employee
    {
        private static int count;
        private string name;
        private int empNo=++count;
        protected decimal basic;
        private short deptNo;
        

        public Employee(string Name = "", decimal Basic = 0, short DeptNo = 0)
        {
            
            this.Name = Name;
            this.Basic = Basic;
            this.DeptNo = DeptNo;
        }

        public abstract decimal getNetSalary();
        


        public string Name
        {
            set
            {
                if (value != "")
                    name = value;
                else
                    Console.WriteLine("Blank names should not be allowed");
            }
            get
            {
                return name;
            }
        }

        public int EmpNo
        {
            
            get
            {
                return empNo;
            }
        }

        public abstract decimal Basic { set; get; }
        

        public short DeptNo
        {
            set
            {
                if (value > 0)
                    deptNo = value;
                else
                    Console.WriteLine("Department Number Must be greater than zero");
            }

            get
            {
                return deptNo;
            }
        }


    }

    public class Manager : Employee
    {
        private string designation;

        public Manager(string Name = "", decimal Basic = 0, short DeptNo = 0,string Designation=""): base(Name,Basic,DeptNo)
        {
            this.Designation = Designation;  
        }

        public String Designation
        {
            set
            {
                if (value != "")
                     designation = value;
                
                else
                     System.Console.WriteLine("Cannot have empty name of designation");
                
            }

            get
            {
                return designation;
            }
        }
        public override decimal Basic { 
            
            set
            {
                if (value >= 20000 && value <= 50000)
                    basic = value;
                else
                    Console.WriteLine("Cannot have basic less than 20000");
            }
            get
            {
                return basic;
            }
        }

        public override decimal getNetSalary()
        {
            decimal da = (100 * basic) / 100;
            decimal hra = (120 * basic) / 100;
            decimal netSalary = basic + da + hra;
            return netSalary;
        }
    }

    public class GeneralManager : Manager
    {
        private string perks;
        public GeneralManager(string Name = "", decimal Basic = 0, short DeptNo = 0, string Designation = "", string Perks = "") : base(Name, Basic, DeptNo, Designation)
        {
            this.Perks = Perks;
        }

        public string Perks
        {
            set
            {
                perks = value;
            }
            get
            {
                return perks;
            }
        }

        public override decimal Basic
        {

            set
            {
                if (value >= 50000 && value <= 70000)
                    basic = value;
                else
                    Console.WriteLine("Cannot have basic less than 50000");
            }
            get
            {
                return basic;
            }
        }

        public override decimal getNetSalary()
        {
            decimal da = (100 * basic) / 100;
            decimal hra = (120 * basic) / 100;
            decimal netSalary = basic + da + hra;
            return netSalary;
        }
    }

    public class CEO : Employee
    {
        public CEO(string Name = "", decimal Basic = 0, short DeptNo = 0) : base(Name, Basic, DeptNo)
        {

        }
        public override decimal Basic
        {

            set
            {
                if (value >= 70000 && value <= 100000)
                    basic = value;
                else
                    Console.WriteLine("Cannot have basic less than 70000");
            }
            get
            {
                return basic;
            }
        }

        public sealed override decimal getNetSalary()
        {
            decimal da = (100 * basic) / 100;
            decimal hra = (120 * basic) / 100;
            decimal netSalary = basic + da + hra;
            return netSalary;
        }

    }
}



