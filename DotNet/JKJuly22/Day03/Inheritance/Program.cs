﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            //DerivedClass obj = new DerivedClass();
            /* base class no param constructor
              derived class no parameter constructor*/

            DerivedClass obj = new DerivedClass(10, 20);
            /*base class no param constructor
            derived class int,int constructor*/

            /*base class int constructor
            derived class int,int constructor*/ // :base(i)

        }
    }

    public class Baseclass
    {
        public int i;
        public Baseclass()
        {
            Console.WriteLine("base class no param constructor");
            i = 10;
        }

        public Baseclass(int i)
        {
            Console.WriteLine("base class int constructor");
            this.i = i;
        }
    }

    public class DerivedClass : Baseclass
    {
        public int j;
        public DerivedClass()
        {
            Console.WriteLine("derived class no parameter constructor");
            j = 20;
        }

        public DerivedClass(int i, int j) : base(i)
        {
            Console.WriteLine("derived class int,int  constructor");
            //this.i = i;
            this.j = j;
        }
    }
}
