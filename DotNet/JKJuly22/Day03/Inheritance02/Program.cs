﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance02
{
    class Program
    {
        static void Main1(string[] args)
        {
            DerivedClass obj = new DerivedClass();
            obj.Display1();
            obj.Display("Amey");
            Console.WriteLine();

            obj.Display2();
            Console.WriteLine();

            obj = new SubSubDerivedClass();
            obj.Display3();


        }

        static void Main()
        {
            BaseClass obj;
            obj = new BaseClass();
            obj.Display2();
            obj.Display3();

            Console.WriteLine();
            obj = new DerivedClass();
            obj.Display2();
            obj.Display3();

            Console.WriteLine();
            obj = new SubDerivedClass();
            obj.Display2();
            obj.Display3();

            Console.WriteLine();
            obj = new SubSubDerivedClass();
            obj.Display2();
            obj.Display3();
        }
    }

    public class BaseClass
    {
        public void Display1()
        {
            Console.WriteLine("Base Display1");
        }

        public void Display2()
        {
            Console.WriteLine("Base Display2");
        }

        public virtual void Display3()
        {
            Console.WriteLine("Base Display3");
        }
    }

    public class DerivedClass : BaseClass
    {
        public void Display(String str)
        {
            Console.WriteLine("Derived Display1 "+str);
        }

        public new void Display2()
        {
            Console.WriteLine("Derived Display2");
        }

        public override void Display3()
        {
            Console.WriteLine("Derived Display3");
        }

    }

    public class SubDerivedClass : DerivedClass
    {
        public sealed override void Display3()
        {
            Console.WriteLine("subDerived Display 3");
        }
    }

    public class SubSubDerivedClass : SubDerivedClass
    {
        public void Display3()
        {
            Console.WriteLine("subsubDerived Display 3");
        }
    }
}
