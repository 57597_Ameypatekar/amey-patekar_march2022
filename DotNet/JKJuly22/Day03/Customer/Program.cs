﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Bank c = new Bank();
             Bank c = new Bank("Virat",30,10000);

            if (c.Cust_name!="" && c.Cust_age>=0 && c.Cust_age<=100 && c.Balance>0)
            Console.WriteLine("Customer ID      : "+c.CustomerId+"\n"+
                              "Customer Name    : " + c.Cust_name + "\n" +
                              "Customer Age     : " + c.Cust_age + "\n" +
                              "Customer Balance : " + c.Balance + "\n" );
            else
                Console.WriteLine("Wrong Details are Entered");

            //Autoincrement 
            Console.WriteLine();
            Bank c1 = new Bank();
            Bank c2 = new Bank();
            
            Console.WriteLine();

            Console.WriteLine(c.CustomerId);
            Console.WriteLine(c1.CustomerId);
            Console.WriteLine(c2.CustomerId);
            Console.WriteLine(c2.CustomerId);
            Console.WriteLine(c1.CustomerId);
            Console.WriteLine(c.CustomerId);

        }


    }

    class Bank
    {
        private static int count;
        private int customerId=++count;
        private string cust_name;
        private short cust_age;
        private decimal balance;

        public Bank(string Cust_name="",short Cust_age=0,decimal Balance=0)
        {
            this.Cust_name = Cust_name;
            this.Cust_age = Cust_age;
            this.Balance = Balance;
        }

        public int CustomerId
        {
            get
            {
                return customerId;
            }
        }

        public string Cust_name
        {
            set
            {
                if(value != "")
                {
                    cust_name = value;
                }
                else
                {
                    Console.WriteLine("Blank names are not allowed");
                }
            }
            get
            {
                return cust_name;
            }
        }

        public short Cust_age
        {
            set
            {

                if(value>=0 && value <= 100)
                {
                    cust_age = value;
                }
                else
                {
                    Console.WriteLine("the age must be inbetween 0 to 100");
                }
            }
            get
            {
                return cust_age;
            }
        }

        public decimal Balance
        {
            set
            {
                if(value > 0)
                {
                    balance = value;
                }
            }
            get
            {
                return balance;
            }
        }
    }
}
