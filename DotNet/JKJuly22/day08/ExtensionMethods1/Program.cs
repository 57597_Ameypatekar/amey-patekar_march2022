﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethods1
{
    class Program
    {
        static void Main1(string[] args)
        {
            string str = "Amey";
            str.Display();

            int i = 10;
            i=i.Add(20);
            i.Show();
        }
        static void Main2(string[] args)
        {
            string str = "Amey";
            MyExtMethodClass.Display(str);

            int i = 10;
            i = MyExtMethodClass.Add(i, 15);
            MyExtMethodClass.Show(i);
            
        }

        static void Main(string[] args)
        {
            ClsMaths o = new ClsMaths();
            Console.WriteLine(o.Substract(30, 2));

            Console.WriteLine(o.CapitalLetters("amey patekar")); 

        }
    }

    //create a static class
    public static class MyExtMethodClass
    {
        //create a static method in the class
        //first parameter should be the type for which you are writing the ext method
        //preced the first parameter with the 'this' keyword
        public static void Display(this string str)  //use this keyword
        {
            Console.WriteLine(str);
        }

        public static void Show(this int i)
        {
            Console.WriteLine(i);
        }

        public static int Add(this int i,int j)
        {
            return i + j;
        }

        //if you define an extension method for an interface, 
        // it is also available to all classes that implement that interface

        public static void ExtMethodForBaseClass(this object s)
        {
            
            Console.WriteLine(s);
        }

        public static int Substract(this IMathOperations i,int a,int b) {
            return a - b;

        }
        //if you define an extension method for an interface, 
        // it is also available to all classes that implement that interface

        public static string CapitalLetters(this object o, string name)
        {
            return name.ToUpper();
        }


    }

    public interface IMathOperations
    {
        int Add(int a, int b);
        int Multiply(int a, int b);
    }

    public class ClsMaths : IMathOperations
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Multiply(int a, int b)
        {
            return a * b;
        }
    }


}
