﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQExample
{
    class Program
    {
        static List<Employee> listEmp = new List<Employee>();
        static List<Department> listDept = new List<Department>();

        public static void AddRecs()
        {
            listDept.Add(new Department { DeptNo = 10, DeptName = "SALES" });
            listDept.Add(new Department { DeptNo = 20, DeptName = "MKTG" });
            listDept.Add(new Department { DeptNo = 30, DeptName = "IT" });
            listDept.Add(new Department { DeptNo = 40, DeptName = "HR" });

            listEmp.Add(new Employee { EmpNo = 1, Name = "Vikram", Basic = 10000, DeptNo = 10, Gender = "M" });
            listEmp.Add(new Employee { EmpNo = 2, Name = "Vikas", Basic = 11000, DeptNo = 10, Gender = "M" });
            listEmp.Add(new Employee { EmpNo = 3, Name = "Abhijit", Basic = 12000, DeptNo = 20, Gender = "M" });
            listEmp.Add(new Employee { EmpNo = 4, Name = "Mona", Basic = 11000, DeptNo = 20, Gender = "F" });
            listEmp.Add(new Employee { EmpNo = 5, Name = "Shweta", Basic = 12000, DeptNo = 20, Gender = "F" });
            listEmp.Add(new Employee { EmpNo = 6, Name = "Amey", Basic = 11000, DeptNo = 30, Gender = "M" });
            listEmp.Add(new Employee { EmpNo = 7, Name = "Arpan", Basic = 10000, DeptNo = 30, Gender = "M" });
            listEmp.Add(new Employee { EmpNo = 8, Name = "Shraddha", Basic = 11000, DeptNo = 40, Gender = "F" });
        }

        static void Main1(string[] args)
        {
            AddRecs();
            /*foreach (Department item in listDept)
            {
                Console.WriteLine(item.DeptName+" "+item.DeptNo);
            }*/

            /*foreach (Employee item in listEmp)
            {
                Console.WriteLine(item.Name+" "+item.EmpNo+" "+item.DeptNo+" "+item.Basic+" "+item.Gender);
            }*/

            var emps = from emp in listEmp select emp;
            foreach (Employee item in emps)
            {
                //Console.WriteLine(item.Name);
                Console.WriteLine(item);
            }
        }

        static void Main2()
        {
            AddRecs();
            var emps = from emp in listEmp select emp.Name;

            foreach (string item in emps)
            {
                Console.WriteLine(item.ToUpper());
            }
            Console.WriteLine();
            var depts = from dept in listDept select dept.DeptName;
            foreach (string item in depts)
            {
                Console.WriteLine(item);
            }
        }

        static void Main3()
        {
            AddRecs();
            var emps = from emp in listEmp select new { emp.Name, emp.Gender };

            foreach (var item in emps)
            {
                Console.WriteLine(item.Name+" "+item.Gender);
            }
            Console.WriteLine();
            
        }

        static void Main4()
        {
            AddRecs();
            /*var emps = from emp in listEmp
                       where emp.Basic>11000
                       select emp;*/
            /*var emps = from emp in listEmp
                       where emp.Basic > 10000 && emp.Basic<12000
                       select emp;*/
            var emps = from emp in listEmp
                       where emp.Name.StartsWith("A")
                       select emp;

            foreach (Employee item in emps)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

        }

        static void Main5()
        {
            AddRecs();
            /*var emps = from emp in listEmp
                       where emp.Basic>11000
                       orderby emp.Name
                       select emp;*/
            var emps = from emp in listEmp
                       orderby emp.DeptNo, emp.Name 
                       select emp;
            foreach (Employee item in emps)
            {
                Console.WriteLine(item);
            }
        }

        static void Main6()
        {
            AddRecs();
            var emps = from emp in listEmp
                       join dept in listEmp
                        on emp.DeptNo equals dept.DeptNo
                       select emp;

            var emps1 = from emp in listEmp
                        join dept in listDept
                        on emp.DeptNo equals dept.DeptNo
                        where emp.DeptNo.Equals(10)
                        select dept;

            var emps2 = from emp in listEmp
                        join dept in listDept
                        on emp.DeptNo equals dept.DeptNo
                        select new { emp, dept };

            var emps3 = from emp in listEmp
                        join dept in listDept
                        on emp.DeptNo equals dept.DeptNo
                        where emp.Gender.Equals("M")
                        select new { emp.Name, dept.DeptNo };

            foreach (var item in emps3)
            {
                Console.WriteLine(item);
            }

            /*foreach (Department item in emps1)
            {
                Console.WriteLine(item.DeptName);
            }*/
                       
        }

        static void Main7()
        {
            AddRecs();

            var emps = from emp in listEmp
                       group emp by emp.DeptNo;


            foreach (var emp in emps)
            {
                Console.WriteLine(emp.Key);
                foreach (var e in emp)
                {
                    Console.WriteLine(e.Name+" "+e.Gender+" "+e.EmpNo+" "+e.Basic);
                }
            }
        }

        static void Main8()
        {
            AddRecs();

            var emps = from emp in listEmp
                       group emp by emp.DeptNo into group1
                       select group1;

            foreach (var emp in emps)
            {
                Console.WriteLine(emp.Key);
                foreach (var e in emp)
                {
                    Console.WriteLine(e);
                }
                Console.WriteLine();
            }

        }

        static void Main()
        {
            AddRecs();
            var emps = from emp in listEmp
                       group emp by emp.DeptNo into group1
                       select new { group1, Count = group1.Count(), Max = group1.Max(x => x.Basic), Min = group1.Min(x => x.Basic) };

            foreach (var emp in emps)
            {
                Console.WriteLine(emp.group1.Key);
                Console.WriteLine(emp.Count);
                Console.WriteLine(emp.Min);
                Console.WriteLine(emp.Max);

                foreach(var e in emp.group1)
                {
                    Console.WriteLine(e);
                }

                Console.WriteLine();
            }
        }
    }

    public class Department
    {
        public int DeptNo { get; set; }
        public string DeptName { get; set; }
    }
    public class Employee
    {
        public int EmpNo { get; set; }
        public string Name { get; set; }
        public decimal Basic { get; set; }
        public int DeptNo { get; set; }
        public string Gender { get; set; }
        public override string ToString()
        {
            string s = Name + "," + EmpNo.ToString() + "," + Basic.ToString() + "," + DeptNo.ToString();
            return s;
        }
    }
}
