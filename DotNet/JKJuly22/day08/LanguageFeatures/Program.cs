﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ImplicitVariables
{
    class Program
    {
        //used only for local variables
        //cant be used for class level vars,fn params and return types
        //var a = 10;
        static void Main1()
        {
            //int i=100;
            var i = 100;
            //i = "abc";
            //implicit variable
            //var a =(long) 100; //implicit variable
            //a= "aaa"; //error
            //var j; //implicit type variables must be initialize
            Console.WriteLine(i);
            Console.WriteLine(i.GetType());
            Console.ReadLine();
        }
    }
}

namespace AnonymousTypes
{
    class Program
    {
        static void Main1()
        {
            //Class1 obj = new Class1{a=1, b="aaa",c = true};

            var o = new { id = 1, name = "amey", sal = 2000 };
            Console.WriteLine(o.id);
            Console.WriteLine(o.sal);
            Console.WriteLine(o.GetType());


            /*var obj = new { a = 1, b = "aaa", c = true };
            var obj2 = new { a = 2, b = "bbb" };
            Console.WriteLine(obj.b);
            Console.WriteLine(obj.GetType());
            Console.WriteLine(obj2.GetType());*/

            Console.ReadLine(); ;
        }
    }
}

//partial classes
namespace LanguageFeatures
{
    //PARTIAL CLASSES

    //partial classes must be in the same assembly
    //partial classes must be in the same namespace
    //partial classes must have the same name

    public partial class A
    {
        public int i;
    }
    public partial class A
    {
        public int j;
    }
    class Program
    {
        static void Main(string[] args)
        {
            A obj = new A();
            obj.i=10;
            obj.j=20;
            obj.k = 30; //same namespace
            obj.l = 40; //same assembly/project
            Console.WriteLine("i="+obj.i+" j="+obj.j + " k=" + obj.k + " l=" + obj.l);

            Console.ReadLine();
        }
    }
}

namespace LanguageFeatures
{
    public partial class A
    {
        public int k;
    }
}