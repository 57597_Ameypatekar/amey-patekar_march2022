﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayAssignment2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter Number of Batches : ");
            int num = int.Parse(Console.ReadLine());
            int[][] arrCDAC = new int[num][];

            for (int i = 0; i < num; i++)
            {
                Console.Write($"Enter Number of Students for Batch{i+1} : ");
                int numOfStudents = int.Parse(Console.ReadLine());
                arrCDAC[i] = new int[numOfStudents];
                for (int j = 0; j < numOfStudents; j++)
                {
                    Console.Write($"Enter Marks for Student{j+1} : ");
                    arrCDAC[i][j] = int.Parse(Console.ReadLine());
                    
                    
                }
            }
            Console.WriteLine();

            for (int i = 0; i < arrCDAC.Length; i++)
            {
                Console.WriteLine($"CDAC Batch Number {i+1} ");
                Console.WriteLine("----------------------------");

                for (int j = 0; j < arrCDAC[i].Length; j++)
                {
                    Console.WriteLine($"Student{j+1} Marks : "+arrCDAC[i][j]);
                }
                Console.WriteLine("\n");
            }
           
        }
    }


}

/*2. CDAC has certain number of batches. each batch has certain number of students
         accept number of batches from the user. for each batch accept number of students.
         create an array to store mark for each student. 
         accept the marks.
         display the marks.*/