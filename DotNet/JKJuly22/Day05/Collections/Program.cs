﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList o = new ArrayList();
            ArrayList o1 = new ArrayList();
            o1.Add("one");
            o1.Add("two");

            o.AddRange(o1);
            o.Add("Amey");
            o.Add(10);
            o.Add(12.34);
            Console.WriteLine(o.Count);
            o.Remove(10);
            o.RemoveAt(0);
            o.Insert(0, "Amey");
            //o.AddRange(o1);
            o.InsertRange(0, o1);
            o.RemoveRange(0,1);
            //o.Clear();
            Console.WriteLine(o.Contains("two"));
            o.IndexOf(12.34);
            Console.WriteLine(o.LastIndexOf("two"));
            object[] arr = new object[5];
            o.CopyTo(arr);
            o.SetRange(1, o1);
            object[] arr1 = o.ToArray();


            //o.Capacity = no_of_records_in_db
            //read records from db in a loop
            //add one item to an arraylist
            //o.TrimToSize()


            foreach (object item in arr1)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
