﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main1(string[] args)
        {
            int[] arr = new int[5];

            for (int i = 0; i < arr.Length; i++)
            {
                //Console.Write("Enter array elements [{0}] : ",i); //placeholders
                Console.Write($"Enter array elemnets [{i}]");
                arr[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < arr.Length; i++)
            {
               // Console.WriteLine("arr[{0}] : {1}",i,arr[i]);
                Console.WriteLine($"The value of arr[{i}] is {arr[i]}");//string interpolation 
            }
            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }
        }
        static void Main2(string[] args)
        {
            int[] arr = new int[3];
            int[] arr2 = new int[3];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"Enter Array Element at index {i} : ");
                arr[i] = int.Parse(Console.ReadLine());
            }

            //Console.WriteLine(Array.IndexOf(arr,20));
            //Console.WriteLine(Array.LastIndexOf(arr,20));
            //int pos=Array.BinarySearch(arr,30);
            //Console.WriteLine(pos);

            //Array.Clear(arr, 0, arr.Length);
            //Array.Copy(arr, arr2, arr.Length);
            //Array.Sort(arr);
            //Array.Reverse(arr);
            foreach (int item in arr)
            {
                Console.WriteLine(item);
            }

        }
        static void Main3(string[] args)
        {
            int[,] arr = new int[3, 2];

            Console.WriteLine(arr.Length);
            Console.WriteLine(arr.Rank);

            Console.WriteLine(arr.GetLength(0));
            Console.WriteLine(arr.GetLength(1));

            Console.WriteLine(arr.GetLowerBound(0));
            Console.WriteLine(arr.GetUpperBound(0));
            Console.WriteLine(arr.GetUpperBound(1));

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.WriteLine($"Enter array elements [{i},{j}] : ");
                    arr[i, j] = int.Parse(Console.ReadLine());
                }
            }

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.WriteLine($"The value of arr[{i},{j}] is {arr[i, j]} ");  //string interpolation

                }
            }
        }
        static void Main4(string[] args)
        {
            int[][] arr = new int[4][];
            arr[0] = new int[2];
            arr[1] = new int[3];
            arr[2] = new int[2];
            arr[3] = new int[4];

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    Console.Write("enter value for subscript [{0}][{1}] : ", i, j);
                    arr[i][j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            
        }

        static void Main()
        {
            Employee[] arrEmps = new Employee[4];
            for (int i = 0; i < arrEmps.Length; i++)
            {
                arrEmps[i] = new Employee();
                //other code
            }
        }
    }

    public class Employee
    {
        public int EmpNo { get; set; }
        public string Name { get; set; }

    }
}
