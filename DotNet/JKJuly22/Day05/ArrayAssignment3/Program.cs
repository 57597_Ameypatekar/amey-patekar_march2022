﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayAssignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] arrStudent = new Student[5];
            Console.WriteLine("Enter Following Information for 5 Students");
            Console.WriteLine("------------------------------------------");
            for (int i = 0; i < arrStudent.Length; i++)
            {
                Console.Write($"Enter Name for Student{i+1} : ");
                string stName = Console.ReadLine();
                Console.Write($"Enter Roll Number for Student{i+1} : ");
                int stRollNo = int.Parse(Console.ReadLine());
                Console.Write($"Enter Marks for Student{i+1} : ");
                int stMarks = int.Parse(Console.ReadLine());
                arrStudent[i] = new Student(stName,stRollNo,stMarks);
                Console.WriteLine();
            }

            for (int i = 0; i < arrStudent.Length; i++)
            {
                Console.WriteLine($"Student {i+1} Information :");
                Console.WriteLine($"Student {i+1} Name        : "+arrStudent[i].name);
                Console.WriteLine($"Student {i+1} Roll Number : "+arrStudent[i].rollNo);
                Console.WriteLine($"Student {i+1} Marks       : "+arrStudent[i].marks);
                Console.WriteLine("------------------------------------------------");
            }
        }
    }

    public struct Student
    {
        public string name;
        public int rollNo;
        public decimal marks;

        public Student(string Name="",int RollNo=0,decimal Marks=0m)
        {
            this.name = Name;
            this.rollNo = RollNo;
            this.marks = Marks;
        }

        

        /*public string Name
        {
            set
            {
                if (value != "")
                    name = value;
                else
                    Console.WriteLine("Name should not be empty");
            }
            get
            {
                return name;
            }
        }

        public int RollNo
        {
            set
            {
                if (value > 0)
                    rollNo = value;
                else
                    Console.WriteLine("Roll number must be greater than zero");
            }
            get
            {
                return rollNo;
            }
        }

        public decimal Marks
        {
            set
            {
                if (value > 0 && value <= 100)
                    marks = value;
                else
                    Console.WriteLine("Marks must be inbetween 0 - 100 ");
            }
            get
            {
                return marks;
            }
        }*/


    }
}


/*3. Create a struct Student with the following properties. Put in appropriate validations.
string Name
int RollNo
decimal Marks
Create a parameterized constructor.
Create an array to accept details for 5 students*/