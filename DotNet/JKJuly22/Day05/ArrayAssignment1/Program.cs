﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayAssignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] empArray = new Employee[3];

            for (int i = 0; i < empArray.Length; i++)
            {
                empArray[i] = new Employee();
                Console.WriteLine($"Enter Information for Employee {i+1} : ");
                Console.Write($"Enter Employee Name : ");
                empArray[i].EmpName = Console.ReadLine();
                Console.Write($"Enter Employee Salaray : ");
                empArray[i].Sal = decimal.Parse(Console.ReadLine());
                Console.WriteLine();
            }

            Console.WriteLine();

            //Display the Employee with highest Salary         
            decimal tempSal = 0;
            Employee e= null;
            for (int i = 0; i < empArray.Length; i++)
            {
                
                    if (empArray[i].Sal > tempSal)
                    {
                        tempSal = empArray[i].Sal;
                        e = empArray[i];
                    }
                
                   
            }

            Console.WriteLine("Highest Salary  : "+tempSal+"\n");
            Console.WriteLine(e.ToString());

            //Accept EmpNo to be searched
            Console.WriteLine();
            Console.Write("Enter Employee Number to be searched : ");
            int num = int.Parse(Console.ReadLine());

            for (int i = 0; i < empArray.Length; i++)
            {
                if (num == empArray[i].EmpNo)
                {
                    Console.WriteLine("Employee Number : " + empArray[i].EmpNo);
                    Console.WriteLine("Employee Name : " + empArray[i].EmpName);
                    Console.WriteLine("Employee Salary : " + empArray[i].Sal);
                    Console.WriteLine();
                }
            }


        }
    }

    class Employee
    {
        private static int count=0;
        private int empNo=++count;
        private string empName;
        private decimal sal;

        

        public int EmpNo
        {
            private set
            {
                
            }
            get { return empNo; }
        }

        public string EmpName
        {
            set 
            {
                empName = value;
            }
            get
            {
                return empName;
            }
        }
        public decimal Sal
        {
            set
            {
                sal = value;
            }
            get
            {
                return sal;
            }
        }

        public override string ToString()
        {
            return "Employee Number : "+empNo +"\n"+
                   "Employee Name   : "+empName+"\n"+
                   "Employee Sal    : "+sal;
        }
    }
}

/*1. Create an array of Employee class objects
        Accept details for all Employees
        Display the Employee with highest Salary
        Accept EmpNo to be searched. Display all details for that employee.*/