﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee obj = new Employee
            {
                EmpNo = 7,
                Basic = 5500,
                Name = "Vinayak",
                DeptNo = 10
            };

            //Connect();
            //Insert(obj);
            //InsertWithParameters(obj);
            //InsertStoredProcedure(obj);
            //UpdateStoredProcedure(obj);
            //DeleteStoredProcedure(obj);
            //QueryReturningValue();
            //SqlDataReader1();
            //SqlDataReaderNextResult();
            //MARS();
            //CallFuncReturningSqlDataReader();
            //Transactions();
            //AsyncCommand();
            //AsyncCommand2();

            Console.ReadLine();
        }

        static void Connect()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";
            
            try
            {
                con.Open();
                Console.WriteLine("connection successful");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            if (con.State == ConnectionState.Open)
                con.Close();
        }

        static void Insert(Employee obj)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdInsert = new SqlCommand();
                cmdInsert.Connection = con;
                cmdInsert.CommandType = CommandType.Text;
               // cmdInsert.CommandText = "insert into Employees values(4,'Ajinkya',7000,20)";
                cmdInsert.CommandText = $"insert into Employees values({obj.EmpNo},'{obj.Name}',{obj.Basic},{obj.DeptNo})";
                cmdInsert.ExecuteNonQuery();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
            
        }

        static void InsertWithParameters(Employee obj)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdInsert = new SqlCommand();
                cmdInsert.Connection = con;
                cmdInsert.CommandType = CommandType.Text;
                // cmdInsert.CommandText = "insert into Employees values(4,'Ajinkya',7000,20)";
                cmdInsert.CommandText = $"insert into Employees values(@EmpNo,@Name,@Basic,@DeptNo)";
                cmdInsert.Parameters.AddWithValue("@EmpNo", obj.EmpNo);
                cmdInsert.Parameters.AddWithValue("@Name", obj.Name);
                cmdInsert.Parameters.AddWithValue("@Basic", obj.Basic);
                cmdInsert.Parameters.AddWithValue("@DeptNo", obj.DeptNo);
                cmdInsert.ExecuteNonQuery();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        static void InsertStoredProcedure(Employee obj)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdInsert = new SqlCommand();
                cmdInsert.Connection = con;
                //changes
                cmdInsert.CommandType = CommandType.StoredProcedure;
                cmdInsert.CommandText = "InsertEmployee";

                cmdInsert.Parameters.AddWithValue("@EmpNo", obj.EmpNo);
                cmdInsert.Parameters.AddWithValue("@Name", obj.Name);
                cmdInsert.Parameters.AddWithValue("@Basic", obj.Basic);
                cmdInsert.Parameters.AddWithValue("@DeptNo", obj.DeptNo);
                cmdInsert.ExecuteNonQuery();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        //update 
        static void UpdateStoredProcedure(Employee obj)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdUpdate = new SqlCommand();
                cmdUpdate.Connection = con;
                //changes
                cmdUpdate.CommandType = CommandType.StoredProcedure;
                cmdUpdate.CommandText = "UpdateEmployee";

                cmdUpdate.Parameters.AddWithValue("@EmpNo", obj.EmpNo);
                cmdUpdate.Parameters.AddWithValue("@Name", obj.Name);
                cmdUpdate.Parameters.AddWithValue("@Basic", obj.Basic);
                cmdUpdate.Parameters.AddWithValue("@DeptNo", obj.DeptNo);
                cmdUpdate.ExecuteNonQuery();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        //delete
        static void DeleteStoredProcedure(Employee obj)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdDelete = new SqlCommand();
                cmdDelete.Connection = con;
                //changes
                cmdDelete.CommandType = CommandType.StoredProcedure;
                cmdDelete.CommandText = "DeleteEmployee";

                cmdDelete.Parameters.AddWithValue("@EmpNo", obj.EmpNo);
           
                cmdDelete.ExecuteNonQuery();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        static void QueryReturningValue()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdInsert = new SqlCommand();
                cmdInsert.Connection = con;
                //changes
                cmdInsert.CommandType = CommandType.Text;
                //cmdInsert.CommandText = "Select count(*) from Employees";
                cmdInsert.CommandText = "Select * from Employees";//1


                object retval=cmdInsert.ExecuteScalar();//return first column of first row
                Console.WriteLine(retval);
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        //read multiple rows
        static void SqlDataReader1()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdSelect = new SqlCommand();
                cmdSelect.Connection = con;
               
                cmdSelect.CommandType = CommandType.Text;
                cmdSelect.CommandText = "Select * from Employees";
                SqlDataReader dr = cmdSelect.ExecuteReader();

                /*dr.Read(); //returns true or false
                //dr[0] dr["column name"]*/

                while (dr.Read())
                {
                    Console.WriteLine(dr["EmpNo"]+" "+dr["Name"]+" "+ dr["Basic"]+" "+ dr["DeptNo"]);
                    //Console.WriteLine(dr[1]);
                    
                    Console.WriteLine();
                }

                dr.Close();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        //multiple results
        static void SqlDataReaderNextResult()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            try
            {
                con.Open();
                SqlCommand cmdSelect = new SqlCommand();
                cmdSelect.Connection = con;

                cmdSelect.CommandType = CommandType.Text;
                cmdSelect.CommandText = "Select * from Employees;Select * from Departments";
                SqlDataReader dr = cmdSelect.ExecuteReader();

                while (dr.Read())
                {
                    Console.WriteLine(dr["EmpNo"] + " " + dr["Name"] + " " + dr["Basic"] + " " + dr["DeptNo"]);
                    //Console.WriteLine(dr[1]);

                    Console.WriteLine();
                }
                Console.WriteLine();



                dr.NextResult();
                while (dr.Read())
                {
                    Console.WriteLine(dr[0]+" "+dr[1]);
                }
                dr.Close();
                Console.WriteLine("Okay");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }

        }

        //MultipleActiveResultSets=true
        static void MARS()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;MultipleActiveResultSets=true";

            try
            {
                con.Open();
                SqlCommand cmdDepts = new SqlCommand();
                cmdDepts.Connection = con;
                cmdDepts.CommandType = CommandType.Text;
                cmdDepts.CommandText = "Select * from Departments";

                SqlCommand cmdEmps = new SqlCommand();
                cmdEmps.Connection = con;
                cmdEmps.CommandType = CommandType.Text;

                SqlDataReader drDepts = cmdDepts.ExecuteReader();
                while (drDepts.Read())
                {
                    Console.WriteLine((drDepts["DeptName"]));

                    cmdEmps.CommandText = "Select * from Employees where DeptNo = " + drDepts["DeptNo"];
                    SqlDataReader drEmps = cmdEmps.ExecuteReader();
                    while (drEmps.Read())
                    {
                        Console.WriteLine(("    " + drEmps["Name"]+" "+drEmps["Basic"]+" "+drEmps["DeptNo"]));
                    }
                    drEmps.Close();
                }
                drDepts.Close();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        static void CallFuncReturningSqlDataReader()
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Employee Table");
            Console.WriteLine("--------------------------------");
            SqlDataReader dr = GetEmployeeDataReader();
            while (dr.Read())
            {
                Console.WriteLine(dr[0]+" "+dr[1]+" "+dr[2]+"  "+dr[3]);
                Console.WriteLine();
            }
            dr.Close();

            Console.WriteLine("--------------------------------");
            Console.WriteLine("Department Table");
            Console.WriteLine("--------------------------------");
            SqlDataReader dr1 = GetDepartmentDataReader();
            while (dr1.Read())
            {
                Console.WriteLine(dr1[0] + " " + dr1[1] );
                Console.WriteLine();
            }
            dr1.Close();

        }

        static SqlDataReader GetEmployeeDataReader()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            
                con.Open();
                SqlCommand cmdSelect = new SqlCommand();
                cmdSelect.Connection = con;
                cmdSelect.CommandType = CommandType.Text;
                cmdSelect.CommandText = "select * from Employees ";

                SqlDataReader dr = cmdSelect.ExecuteReader(CommandBehavior.CloseConnection);
                return dr;
        }

        static SqlDataReader GetDepartmentDataReader()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";


            con.Open();
            SqlCommand cmdSelect = new SqlCommand();
            cmdSelect.Connection = con;
            cmdSelect.CommandType = CommandType.Text;
            cmdSelect.CommandText = "select * from Departments ";

            SqlDataReader dr = cmdSelect.ExecuteReader(CommandBehavior.CloseConnection);
            return dr;
        }

        static void Transactions()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True;";

            con.Open();
            SqlTransaction t = con.BeginTransaction();

            SqlCommand cmdInsert = new SqlCommand();
            cmdInsert.Connection = con;
            cmdInsert.Transaction = t;

            cmdInsert.CommandType = CommandType.Text;
            cmdInsert.CommandText= "insert into Employees values(7, 'Vinayak', 4500, 30)";

            SqlCommand cmdInsert2 = new SqlCommand();
            cmdInsert2.Connection = con;
            cmdInsert2.Transaction = t;

            cmdInsert2.CommandType =  CommandType.Text;
            cmdInsert2.CommandText = "insert into Employees values(8, 'Aditya', 5500, 30)";

            try
            {
                cmdInsert.ExecuteNonQuery();
                cmdInsert2.ExecuteNonQuery();
                t.Commit();
                Console.WriteLine("no errors- commit successful");
            }
            catch (Exception ex)
            {
                t.Rollback();
                Console.WriteLine("Rollback");
                Console.WriteLine(ex.Message);
            }
            finally
            {
                CallFuncReturningSqlDataReader();
            }
        }

        static async void AsyncCommand()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True";

            await con.OpenAsync();  //async connection
            try
            {
                SqlCommand cmdInsert = new SqlCommand();
                cmdInsert.Connection = con;
                cmdInsert.CommandType = CommandType.Text;
                cmdInsert.CommandText = "insert into Employees values(9,'Ganesh',9500,30)";

                await cmdInsert.ExecuteNonQueryAsync();

                Console.WriteLine("okay");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (con.State == ConnectionState.Open)
                con.Close();

        }

        static void AsyncCommand2()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=JKJuly2022;Integrated Security=True";

            con.Open();

            SqlCommand cmdInsert = new SqlCommand();
            cmdInsert.Connection = con;
            cmdInsert.CommandType = CommandType.Text;
            cmdInsert.CommandText = "Select * from Employees";

            cmdInsert.BeginExecuteReader(delegate (IAsyncResult ar)
            {
                SqlDataReader dr = cmdInsert.EndExecuteReader(ar);
                while (dr.Read())
                {
                    Console.WriteLine(dr["Name"]+" "+dr["DeptNo"]);
                    //Console.WriteLine(dr[1]);
                }
                dr.Close();
                con.Close();
            }, null);

            Console.WriteLine("code after begin....");
        }
    }

    class Employee
    {
        public int EmpNo { get; set; }
        public string Name { get; set; }
        public decimal Basic { get; set; }
        public int DeptNo { get; set; }
    }
}


/*
todo--

 1.update employee
UPDATE Employees
 SET Name = @Name, Basic = @Basic, DeptNo=@DeptNo
 WHERE EmpNo=@EmpNo

2.delete Employee

DELETE FROM Employees WHERE EmpNo=@EmpNo;
 */