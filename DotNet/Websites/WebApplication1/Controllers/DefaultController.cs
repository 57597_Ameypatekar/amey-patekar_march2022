﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        /*public string Index()
        {
            
            return "<h1>Hello Amey</h1>";
        }*/

        /*public ViewResult Index()
        {
            return View();
        }*/

        //without layout
        public ActionResult Index()
        {
            //return HttpNotFound();

            return View();
        }

        //with layout
        public ActionResult Index2()
        {
            
            return View();
        }

        //specific layout(which can be any layout)
        public ActionResult Index3()
        {
            
            return View();
        }

        //with parameter
        /* public ActionResult Index4(int id=0)
         {

             return View();
         }*/

        public ActionResult Index4(int? id)
        {
            if(id==null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.id = id;
                return View();
            }
           
        }

        public ActionResult SignUpForm()
        {
             return View();
        }

    }
}