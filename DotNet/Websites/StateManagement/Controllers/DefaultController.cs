﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StateManagement.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            ViewData["key1"] = "abc";
            ViewData["key2"] = 100;
            ViewBag.key3 = true;

            TempData["key4"] = "amey";


            Session["name"] = "Vikram";
            Session["id"] = 10;

            HttpCookie objCookie = new HttpCookie("Chocochip");
            objCookie.Expires = DateTime.Now.AddSeconds(15);
            //objCookie.Expires = DateTime.Now.AddDays(15);
            //objCookie.Expires = DateTime.Now.AddMinutes(15);
            objCookie.Value = "amey";
            objCookie.Values["key1"] = "a";
            objCookie.Values["key2"] = "b";
            objCookie.Values["key3"] = "c";
            Response.Cookies.Add(objCookie);

            System.Web.HttpContext.Current.Application["ApplicationVar"] = "Abc";

            return View();
            //return RedirectToAction("Index2");
        }

        public ActionResult Index2()
        {
            string s;
            s = (string)Session["name"];

            int id;
            id = (int)Session["id"];

            
            return View();

        }

        public ActionResult Index3()
        {
            HttpCookie objCookie = new HttpCookie("Chocochip");
            objCookie.Expires = DateTime.Now.AddSeconds(-15);
            Response.Cookies.Add(objCookie);


            return View();

        }
       
    }
}