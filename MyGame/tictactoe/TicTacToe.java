/*
Game-Tic-tac-toe
langauge-java
date-18/04/2022
Name-Amey Ramesh Patekar(TL-Team02_CDAC Juhu)
*/
import java.util.*;
class TicTacToe {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\t============================================================");
        System.out.println("\t\t\t\tTic-Tac-Toe");
        System.out.println("\t============================================================");
        System.out.println("\n");
        System.out.print("\tEnter Player 1 Name :");
        String player1 = sc.nextLine();
        System.out.println();
        System.out.print("\tEnter Player 2 Name :");
        String player2 = sc.nextLine();
        System.out.println();
        System.out.print("\tSelect Your Sign for Player1 : 1(O)/2(X) :");
        int sign = sc.nextInt();
        String zero = "O", cross = "X";
        String player1entry = "", player2entry = "";
        if (sign == 1) {
            player1entry = zero;
            player2entry = cross;
        } else if (sign == 2) {
            player1entry = cross;
            player2entry = zero;
        }
        //Matrix index logic
        
        String arr[] = {
            "[ 1 ]",
            "[ 2 ]",
            "[ 3 ]",
            "[ 4 ]",
            "[ 5 ]",
            "[ 6 ]",
            "[ 7 ]",
            "[ 8 ]",
            "[ 9 ]"
        };
        System.out.println();
        for (int i = 0; i < arr.length; i++) {

            if (i == 3) {
                System.out.println();
            }
            if (i == 6) {
                System.out.println();
            }
            for (int space = 0; space < 12; space++) {
                System.out.print(" ");
            }
            System.out.print(arr[i]);

        }
        System.out.println();
        System.out.println();
        int round = 1;
        int ch = 1;
        int score1 = 0, score2 = 0;
        while (ch == 1) {

            System.out.println("\t=============================================================");
            System.out.println("\t\t\t\t  ROUND - " + round++);
            System.out.println("\t=============================================================");
            //game logic
            System.out.print("\tPlayer 1 : " + player1 + " (" + player1entry + ")");
            System.out.print("\t\t\tPlayer 2 : " + player2 + " (" + player2entry + ")");
            System.out.println();
            System.out.println();
            System.out.print("\tScore : " + score1);
            System.out.print("\t\t\t\tScore : " + score2);
            System.out.println();
            System.out.println();


            String tarr[] = {
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            };

            for (int i = 0; i < tarr.length; i++) {

                if (i == 3) {
                    System.out.println();
                }
                if (i == 6) {
                    System.out.println("");
                }
                for (int space = 0; space < 12; space++) {
                    System.out.print(" ");
                }
                System.out.print("[ " + tarr[i] + "  ]");

            }
            int count = 0;

            int choice = 1;
            while (choice == 1) {
                count = ++count;
                System.out.println("\n");
                System.out.print("\t" + player1 + " Enter your placement number : ");
                int index = sc.nextInt();

                tarr[index - 1] = player1entry;
                System.out.println();
                for (int i = 0; i < tarr.length; i++) {

                    if (i == 3) {
                        System.out.println();
                    }
                    if (i == 6) {
                        System.out.println("");
                    }
                    for (int space = 0; space < 12; space++) {
                        System.out.print(" ");
                    }
                    
                    System.out.print(String.format("%-4s", "[ " + tarr[i]));
                    System.out.print(String.format("%-1s", "]"));

                }
                if (tarr[0] == player1entry && tarr[1] == player1entry && tarr[2] == player1entry ||
                    tarr[0] == player1entry && tarr[3] == player1entry && tarr[6] == player1entry ||
                    tarr[3] == player1entry && tarr[4] == player1entry && tarr[5] == player1entry ||
                    tarr[6] == player1entry && tarr[7] == player1entry && tarr[8] == player1entry ||
                    tarr[1] == player1entry && tarr[4] == player1entry && tarr[7] == player1entry ||
                    tarr[2] == player1entry && tarr[5] == player1entry && tarr[8] == player1entry ||
                    tarr[0] == player1entry && tarr[4] == player1entry && tarr[8] == player1entry ||
                    tarr[2] == player1entry && tarr[4] == player1entry && tarr[6] == player1entry) {
                    System.out.println("\n\n\t " + player1 + " is a Winner !!!");
                    score1 = score1 + 1;
                    
                    break;
                }
                if (count == 9) {
                    System.out.println("\n\n\tIts a Draw !!!");
                    break;
                }

                int choice1 = 1;
                while (choice1 == 1) {
                    count = ++count;
                    System.out.println("\n");
                    System.out.print("\t" + player2 + " Enter your placement number : ");
                    index = sc.nextInt();

                    tarr[index - 1] = player2entry;
                    System.out.println();

                    for (int i = 0; i < tarr.length; i++) {

                        if (i == 3) {
                            System.out.println();
                        }
                        if (i == 6) {
                            System.out.println("");
                        }
                        for (int space = 0; space < 12; space++) {
                            System.out.print(" ");
                        }
                        
                        System.out.print(String.format("%-4s", "[ " + tarr[i]));
                        System.out.print(String.format("%-1s", "]"));

                    }
                    if (tarr[0] == player2entry && tarr[1] == player2entry && tarr[2] == player2entry ||
                        tarr[0] == player2entry && tarr[3] == player2entry && tarr[6] == player2entry ||
                        tarr[3] == player2entry && tarr[4] == player2entry && tarr[5] == player2entry ||
                        tarr[6] == player2entry && tarr[7] == player2entry && tarr[8] == player2entry ||
                        tarr[1] == player2entry && tarr[4] == player2entry && tarr[7] == player2entry ||
                        tarr[2] == player2entry && tarr[5] == player2entry && tarr[8] == player2entry ||
                        tarr[0] == player2entry && tarr[4] == player2entry && tarr[8] == player2entry ||
                        tarr[2] == player2entry && tarr[4] == player2entry && tarr[6] == player2entry) {
                        System.out.println("\n\n\t " + player2 + " is a Winner !!!");
                        score2 = score2 + 1;
                        choice = 0;
                        
                        break;
                    }
                    choice1 = 0;
                }


            }

            System.out.println();
            System.out.println();
            System.out.print("\tWant to continue Enter 1(YES)/0(NO) : ");
            ch = sc.nextInt();
        }


        System.out.println("\n");
        if (score1 == score2) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t    SCORES ARE TIED ");
            System.out.println("\t==========================================================");
        } else if (score1 > score2) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t    CONGRATULATIONS ");

            System.out.println("\t\t\t   "+ player1 + " is a Winner !!!");
            System.out.println("\t==========================================================");
        } else if (score1 < score2) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t    CONGRATULATIONS ");

            System.out.println("\t\t\t   "+ player2 + " is a Winner !!!");
            System.out.println("\t==========================================================");
        }


    }
}