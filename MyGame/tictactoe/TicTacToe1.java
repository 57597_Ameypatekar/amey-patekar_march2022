/*
Game-Tic-tac-toe
langauge-java
date-18/04/2022
Name-Amey Ramesh Patekar(TL-Team02_CDAC Juhu)
*/
import java.util.*;
class TicTacToe1 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("\t==========================================================");
        System.out.println("\t\t\t\tTic-Tac-Toe");
        System.out.println("\t==========================================================");
        System.out.println("\n");
        System.out.print("\tEnter Your Name :");
        String team1 = sc.nextLine();
        System.out.println();
        
        String team2 = "Computer";
        System.out.println();
        System.out.print("\tSelect Your Sign : 1(O)/2(X) :");
        int sign = sc.nextInt();
        String zero = "O", cross = "X";
        String team1entry = "", team2entry = "";
        if (sign == 1) {
            team1entry = zero;
            team2entry = cross;
        } else if (sign == 2) {
            team1entry = cross;
            team2entry = zero;
        }
        //Matrix index logic
        int flag = 0;
        String arr[] = {
            "[ 1 ]",
            "[ 2 ]",
            "[ 3 ]",
            "[ 4 ]",
            "[ 5 ]",
            "[ 6 ]",
            "[ 7 ]",
            "[ 8 ]",
            "[ 9 ]"
        };
        System.out.println();
        for (int i = 0; i < arr.length; i++) {

            if (i == 3) {
                System.out.println();
            }
            if (i == 6) {
                System.out.println("");
            }
            for (int space = 0; space < 12; space++) {
                System.out.print(" ");
            }
            System.out.print(arr[i]);

        }
        System.out.println();
        System.out.println();
        int round = 1;
        int ch = 1;
        int tScore1 = 0, tScore2 = 0;
        while (ch == 1) {

            System.out.println("\t==========================================================");
            System.out.println("\t\t\t\tROUND - " + round++);
            System.out.println("\t==========================================================");
            //game logic
            System.out.print("\tPlayer 1 : " + team1 + " (" + team1entry + ")");
            System.out.print("\t\t\tPlayer 2 : " + team2 + " (" + team2entry + ")");
            System.out.println();
            System.out.println();
            System.out.print("\tScore : " + tScore1);
            System.out.print("\t\t\t\tScore : " + tScore2);
            System.out.println();
            System.out.println();


            String tarr[] = {
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            };

            for (int i = 0; i < tarr.length; i++) {

                if (i == 3) {
                    System.out.println();
                }
                if (i == 6) {
                    System.out.println("");
                }
                for (int space = 0; space < 12; space++) {
                    System.out.print(" ");
                }
                System.out.print("[ " + tarr[i] + "  ]");

            }
			
			
            int count = 0;

            int choice = 1;
            while (choice == 1) {
                count = ++count;
                System.out.println("\n");
                System.out.print("\t" + team1 + " Enter your position number : ");
                int index = sc.nextInt();

                tarr[index - 1] = team1entry;
                System.out.println();
                for (int i = 0; i < tarr.length; i++) {

                    if (i == 3) {
                        System.out.println();
                    }
                    if (i == 6) {
                        System.out.println("");
                    }
                    for (int space = 0; space < 12; space++) {
                        System.out.print(" ");
                    }
                    //System.out.print(String.format("%-5s","[ "+tarr[i]+" ]"));
                    System.out.print(String.format("%-4s", "[ " + tarr[i]));
                    System.out.print(String.format("%-1s", "]"));

                }
                if (tarr[0] == team1entry && tarr[1] == team1entry && tarr[2] == team1entry ||
                    tarr[0] == team1entry && tarr[3] == team1entry && tarr[6] == team1entry ||
                    tarr[3] == team1entry && tarr[4] == team1entry && tarr[5] == team1entry ||
                    tarr[6] == team1entry && tarr[7] == team1entry && tarr[8] == team1entry ||
                    tarr[1] == team1entry && tarr[4] == team1entry && tarr[7] == team1entry ||
                    tarr[2] == team1entry && tarr[5] == team1entry && tarr[8] == team1entry ||
                    tarr[0] == team1entry && tarr[4] == team1entry && tarr[8] == team1entry ||
                    tarr[2] == team1entry && tarr[4] == team1entry && tarr[6] == team1entry) {
                    System.out.println("\n\n\t " + team1 + " is a Winner !!!");
                    tScore1 = tScore1 + 1;
                    flag = 1;
                    break;
                }
                if (count == 9) {
                    System.out.println("\n\n\tIts a Draw !!!");
                    break;
                }

                int choice1 = 1;
                while (choice1 == 1) {
                    
                    System.out.println("\n");
                    if(count==1)
					{
						if(index==1||index==3||index==7||index==9)
						{
							tarr[4] = team2entry;
						}
						else if(index==5||index==6||index==8)
						{
							tarr[8] = team2entry;
						}
						else if(index==2)
						{
							tarr[2] = team2entry;
						}
						else if(index==4)
						{
							tarr[6] = team2entry;
						}
                    }
					
					
						
                    System.out.println();

                    for (int i = 0; i < tarr.length; i++) {

                        if (i == 3) {
                            System.out.println();
                        }
                        if (i == 6) {
                            System.out.println("");
                        }
                        for (int space = 0; space < 12; space++) {
                            System.out.print(" ");
                        }
                        //System.out.print(String.format("%-5s","[ "+tarr[i]+" ]"));
                        System.out.print(String.format("%-4s", "[ " + tarr[i]));
                        System.out.print(String.format("%-1s", "]"));

                    }
                    if (tarr[0] == team2entry && tarr[1] == team2entry && tarr[2] == team2entry ||
                        tarr[0] == team2entry && tarr[3] == team2entry && tarr[6] == team2entry ||
                        tarr[3] == team2entry && tarr[4] == team2entry && tarr[5] == team2entry ||
                        tarr[6] == team2entry && tarr[7] == team2entry && tarr[8] == team2entry ||
                        tarr[1] == team2entry && tarr[4] == team2entry && tarr[7] == team2entry ||
                        tarr[2] == team2entry && tarr[5] == team2entry && tarr[8] == team2entry ||
                        tarr[0] == team2entry && tarr[4] == team2entry && tarr[8] == team2entry ||
                        tarr[2] == team2entry && tarr[4] == team2entry && tarr[6] == team2entry) {
                        System.out.println("\n\n\t " + team2 + " is a Winner !!!");
                        tScore2 = tScore2 + 1;
                        choice = 0;
                        flag = 2;
                        break;
                    }
                    choice1 = 0;
					count = ++count;
                }


            }

            System.out.println();
            System.out.println();
            System.out.print("\tWant to continue Enter 1(YES)/0(NO) : ");
            ch = sc.nextInt();
        }


        System.out.println("\n");
        if (tScore1 == tScore2) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t SCORES ARE TIED ");
            System.out.println("\t==========================================================");
        } else if (flag == 1) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t CONGRATULATIONS ");

            System.out.println("\t\t\t" + team1 + " is a Winner !!!");
            System.out.println("\t==========================================================");
        } else if (flag == 2) {
            System.out.println("\t==========================================================");
            System.out.println("\t\t\t CONGRATULATIONS ");

            System.out.println("\t\t\t" + team2 + " is a Winner !!!");
            System.out.println("\t==========================================================");
        }


    }
}