console.log('tutorial 12')
//element selector
/*
1.single element selector
2.multi element selector


*/

let element=document.getElementById('first')
/* element = element.className
element = element.childNodes
element = element.parentNode */
element.style.color='red'
element.innerText='amey patekar'
element.innerHTML='<h1>amey</h1>'
console.log(element.innerText)



let sel= document.querySelector('#first')
sel=document.querySelector('.child')
sel=document.querySelector('h1')
sel=document.querySelector('div')

console.log(sel)


//2.multi element selector
let elements = document.getElementsByClassName('child')
elements=document.getElementsByClassName('.container')
elements=document.getElementsByTagName('div')

console.log(elements)

Array.from(elements).forEach(  elements => {

    console.log(elements)
    elements.style.color = 'red'
})