//primitive data types
//String
var name='amey';
let lname="patekar";
console.log("my name is "+name,lname);
console.log("my data type is "+(typeof name));

//numbers
let marks=100;
console.log('data type is '+(typeof marks));

//boolean
let isDriver = true
console.log('data type is '+(typeof isDriver));

//null
let nullVar = null;
console.log('data type is '+(typeof nullVar)); //object but it is primitive

//undefined
let undef= undefined;
console.log('data type is '+(typeof undef));

//reference data types
//arrays
let myarr = [1,2,3,4,false,"string"];
console.log("Data type is "+(typeof myarr));

//object literals
let stMarks = {
    amey: 100,
    onkar:90,
    roshan:80
}

console.log(stMarks) //{amey: 100, onkar: 90, roshan: 80}
console.log(typeof stMarks) //object

//function
function findName() {
    
}
console.log(typeof findName) //function

//date
let date=new Date;
console.log(typeof date) // object