console.log('tutorial 6')

let marks=[50,65,85,95,15]
const fruits= ['orange','apple','pineapple']
const mixed = ['str',89,[3,5]]

const arr = new Array(23,125,32,'Orange')
console.log(marks)
console.log(arr)
console.log(mixed)
console.log(fruits)

console.log(arr.length) //4
console.log(Array.isArray(arr)) //true
arr[0] = 'harry';
console.log(arr);


let val = marks.indexOf(65);
console.log(val) //1

//mutating or modifying arrays
marks.push(100)
console.log(marks)

marks.pop()
console.log(marks)

marks.shift()
console.log(marks)

marks.splice(1,2)
console.log(marks)

marks.reverse(marks)
console.log(marks)

let marks2=[10,20,30]
marks= marks.concat(marks2)
console.log(marks)


let myobj = {
    name : 'amey',
    channel : 'code with harry',
    isActive : true,
    marks: [1,2,3,4]
}
console.log(myobj)

console.log(myobj.marks)
console.log(myobj['channel'])