//type conversion and type coercion

console.log('welcome to console')

let myVar;
myVar = 28;
console.log(myVar,typeof myVar)

myVar=String(28);
console.log(myVar,typeof myVar)

let booleanVar= true;
console.log(booleanVar,typeof booleanVar)
 booleanVar= String(true)
 console.log(booleanVar,typeof booleanVar)


 let date=new Date();
 console.log(date,typeof date)
 date = String(new Date);
 console.log(date,typeof date)


 let arr = [1,2,3,4,5]
 console.log(arr,typeof arr,arr.length)
arr = String([1,2,3,4,5])
 console.log(arr,typeof arr,arr.length)


 let i = 8;
 console.log(i.toString())  //convert to string

 let stri = "2464"
 console.log(stri,typeof stri)
 stri = Number("2464")
 stri = Number(true)
 console.log(stri,typeof stri) //2464 'number'


 //parseint
 let number = '28'
 console.log(number, (typeof number))
number = parseInt('28.25')
 console.log(number, (typeof number)) //28 'number'
 number = parseFloat('28.25')
 console.log(number, (typeof number)) //28.25 'number'
 number = parseFloat('28.2565465')
 console.log(number.toFixed(3), (typeof number)) //28.257 number

 //Type coercion
  let mystr = "698"
  let mynum = 34
  console.log(mystr+mynum)

 mystr = 698
 mynum = 34
  console.log(mystr+mynum)