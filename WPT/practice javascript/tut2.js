console.log('tut2')
// variables in js
// var, let, const

var name = 'amey';
var channel;
var marks = 28
marks = 100
channel = 'codewith harry'
console.log(name,channel,marks)

//rules for creating javascript variables
//1.cannot start with numbers
//2.can start with letter,numbers,$,_
//3.case sensetive

var city = 'kankavli'
console.log(city)

const myName= 'amey patekar'
//myname = 'amey' updation not allowed
console.log(myName)
//const fruit;   error
const fruit= 'mango'
console.log(fruit)

{
    let city= 'mumbai'
    city= 'panjim'
    console.log(city)  //panjim
}
console.log(city)  //kankavli

const arr1=[10,20,30,40];
//arr1=[10]   error
arr1.push(50);
console.log(arr1)


/*most common programming case types
1.camelCase
2.kebab-case
3.snake_case
4.PascalCase


*/

