console.log('tutorial8')

// general loops :for, while, dowhile

/* for(let i=0;i<100;i++)
{
    console.log(i)
}

let j=0;
while (j<100) {
    console.log(j)
    j++;
} */


let k = 0
/* do {
    if(k===5)
    {
        break;
    }
    console.log(k++)
} while (k<10); */

/* do {
    if(k===8)
    {
        k=k+1
        continue;
    }
    console.log(k++)
} while (k<10); */

//for each loop
let arr = [2,5,15,52,32,54]
arr.forEach(function(element){
    console.log(element)
})

arr.forEach(element => {
    console.log(element)
});

let obj = {
    name : 'amey',
    age : 28,
    type: 'dangerous programmer',
    os: "ubuntu"
}

for(let key in obj)
{
    console.log(`${key} of object is ${obj[key]}`)
}


arr.forEach(function(element,index,array){
    console.log(element,index,array)
})