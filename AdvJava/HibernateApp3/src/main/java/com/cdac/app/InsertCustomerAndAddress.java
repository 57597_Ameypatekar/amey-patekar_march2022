package com.cdac.app;

import com.cdac.dao.AddressDao;
import com.cdac.dao.CustomerDao;
import com.cdac.entity.Address;
import com.cdac.entity.Customer;

public class InsertCustomerAndAddress {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//insert method 
				Address a=new Address();
				a.setCity("Kankavli");
				a.setPincode(416602);
				a.setState("Maharashtra");
				
				AddressDao a_dao = new AddressDao();
				a_dao.add(a);
				
				
				Customer c=new Customer();
				
				c.setName("Amey Patekar");
				c.setEmail("ameypatekar28@gmail.com");
				c.setAddress(a);						
				CustomerDao c_dao=new CustomerDao();
				c_dao.add(c);
				
				
				
				
	}

}
