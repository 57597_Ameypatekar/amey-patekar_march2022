package com.cdac.app;

import java.time.LocalDate;
import java.util.List;

import com.cdac.dao.AlbumSongDao;
import com.cdac.dao.GenericDao;
import com.cdac.entity.Album;
import com.cdac.entity.Song;

public class AlbumSongExample {

	public static void main(String[] args) {
		GenericDao dao = new GenericDao();
		//AlbumSongDao dao = new AlbumSongDao();
		
		//adding an album
		/*Album album = new Album();
		album.setName("Hits of 2021");
		album.setReleaseDate(LocalDate.of(2021, 12, 30));
		album.setCopyright("Sony Music");
		dao.save(album);*/
		
		//adding song to an album
		/*Album album1 = (Album) dao.fetchById(Album.class, 2);
		Song song = new Song();
		song.setTitle("xyz");
		song.setArtist("Shakira");
		song.setDuration(4.30);
		song.setAlbum(album1);
		dao.save(song);*/
		
		List<Song> songs =  ((AlbumSongDao) dao).fetchSongsSungBy("Taylor Swift");
		for(Song song : songs)
			System.out.println(song.getTitle() + " " + song.getDuration()); 
		
		//dao.delete(Song.class, 8);
	}
}
