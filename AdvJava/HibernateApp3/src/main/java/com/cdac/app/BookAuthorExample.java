package com.cdac.app;

import java.util.ArrayList;
import java.util.List;

import com.cdac.dao.GenericDao;
import com.cdac.entity.Author;
import com.cdac.entity.Book;

public class BookAuthorExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GenericDao dao=new GenericDao();
		
	/*Author author = new Author();
		author.setName("Elisabeth Robson");
		author.setEmail("elis@gmail.com");
		dao.save(author);*/
		
		/*Book book = new Book();
		book.setName("Head First Design Pattern");
		book.setCost(2500);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add((Author) dao.fetchById(Author.class, 1));
		authors.add((Author) dao.fetchById(Author.class, 2));
	
		book.setAuthors(authors);
		dao.save(book);*/
				
		
		/*Book book = new Book();
		book.setName("Head First JavaScript");
		book.setCost(1500);
		dao.save(book);*/
		
		/*Book book=(Book) dao.fetchById(Book.class, 2);
		List<Author> authors =  new ArrayList<Author>();
		authors.add((Author) dao.fetchById(Author.class, 1));
		authors.add((Author) dao.fetchById(Author.class, 2));
		
		book.setAuthors(authors);
		dao.save(book);*/
		
		Book book = new Book();
		book.setName("Groovy 2 Cookbook");
		book.setCost(3000);
		
		Author author = new Author();
		author.setName("Andrey Adamovich");
		author.setEmail("andrey@gmail.com");
		
		Author author2 = new Author();
		author2.setName("Luciano Flandesio");
		author2.setEmail("luciano@gmail.com");
		
		List<Author> list=new ArrayList<Author>();
		list.add(author2);
		list.add(author);
		
		book.setAuthors(list);
				
		dao.save(book);
		
	}

}
