package com.cdac.app;

import java.time.LocalDate;
import java.util.List;

import com.cdac.dao.AddressDao;
import com.cdac.dao.CustomerDao;
import com.cdac.dao.PersonPassportDao;
import com.cdac.entity.Address;
import com.cdac.entity.Customer;
import com.cdac.entity.Passport;
import com.cdac.entity.Person;

public class PersonPassportExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//insert method 
		PersonPassportDao dao = new PersonPassportDao();
		
		/*Person p = new Person();
		p.setName("Amey");
		p.setEmail("ameypatekar@gmail.com");
		p.setDateOfBirth(LocalDate.of(1994, 5, 28));
		
		Passport ps = new Passport();
		ps.setIssueDate(LocalDate.of(2020, 1, 1));
		ps.setExpiryDate(LocalDate.of(2030, 12, 31));
		ps.setIssuedBy("Government of INDIA");
				
		p.setPassport(ps);
		ps.setPerson(p);
		
		dao.add(p);*/
		
		/*List<Person> list=dao.fetchDateOfBirhtByName("Amey");
		
		for(Person p : list)
			System.out.println(p.getDateOfBirth());*/
		
		Passport p =dao.fetchPassportByPersonName("Amey");
		System.out.println(p.getPassportNo()+" "+p.getExpiryDate()+" "+p.getIssueDate()+" "+p.getIssuedBy());
		
				
				
	}

}
