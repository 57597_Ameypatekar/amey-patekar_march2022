package com.cdac.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.cdac.entity.Address;
import com.cdac.entity.Customer;

public class AddressDao {
	public void add(Address add) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("learning-hibernate");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(add); // persist method will generate insert query
		tx.commit();

	}

	public Address fetchAddress(int id) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("learning-hibernate");
		EntityManager em = emf.createEntityManager();

		Address add = em.find(Address.class, id);

		emf.close();
		return add;

	}

	public List<Address> fetchAddressByName(String name) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("learning-hibernate");
		EntityManager em = emf.createEntityManager();

		Query q = em.createQuery("select a from Address a where a.name like :nm");
		q.setParameter("nm", name);

		List<Address> list = (List<Address>) q.getSingleResult();

		emf.close();
		return list;
	}
	
	
	public Address fetchAddressByCustomerName(String name) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("learning-hibernate");
		EntityManager em = emf.createEntityManager();
		
		Query q= em.createQuery("select a from Customer c join c.address a where c.name = :nm");
		q.setParameter("nm",name);
		Address addr=(Address) q.getSingleResult();
		
		emf.close();
		return addr;
		
	}
	
}











