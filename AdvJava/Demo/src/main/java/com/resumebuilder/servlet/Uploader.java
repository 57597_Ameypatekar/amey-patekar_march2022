package com.resumebuilder.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;


@WebServlet("/Uploader")
@MultipartConfig(maxFileSize = 16177215) 
public class Uploader extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			
			Part filePart = request.getPart("photo");
			System.out.println("hello "+filePart);
			InputStream inputStream=null;
				
			if(filePart != null) {
				long fileSize = filePart.getSize();
				String fileContentString = filePart.getContentType();
				inputStream =  filePart.getInputStream();
			}
			
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ameypatekar_20_jh", "root", "cdac");
			PreparedStatement st = conn.prepareStatement("insert into tbl_image(photo) values(?)");
			
			st.setBlob(1, inputStream);
			
			st.executeUpdate();
			
			PrintWriter out = response.getWriter();
			out.write("<html><body>");
			out.write("<h1>Registration successful!</h1>");
			out.write("</body></html>");
			
	}catch (Exception e) {
		// TODO: handle exception
	}

}
}