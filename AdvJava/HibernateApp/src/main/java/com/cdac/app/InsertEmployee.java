package com.cdac.app;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.cdac.dao.EmployeeDao;
import com.cdac.entity.Employee;
public class InsertEmployee {
	public static void main(String[] args) {
		//insert method 
		/*Employee emp = new Employee();
		emp.setEmpno(1003);
		emp.setName("Shewta");
		emp.setSalary(12000);
		emp.setDateOfJoining(LocalDate.of(2021, 10, 10));
		
		EmployeeDao dao = new EmployeeDao();
		dao.add(emp);*/
		
		
		//fetch method 
		/*
		EmployeeDao dao = new EmployeeDao();
		Employee emp=dao.fetch(1003);
		System.out.println(emp.getName()+" "+emp.getSalary()+" "+emp.getDateOfJoining());
		*/
		
		//fetchAll
		EmployeeDao dao = new EmployeeDao();
		List<Employee> list=dao.fetchAll();
		for(Employee emp:list)
			System.out.println(emp.getEmpno()+" "+emp.getName()+" "+emp.getSalary()+" "+emp.getDateOfJoining());
	
		List<String> listNames=dao.fetchAllNames();
		System.out.println("Names : ");
		for(String name:listNames)
			System.out.println(name);
		
		List<Object[]> listNameAndSal=dao.fetchAllNameAndSalary();
		System.out.println("Names and Salary : ");
		for(Object[] arr:listNameAndSal)
			System.out.println(arr[0]+" "+arr[1]);
			
	
		System.out.println("==============");
		double sal=10000;
		List<Employee> listNameBySal=dao.fetchAllBySalary(sal);
		System.out.println("Names By Sal: "+sal);
		for(Employee emp:listNameBySal)
			System.out.println(emp.getEmpno()+" "+emp.getName()+" "+emp.getSalary()+" "+emp.getDateOfJoining());
		
	}
}
	
	



















	/*public static void main(String[] args) {
		//During this step, the persistence.xml file will be read
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("learning-hibernate");
		EntityManager em = emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		
		Employee emp = new Employee();
		
		emp.setName("Vipul");
		emp.setSalary(10000);
		emp.setDateOfJoining(LocalDate.of(2019, 10, 10));
		em.persist(emp); //persist method will generate insert query
		
		
        
        tx.commit();
        
       
		emf.close();
	}
}
*/



