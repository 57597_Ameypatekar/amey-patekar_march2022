package com.cdac.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.cdac.service.LoginService;

@WebServlet(urlPatterns = "/login.cdac", loadOnStartup = 1)
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		String uname, pass;
		int flag = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ameypatekar_20_jh", "root",
					"cdac");
			String str = "select * from tbl_customer";
			Statement st = conn.createStatement();
			ResultSet set = st.executeQuery(str);

			while (set.next()) {

				uname = set.getString(5);
				pass = set.getString(6);
				byte[] decodedBytes = Base64.getDecoder().decode(pass);
				String decodedString = new String(decodedBytes);
				if (username.equals(uname) && password.equals(decodedString)) {
					flag = 1;
					break;
				}

			} // end of while loop

			if (flag == 1) {
				response.sendRedirect("welcome.html");
			} else {
				response.sendRedirect("login.html");
			}

			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
