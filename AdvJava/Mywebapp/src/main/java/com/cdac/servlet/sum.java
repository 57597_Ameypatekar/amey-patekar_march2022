package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class sum
 */
@WebServlet("/sum")
public class sum extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String n1=request.getParameter("num1");
		String n2=request.getParameter("num2");
		int num1=Integer.parseInt(n1);
		int num2=Integer.parseInt(n2);
		
		PrintWriter out = response.getWriter();
		
		out.write("<html><body>");
		out.write("<h1>Sum is " + (num1+num2) + "</h1>");
		out.write("</body></html>");
	}

	
}
