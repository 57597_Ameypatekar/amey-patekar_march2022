package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/age")
public class age extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				
		String str=request.getParameter("date");
		
		LocalDate now=LocalDate.now();
		LocalDate birthDate = LocalDate.parse(str);
		Period p=Period.between(birthDate, now);

		PrintWriter out = response.getWriter();
		
		out.write("<html><body>");
		out.write("<h1>Age is " + p.getYears() +" years  "+p.getMonths()+" months "+p.getDays()+" days old"+ "</h1>");
		out.write("</body></html>");
	}
}
