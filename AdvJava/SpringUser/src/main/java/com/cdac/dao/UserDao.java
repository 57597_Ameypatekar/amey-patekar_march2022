package com.cdac.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.cdac.component.User;

@Component
public class UserDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public void addUser(User user) {
		em.persist(user);
	}
	
	@Transactional
	public void updateUser(User user) {
		em.merge(user);
	}
	
	@Transactional
	public void removeUser(Class clazz, Object pk) {
		Object obj = em.find(clazz, pk);
		em.remove(obj);
	}
}
