package com.cdac.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import com.cdac.entity.Registration;

@Component
public class ResumeBuilderDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Transactional
	public void save(Registration reg) {
		entityManager.persist(reg);
	}
}
