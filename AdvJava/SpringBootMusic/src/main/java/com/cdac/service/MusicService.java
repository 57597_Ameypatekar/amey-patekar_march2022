package com.cdac.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdac.entity.Album;
import com.cdac.entity.Song;
import com.cdac.repository.AlbumRepository;
import com.cdac.repository.SongRepository;

@Service
@Transactional
public class MusicService {

	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private SongRepository songRepository;
	
	public void save(Album album) {
		albumRepository.save(album);
	}
	
	public void save(Song song) {
		songRepository.save(song);
	}
	
	public Album fetchAlbum(int id) {
		return albumRepository.findById(id).get();
	}
	
	public Song fetchSong(int id) {
		return songRepository.findById(id).get();
	}
	
	public void deleteAlbum(int id) {
		albumRepository.deleteById(id);
	}
	
	public void deleteSong(int id) {
		songRepository.deleteById(id);
	}
}
