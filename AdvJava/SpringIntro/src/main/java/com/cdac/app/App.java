package com.cdac.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cdac.component.Atm;
import com.cdac.component.Calculator;
import com.cdac.component.Car;
import com.cdac.component.CurrencyConverter;
import com.cdac.component.HelloWorld;
import com.cdac.component.LoginService;
import com.cdac.component.TextEditor;
import com.cdac.component.bank.Atm2;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Loading Spring/IOC Container (Inversion of control container)
		ApplicationContext ctx = new ClassPathXmlApplicationContext("my-spring-config.xml");
		
		//Accessing a Particular bean
		HelloWorld hw=(HelloWorld) ctx.getBean("hello");
		System.out.println(hw.sayHello("Amey"));
		
		//But why are we using Spring to create object of HelloWorld class?
		//we could have created object on our own like this:
		//HelloWorld hw=new HelloWorld();
		
		Calculator c = (Calculator) ctx.getBean("calc");
		System.out.println(c.add(10, 20));
		System.out.println(c.sub(10, 20));
	
		CurrencyConverter cc = (CurrencyConverter) ctx.getBean("currencyConv");
		System.out.println("1000 USD to INR conversion : "+cc.convert("USD", "INR", 1000));
		
		LoginService loginServ=(LoginService) ctx.getBean("loginServ");
		System.out.println("Valid user : "+loginServ.isValidUser("amey","1234"));
		
		TextEditor te = (TextEditor) ctx.getBean("txtEdtr");
		te.load("abc.txt");
		
		Car car=(Car) ctx.getBean("car");
		car.drive();
		
		Atm2 atm=(Atm2) ctx.getBean("hdfcAtm-v2");
		atm.withdraw(20202020, 5000);
	
	}

}
