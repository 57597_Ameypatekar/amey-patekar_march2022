package com.cdac.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("txtEdtr")
public class TextEditor {
	
	@Autowired //DI (Dependency Injection)
	private SpellChecker sp;
	public void load(String document) {
		System.out.println("some code here for loading "+document);
		
		
	    sp.checkSpellingMistakes(document);
	}
}
