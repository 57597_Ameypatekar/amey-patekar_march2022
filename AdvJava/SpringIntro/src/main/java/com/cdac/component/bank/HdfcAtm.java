package com.cdac.component.bank;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component ("hdfcAtm-v2")
public class HdfcAtm implements Atm2{

	@Autowired
	private List<Bank2> banks;
	
	public void withdraw(int acno, double amount) {
		System.out.println("Customer at HdfcAtm wants to withdraw money ...");
		
		
		Bank2 currentBank = null;
		for(Bank2 bank : banks) {
			if(bank.isAccountPresent(acno)) {
				currentBank = bank;
				currentBank.withdraw(12345, acno, amount);
				
			}
		}
		//currentBank.withdraw(12345, acno, amount);
	}

}
