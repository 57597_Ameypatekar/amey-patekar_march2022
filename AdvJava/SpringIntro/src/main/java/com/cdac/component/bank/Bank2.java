package com.cdac.component.bank;

public interface Bank2 {

	public boolean isAccountPresent(int acno);
	public void withdraw(int atmId, int acno, double amount);
}
