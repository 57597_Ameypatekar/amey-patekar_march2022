package com.cdac.repository;
import org.springframework.data.repository.CrudRepository;
import com.cdac.entity.Song;
public interface SongRepository extends CrudRepository<Song, Integer>{}