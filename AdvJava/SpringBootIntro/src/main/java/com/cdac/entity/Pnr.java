package com.cdac.entity;

import java.time.LocalDate;
import java.util.List;

public class Pnr {
	
	private int pnrNo;
	private int trainNo;
	private LocalDate traveLocalDate;
	private List<Passenger> passengers;
	
	public LocalDate getTraveLocalDate() {
		return traveLocalDate;
	}
	public void setTraveLocalDate(LocalDate traveLocalDate) {
		this.traveLocalDate = traveLocalDate;
	}
	
	public int getPnrNo() {
		return pnrNo;
	}
	public void setPnrNo(int pnrNo) {
		this.pnrNo = pnrNo;
	}
	public int getTrainNo() {
		return trainNo;
	}
	public void setTrainNo(int trainNo) {
		this.trainNo = trainNo;
	}
	public List<Passenger> getPassengers() {
		return passengers;
	}
	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}
	
	
}
