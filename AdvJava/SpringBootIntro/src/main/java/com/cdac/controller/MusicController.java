package com.cdac.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cdac.entity.Album;
import com.cdac.service.MusicService;

@RestController
public class MusicController {

	@Autowired
	private MusicService musicService;
	
	@PostMapping("/add-album")
	public String addAlbum(@RequestBody Album album) {
		musicService.save(album);
		return "Album added successfully!";
	}
	
}
