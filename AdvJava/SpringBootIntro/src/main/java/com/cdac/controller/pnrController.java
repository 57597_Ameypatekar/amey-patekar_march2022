package com.cdac.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cdac.entity.Passenger;
import com.cdac.entity.Pnr;
import com.cdac.entity.Passenger.Gender;
import com.cdac.entity.Passenger.Status;

//Assume this api has been developed by irctc
@RestController
//@CrossOrigin("http://localhost:3000")
public class pnrController {

	@RequestMapping("/pnr-status.irctc")
	public Pnr getPnrDetails(@RequestParam("pnrNo") int pnrNo) {
		
		Pnr pnr = new Pnr();
		pnr.setPnrNo(pnrNo);
		pnr.setTrainNo(1111);
		pnr.setTraveLocalDate(LocalDate.of(2022, 7, 20));
		
		List<Passenger> list = new ArrayList<>();
		
		Passenger p1=new Passenger();
		p1.setGender(Gender.MALE);
		p1.setName("Amey");
		p1.setStatus(Status.CONFIRMED);
		list.add(p1);
		
		Passenger p2=new Passenger();
		p2.setGender(Gender.MALE);
		p2.setName("Akshay");
		p2.setStatus(Status.RAC);
		list.add(p2);
		
		pnr.setPassengers(list);
		return pnr;
	
	}
}
