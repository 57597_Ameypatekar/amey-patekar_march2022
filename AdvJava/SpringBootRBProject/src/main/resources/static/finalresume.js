$(() => {
    console.log("hello  dom");
    let userid=1;
    $("#btnClickMe").click(() => {
        console.log("btn click");

        $.ajax({
            url: "http://localhost:8080/getHeader/1",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
                $("#firstName").html(res.first_name+" "+res.last_name);
                $("#Address").html(res.address+"  City :"+res.city+"  State:"+res.state);
                $("#Address2").html(res.address2);
                $("#pincode").html(res.pincode);
                $("#emailid").html(res.email);
                $("#contactnumber").html(res.mobile_number);
                //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getObjective/1",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
                $("#objective").html(res.objective);
               
                //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getEducation",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
                $("#eduID0").html(res[0].id);
                $("#examination0").html(res[0].examination);
                $("#branch0").html(res[0].branch);
                $("#instituteName0").html(res[0].institueName);
                $("#University0").html(res[0].university);
                $("#Passingyear0").html(res[0].passingYear);
                $("#Percentage0").html(res[0].percentage);


                $("#eduID1").html(res[1].id);
                $("#examination1").html(res[1].examination);
                $("#branch1").html(res[1].branch);
                $("#instituteName1").html(res[1].institueName);
                $("#University1").html(res[1].university);
                $("#Passingyear1").html(res[1].passingYear);
                $("#Percentage1").html(res[1].percentage);

                $("#eduID2").html(res[2].id);
                $("#examination2").html(res[2].examination);
                $("#branch2").html(res[2].branch);
                $("#instituteName2").html(res[2].institueName);
                $("#University2").html(res[2].university);
                $("#Passingyear2").html(res[2].passingYear);
                $("#Percentage2").html(res[2].percentage);

                $("#eduID3").html(res[3].id);
                $("#examination3").html(res[3].examination);
                $("#branch3").html(res[3].branch);
                $("#instituteName3").html(res[3].institueName);
                $("#University3").html(res[3].university);
                $("#Passingyear3").html(res[3].passingYear);
                $("#Percentage3").html(res[3].percentage);

                $("#eduID4").html(res[4].id);
                $("#examination4").html(res[4].examination);
                $("#branch4").html(res[4].branch);
                $("#instituteName").html(res[4].institueName);
                $("#University4").html(res[4].university);
                $("#Passingyear4").html(res[4].passingYear);
                $("#Percentage4").html(res[4].percentage);
                console.log(res);
               
                //alert(res);
            }
        });


        $.ajax({
            url: "http://localhost:8080/getExperience",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
                $("#expId0").html(res[0].id);
                $("#companyName0").html(res[0].company);
                $("#jobTitle0").html(res[0].jobTitle);
                $("#expCity0").html(res[0].city);
                $("#expcountry0").html(res[0].country);
                $("#expStartDate0").html(res[0].startDate);
                $("#expEndDate0").html(res[0].endDate);

                $("#expId1").html(res[1].id);
                $("#companyName1").html(res[1].company);
                $("#jobTitle1").html(res[1].jobTitle);
                $("#expCity1").html(res[1].city);
                $("#expcountry1").html(res[1].country);
                $("#expStartDate1").html(res[1].startDate);
                $("#expEndDate1").html(res[1].endDate);
                console.log(res);
               
                //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getSkills",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
               
                    $("#skills0").html(res[0].skills);
                    $("#skills1").html(res[1].skills);
                    $("#skills2").html(res[2].skills);
                    $("#skills3").html(res[3].skills);
                    $("#skills0").html(res[4].skills);
                    console.log(res);
                              //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getLanguages",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
               
                    $("#langauges0").html(res[0].languages);
                    $("#langauges1").html(res[1].languages);
                    $("#langauges2").html(res[2].languages);
                    $("#langauges3").html(res[3].languages);
                    $("#langauges4").html(res[4].languages);
                    console.log(res);
                              //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getHobbies",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
               
                    $("#Hobbies0").html(res[0].hobbies);
                    $("#Hobbies1").html(res[1].hobbies);
                    $("#Hobbies2").html(res[2].hobbies);
                    $("#Hobbies3").html(res[3].hobbies);
                    $("#Hobbies4").html(res[4].hobbies);
                    console.log(res);
                              //alert(res);
            }
        });


        $.ajax({
            url: "http://localhost:8080/getAchievements",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
               
                    $("#achievements0").html(res[0].achievements);
                    $("#achievements1").html(res[1].achievements);
                    $("#achievements2").html(res[2].achievements);
                    $("#achievements3").html(res[3].achievements);
                    $("#achievements4").html(res[4].achievements);
                    console.log(res);
                              //alert(res);
            }
        });

        $.ajax({
            url: "http://localhost:8080/getProjects",
            type: 'GET',
            dataType: 'json', // added data type
            success: function (res) {
                console.log(res);
               
                    $("#projects0").html(res[0].projects);
                    $("#projects1").html(res[1].projects);
                    $("#projects2").html(res[2].projects);
                    $("#projects3").html(res[3].projects);
                    $("#projects4").html(res[4].projects);
                    console.log(res);
                              //alert(res);
            }
        });

    })
});