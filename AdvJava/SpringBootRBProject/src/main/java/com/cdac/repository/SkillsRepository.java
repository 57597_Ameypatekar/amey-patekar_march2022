package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Skills;

public interface SkillsRepository extends CrudRepository<Skills, Integer>{

}
