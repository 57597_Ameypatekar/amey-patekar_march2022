package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Achievements;

public interface AchievementsRepository extends CrudRepository<Achievements, Integer>{

}
