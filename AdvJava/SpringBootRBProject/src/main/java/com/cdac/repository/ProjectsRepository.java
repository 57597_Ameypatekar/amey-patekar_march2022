package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Projects;

public interface ProjectsRepository extends CrudRepository<Projects, Integer>{

}
