package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Experience;


public interface ExperienceRepository extends CrudRepository<Experience, Integer>

{

}
