package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Languages;

public interface LangaugesRepository extends CrudRepository<Languages, Integer>{

}
