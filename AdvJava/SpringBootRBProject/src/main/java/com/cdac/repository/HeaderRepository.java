package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Header;


public interface HeaderRepository extends CrudRepository<Header, Integer>{

}
