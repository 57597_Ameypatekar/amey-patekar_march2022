package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Objective;

public interface ObjectiveRepository extends CrudRepository<Objective, Integer>{

}
