package com.cdac.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import com.cdac.entity.Registration;


public interface RegistrationRepository extends CrudRepository<Registration, Integer>{

		
}
