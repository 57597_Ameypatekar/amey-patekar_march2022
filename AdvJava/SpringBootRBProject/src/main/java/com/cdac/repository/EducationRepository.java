package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Education;

public interface EducationRepository extends CrudRepository<Education, Integer>{

}
