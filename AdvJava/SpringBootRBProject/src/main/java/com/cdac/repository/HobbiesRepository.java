package com.cdac.repository;

import org.springframework.data.repository.CrudRepository;

import com.cdac.entity.Hobbies;

public interface HobbiesRepository extends CrudRepository<Hobbies, Integer>{

}
