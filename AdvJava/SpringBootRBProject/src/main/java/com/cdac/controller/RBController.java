package com.cdac.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cdac.entity.Achievements;
import com.cdac.entity.Education;
import com.cdac.entity.Experience;
import com.cdac.entity.Header;
import com.cdac.entity.Hobbies;
import com.cdac.entity.Languages;
import com.cdac.entity.Objective;
import com.cdac.entity.Projects;
import com.cdac.entity.Registration;
import com.cdac.entity.Skills;
import com.cdac.service.RBService;



@RestController
public class RBController {
	
	@Autowired
	private RBService rbService;
	
	@PostMapping("/registration")
	public String registerUser(@RequestBody Registration register) {
		rbService.save(register);
		return "user register successfully!";
	}
	
	
	@PostMapping("/header")
	public String headerInfo(@RequestBody Header header) {
		rbService.save(header);
		return "information added successfully!";
	}
	
	@PostMapping("/experience")
	public String experienceInfo(@RequestBody Experience experience) {
		rbService.save(experience);
		return "information added successfully!";
	}
	
	@PostMapping("/education")
	public String educationInfo(@RequestBody Education education) {
		rbService.save(education);
		return "information added successfully!";
	}
	
	@PostMapping("/skills")
	public String addSkills(@RequestBody Skills skills) {
		rbService.save(skills);
		return "skills added successfully!";
	}
	
	@PostMapping("/languages")
	public String addLangauges(@RequestBody Languages languages) {
		rbService.save(languages);
		return "languages added successfully!";
	}
	
	@PostMapping("/hobbies")
	public String addHobbies(@RequestBody Hobbies hobbies) {
		rbService.save(hobbies);
		return "hobbies added successfully!";
	}
	
	
	@PostMapping("/objective")
	public String addObjective(@RequestBody Objective objective) {
		rbService.save(objective);
		return "objective added successfully!";
	}
	
	@PostMapping("/achievements")
	public String addAchievements(@RequestBody Achievements achievements) {
		rbService.save(achievements);
		return "achievement added successfully!";
	}
	
	@PostMapping("/projects")
	public String addProjects(@RequestBody Projects projects) {
		rbService.save(projects);
		return "project added successfully!";
	}
	
	@GetMapping("/getdetails/{registration_id}")
	public Registration fetchdetails(@PathVariable("registration_id") int id) {
		
		Registration registration = rbService.fetch(id);
		
		return registration;
		
	}
	
	@GetMapping("/getHeader/{id}")
	public Header fetchHeader(@PathVariable("id") int id) {
		Header header=rbService.fetchHeaderDetails(id);
		return header;
	}
	
	@GetMapping("/getExperience")
	public List<Experience> fetchExperience() {
		
		return rbService.fetchExperienceDetails();
	}
	
	@GetMapping("/getEducation")
	public List<Education> fetchEducation() {
		
		return rbService.fetchEducationDetails();
	}
	
	@GetMapping("/getSkills")
	public List<Skills> fetchSkills() {
		
		return rbService.fetchSkillsDetails();
	}
	
	@GetMapping("/getLanguages")
	public List<Languages> fetchLanguages() {
		
		return rbService.fetchLanguagesDetails();
	}
	
	@GetMapping("/getHobbies")
	public List<Hobbies> fetchHobbies() {
		
		return rbService.fetchHobbiesDetails();
	}
	
	@GetMapping("/getObjective/{id}")
	public Objective fetchObjective(@PathVariable("id") int id) {
		Objective objective=rbService.fetchObjectiveDetails(id);
		return objective;
	}
	
	@GetMapping("/getAchievements")
	public List<Achievements> fetchAchievements() {
		
		return rbService.fetchAchievementsDetails();
	}
	
	@GetMapping("/getProjects")
	public List<Projects> fetchProjects() {
		
		return rbService.fetchProjectsDetails();
	}
	
	
	
	
	
	
	
	
	
	
	
}







