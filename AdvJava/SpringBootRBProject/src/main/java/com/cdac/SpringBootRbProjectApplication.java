package com.cdac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRbProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRbProjectApplication.class, args);
	}

}
