<%@page import="com.cdac.model.Option"%>
<%@page import="java.util.List"%>
<%@page import="com.cdac.model.Question"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body class="m-5 d-flex justify-content-center " style="background-color:#edfbfe">
	<form  action="CalculateScoreServlet">
	<div class="mb-3 row">
	<h1>Java Quiz</h1>
	
<%
	Question question = (Question) session.getAttribute("currentQs");
%>
</div>
<div class="mb-3 row">
<h3>Q. <%= question.getQuestion() %></h3>
</div>
<%
	List<Option> options = question.getOptions();
	for(int i=0; i<options.size(); i++) {
		Option option = options.get(i);
%>
		<div>
			<input class="form-check-input" type="radio"   name="op" value="<%= i %> " />
			<label  for="btn-check"><%= option.getOption() %></label>
			
		</div>
<%
	}
%>
<br />
<!-- <a href="QuestionLoaderServlet">Next</a> -->
<button class="btn btn-outline-dark" type="submit">Next</button>

</form>
</body>
</html>