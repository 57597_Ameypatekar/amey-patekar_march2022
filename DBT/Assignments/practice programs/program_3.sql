delimiter //
create procedure abc ()
begin
	declare x int default 20 ;
    insert into tempp values (x , 'HELLO') ;
end ; //
delimiter ;

call abc() ;

select * from tempp;
drop procedure abc ;
