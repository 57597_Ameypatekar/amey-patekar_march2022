Delimiter //
Create procedure abc()
Begin 
	Declare x Boolean default TRUE;
	If x then 
		Insert into tempp values (1, 'Mumbai') ;
	End if;
End ; //
Delimiter ;



call abc();
select * from tempp;

drop procedure abc;