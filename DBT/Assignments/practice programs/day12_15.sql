Delimiter //
Create procedure abc ()
Begin 
	Declare x int default 1;
	Pqr_loop: loop
		If x > 10 then
				Leave pqr_loop;
		End if;
		Set x = x+1;
		If mod(x,2) != 0 then
				Iterate pqr_loop;
		Elsedept
			Insert into tempp values (x, 'loop') ;
		End if;
	End loop;
End ; //
Delimiter ;


call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;