Delimiter //
Create procedure abc ()
Begin 
	Declare x int default 1;
    declare y int default 100;
	repeat
			Insert into tempp values (y, 'in loop');
            set y = y + 100;
			Set x = x+1;
	Until x > 10
	End repeat;
End ; //
Delimiter ;


call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;
