Delimiter //
Create procedure abc()
Begin 
	Declare x int;
	Select sal into x from emp where ename = 'KING' ;
	If x > 5000 then 
		Insert into tempp values (x, 'High sal') ;
	Elseif x < 5000 then 
			Insert into tempp values (x, 'Low sal') ;
	else
			Insert into tempp values (x, 'Equal sal') ;
        
	End if;
End ; //
Delimiter ;

call abc();
select * from tempp;

drop procedure abc;