Create table tempp
(
Fir int,
Sec char (15)
);
select * from tempp;

create table EMP
(
ENAME CHAR(20),
SAL FLOAT,
JOB CHAR(20),
DEPTNO INT
);
select * from  EMP;
insert into  EMP values 
('SCOTT',3000,'CLERK',10),
('KING',5000,'MANAGER',20);

delimiter //
create procedure abc()
begin
		declare	x int ;
        select sal into x from EMP
        where ename = 'KING' ;
        /* processing , e.g. set hra = x*0.4, etc */
        insert into tempp values (x, 'KING') ;
end; //
delimiter ;

call abc();
drop procedure abc;
drop table EMP_TABLE;