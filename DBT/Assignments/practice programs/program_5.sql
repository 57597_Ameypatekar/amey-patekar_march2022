delimiter //
create procedure abc ()
begin	
		declare x char(15) default 'KING' ;
        declare y float default 3000 ;
        declare z float default 0.4 ;
        declare hra float ;
        set hra = y*z ;
        insert into tempp values (y, x) ;
        insert into tempp values (hra, 'hra') ;
end ; //
delimiter ;
 
call abc ();

select * from tempp;
drop procedure abc;