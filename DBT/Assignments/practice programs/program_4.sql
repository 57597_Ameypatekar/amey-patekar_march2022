delimiter @
create procedure abc ()
begin
	declare x char(15) default 'CDAC';
    insert into tempp values (2, x) ;
end ; @
delimiter ;

call abc(); 
select * from tempp;
drop procedure abc ;
