Delimiter //
Create procedure abc()
Begin 
	Declare x int;
	Select sal into x from emp where ename = 'KING' ;
	If x > 5000 then 
		Insert into tempp values (x, 'High sal') ;
	Else
		If x < 5000 then 
			Insert into tempp values (x, 'Low sal') ;
        else
			Insert into tempp values (x, 'Equal sal') ;
        end if;
	End if;
End ; //
Delimiter ;

call abc();
select * from tempp;

drop procedure abc;