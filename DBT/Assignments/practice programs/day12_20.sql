Delimiter //
Create procedure abc ()
Begin 
	Declare a int;
	Declare b varchar(15);
	Declare c int;
	Declare d int;
	Declare y int default 0;
    Declare c1 cursor for select * from emp ; 
    declare continue handler for not found set y = 1 ;
   
	Open c1; 
		cursor_c1_loop : loop
			fetch c1 into a,b,c,d;
            if y = 1 then 
				leave cursor_c1_loop;
			end if;
            insert into tempp values (c,b) ;
		end loop cursor_c1_loop;
	Close c1;
	
End ; //
Delimiter ;

call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;