Create table tempp
(
Fir int,
Sec char (15)
);

select * from tempp;

Delimiter //
create procedure abc ()
begin
	insert into tempp values (1,'Hello');
	
end; //
delimiter ;

call abc();
savepoint s1;
rollback work to s1;
truncate table tempp;

drop procedure abc;
show procedure status;
