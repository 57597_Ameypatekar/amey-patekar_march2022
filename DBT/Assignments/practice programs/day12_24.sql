Delimiter //
Create procedure abc ()
Begin 
	Declare a int ;
	Declare b varchar(15);
	Declare c int;
	Declare d int;
    declare e int;
    declare f char(15);
    declare g char(15);
	Declare y int default 0;
	Declare c1 cursor for select * from emp;
	Declare c2 cursor for select * from dept;
	Declare continue handler for not found set y = 1;

		Open c1;
		Open c2;
			cursor_c1_loop : loop
            fetch c1 into a,b,c,d;
            fetch c2 into e,f,g;
            if y = 1 then 
				leave cursor_c1_loop;
			end if ;
            insert into tempp values (c,g);
            end loop cursor_c1_loop ;
		Close c1;
		Close c2;       	
End; //
Delimiter ;


call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;
