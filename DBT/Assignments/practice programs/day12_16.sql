create table emp (
Empno int,
Ename varchar (15),
Sal int,
Deptno int
);

select * from emp;
insert into emp values (1	,'A',	5000,	1), 
(2	,'B',	6000,	1),
(3	,'C',	7000,	1),
(4	,'D',	9000,	2),
(5	,'E',	8000,	2);


Delimiter //
Create procedure abc ()
Begin 
	Declare a int;
	Declare b varchar(15);
	Declare c int;
	Declare d int;
	Declare x int default 0;
	Declare c1 cursor for select * from emp; 
	Open c1; 
		While x < 5 do
				Fetch c1 into a, b, c, d;  
				Insert into tempp values (a, b);
		Set x = x+1;
		End while;
	Close c1;
	
End ; //
Delimiter ;

call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;