delimiter //
create procedure abc(x char(15), y float, z float)
begin
	declare hra float;
    set hra = y*z;
    /* writing formula
    */
    insert into tempp values (y, x) ;
    insert into tempp values (hra, 'HRA');
    -- inserting values
end; //
delimiter ;

call abc ('KONG',4000,0.25) ;
call abc ('SCOTT',5000,0.5) ;
select * from tempp ;
drop procedure abc ;

show procedure status ;
show procedure status where db = 'ameypatekar_20_jh' ;
show procedure status where name like 'a%' ;
show create procedure abc ;
