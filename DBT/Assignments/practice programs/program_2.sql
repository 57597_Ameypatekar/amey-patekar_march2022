delimiter //
create procedure abc()
begin
	declare x int;
    set x = 10 ;
    insert into tempp values (x, 'hello') ;
end ; //
delimiter ;

call abc();

select * from tempp ;
drop procedure abc;