delimiter //
create procedure abc()
begin
        declare x int ;
        declare y char(15) ;
        select sal, job into x, y from EMP
        where deptno = 10 ;
        /* processing , e.g. set hra = x*0.4, set y = lower(y) etc */
        insert into tempp values (x, y) ;
end; //
delimiter ;

call abc () ;
select * from tempp;


drop procedure abc;
drop table tempp;