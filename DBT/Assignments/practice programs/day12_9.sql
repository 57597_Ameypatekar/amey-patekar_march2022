Delimiter //
Create procedure abc()
Begin 
	Declare x Boolean default FALSE;
	If NOT x then 
		Insert into tempp values (1, 'Mumbai') ;
	End if;
End ; //
Delimiter ;



call abc();
select * from tempp;

drop procedure abc;