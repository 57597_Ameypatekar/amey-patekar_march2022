delimiter //
create procedure abc(y char (15))
begin
        declare x int ;
        select sal into x from emp
        where ename = y;
        /* processing , e.g. set hra = x*0.4, etc */
        insert into tempp values (x, y) ;
end; //
delimiter ;


call abc('king');
call abc('scott');
select * from tempp;


drop procedure abc;
