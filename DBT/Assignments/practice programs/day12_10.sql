
Delimiter //
Create procedure abc ()
Begin 
	Declare x int default 1;
	While x < 10 do 
		Insert into tempp values (x, 'in while loop');
		Set x = x+1;
	End while;
End ; //
Delimiter ;


call abc();
select * from tempp;

drop procedure abc;