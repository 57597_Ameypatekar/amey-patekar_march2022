Delimiter //
Create procedure abc()
Begin 
	Declare x int;
	Select sal into x from emp where ename = 'KING' ;
	If x < 4000 then 
		Insert into tempp values (x, 'High sal') ;
	Else
		Insert into tempp values (x, 'Low sal') ;
	End if;
End ; //
Delimiter ;

call abc();
select * from tempp;
