Delimiter //
Create procedure abc ()
Begin 
	Declare a int ;
	Declare b varchar(15);
	Declare c int;
	Declare d int;
	Declare y int default 0;
	Declare c1 cursor for select * from emp where deptno = 1;
	Declare continue handler for not found set y = 1;
	Open c1;
		Cursor_c1_loop: loop
				Fetch c1 into a, b, c, d;
				If y = 1 then 
						Leave cursor_c1_loop;
				End if;
				Insert into tempp values (c, b);
		End loop cursor_c1_loop;
	Close c1;       	
End; //
Delimiter ;



call abc();
select * from tempp;
truncate table tempp;
drop procedure abc;