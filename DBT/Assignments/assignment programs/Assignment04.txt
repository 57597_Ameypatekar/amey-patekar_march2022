SQL Exercise 4
1. Display the minimum Status in the Supplier table.
	mysql> select min(status) from s;
	+-------------+
	| min(status) |
	+-------------+
	|          10 |
	+-------------+
	1 row in set (0.01 sec)
----------------------------------------------------------------------------------------------------	
2. Display the maximum Weight in the Parts table.
	mysql> select max(weight) "MAX" from p;
	+------+
	| MAX  |
	+------+
	|   19 |
	+------+
	1 row in set (0.00 sec)
----------------------------------------------------------------------------------------------------	
3. Display the average Weight of the Parts.
	mysql> select avg(weight) "AVERAGE" from p;
	+--------------------+
	| AVERAGE            |
	+--------------------+
	| 15.166666666666666 |
	+--------------------+
	1 row in set (0.01 sec)
----------------------------------------------------------------------------------------------------
4. Display the total Quantity sold for part ‘P1’.
	mysql> select pid,qty from spj where pid='p1';
	+------+------+
	| pid  | qty  |
	+------+------+
	| p1   |  200 |
	| p1   |  700 |
	| p1   |  100 |
	+------+------+
	3 rows in set (0.00 sec)
----------------------------------------------------------------------------------------------------
5. Display the total Quantity sold for each part.
	mysql> select sum(qty) from spj;
	+----------+
	| sum(qty) |
	+----------+
	|     8500 |
	+----------+
	1 row in set (0.00 sec)
----------------------------------------------------------------------------------------------------
6. Display the average Quantity sold for each part.
	mysql> select pname,avg(qty)"average" from spj,p
		-> where p.pid=spj.pid
		-> group by pname;
	+-------+----------+
	| pname | average  |
	+-------+----------+
	| Nut   | 333.3333 |
	| Screw | 436.3636 |
	| Cam   | 275.0000 |
	| Cog   | 325.0000 |
	| Bolt  | 150.0000 |
	+-------+----------+
	5 rows in set (0.01 sec)
----------------------------------------------------------------------------------------------------
7. Display the maximum Quantity sold for each part, provided the maximum Quantity is not greater than 800.
	mysql> select pname, max(qty) from spj,p
		-> where p.pid=spj.pid
		-> group by pname;
	+-------+----------+
	| pname | max(qty) |
	+-------+----------+
	| Nut   |      700 |
	| Screw |      800 |
	| Cam   |      500 |
	| Cog   |      500 |
	| Bolt  |      200 |
	+-------+----------+
----------------------------------------------------------------------------------------------------
8. Display the Status and the count of Suppliers with that Status.
	mysql> select status, count(sname) from s
		-> group by status;
	+--------+--------------+
	| status | count(sname) |
	+--------+--------------+
	|     20 |            2 |
	|     10 |            1 |
	|     30 |            2 |
	+--------+--------------+
	3 rows in set (0.01 sec)
----------------------------------------------------------------------------------------------------
9. Display the count of Projects going on in different cities.
	mysql> select count(jname) "Project Count", city from j
		-> group by city;
	+---------------+--------+
	| Project Count | city   |
	+---------------+--------+
	|             1 | Paris  |
	|             1 | Rome   |
	|             2 | Athens |
	|             2 | London |
	|             1 | Oslo   |
	+---------------+--------+
	5 rows in set (0.00 sec)
----------------------------------------------------------------------------------------------------
10. What is the difference between COUNT(Status) and COUNT(*) ?
	mysql> select count(status)-count(*) from s;
	+------------------------+
	| count(status)-count(*) |
	+------------------------+
	|                      0 |
	+------------------------+
	1 row in set (0.00 sec)
----------------------------------------------------------------------------------------------------
11. Display the Status and the Count of Suppliers with that Status in the following format as shown below:-
Status Count
Ten 1
Twenty 2
Thirty 3

	mysql> select
    -> case
    -> when status = 10 then 'Ten'
    -> when status = 20 then 'Twenty'
    -> when status = 30 then 'Thirty'
    -> end "STATUS"
    -> ,count(sname) "Count" from s
    -> group by status;
	+--------+-------+
	| STATUS | Count |
	+--------+-------+
	| Twenty |     2 |
	| Ten    |     1 |
	| Thirty |     2 |
	+--------+-------+
----------------------------------------------------------------------------------------------------