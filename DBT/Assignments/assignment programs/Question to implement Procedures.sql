CREATE TABLE author (
    author_id INTEGER PRIMARY KEY,
    authorName VARCHAR(30),
    email VARCHAR(25),
    gender VARCHAR(6)
); 

CREATE TABLE book (
    BookId INTEGER NOT NULL UNIQUE,
    ISBN INTEGER PRIMARY KEY,
    book_name VARCHAR(30) NOT NULL,
    author INTEGER,
    ed_num INTEGER,
    price INTEGER,
    pages INTEGER,
    FOREIGN KEY (author)
        REFERENCES author (author_id)
);

insert into author values (1, "Kraig Muller", "Wordnewton@gmail.com", "Male");
insert into author values (2, "Karrie Nicolette", "karrie23@gmail.com", "Female");
insert into book values (1, 001, "Glimpses of the past", 1, 1, 650, 396); 
insert into book values (2, 002, "Beyond The Horizons of Venus", 1, 1, 650, 396); 
insert into book values (3, 003, "Ultrasonic Aquaculture", 2, 1, 799, 500); 
insert into book values (4, 004, "Cyrogenic Engines", 2, 1, 499, 330);


/*1. Write a Procedure (with no parameters) to display all the books*/
create table tempp
(booknames varchar(50));


delimiter //
create procedure abc()
begin
	select book_name from book ;
end; //
delimiter ;

call abc();
drop procedure abc;

/*2. Write a Procedure to update price of a book taking ISBN of the book and its new price as
input:*/


delimiter //
create procedure abc(x int)
begin
	update book set price = price + x ;
    select * from book;
end ; //
delimiter ;

call abc(120);

/*3. Write a Procedure to display the highest price among all the books with an output
parameter:*/
create table tempp(
max_price int );

delimiter //
create procedure abc()
begin
	declare x int ;
    set x = 
    (select max(price) from book ) ;
    insert into tempp values (x) ;
end ; //
delimiter ;

call abc();

/*4. Write a Procedure to take gender type input (‘Male’/’Female’ here) with an in-out
parameter which reflects the number of authors falling in that gender category/type:*/
delimiter //
create procedure abc(x char(6))
begin
	declare c int ;
	if x = 'male' then
		set c = (select count(*) from author where gender = 'male') ;
		insert into tempp values (c) ;
	else 
		set c = (select count(*) from author where gender = 'female') ;
		insert into tempp values (c) ;
	end if ;
end ; //
delimiter ;

call abc ('female') ;

