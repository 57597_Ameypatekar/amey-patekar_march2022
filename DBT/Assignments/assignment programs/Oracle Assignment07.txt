Assignment –7
Summarizing Data with Aggregate Functions.
1) Write a query that counts all orders for October 3.
	SQL> select count(*) from orders
	  2  where odate='03-oct-90';

	  COUNT(*)
	----------
			 5
2) Write a query that counts the number of different non-NULL city values in the Customers table.
	SQL> select count(*) from customers
	  2  where city is not null;

	  COUNT(*)
	----------
			 7
3) Write a query that selects each customer’s smallest order.
	SQL> select min(amt) "Smallest order", cnum
	  2  from orders
	  3  group by cnum;

	Smallest order       CNUM
	-------------- ----------
			767.19       2001
			  4723       2006
			1900.1       2007
		   5160.45       2003
			 75.75       2004
			 18.69       2008
		   1713.23       2002

	7 rows selected.
4) Write a query that selects the first customer, in alphabetical order, whose name begins with G.
	SQL> select * from customers
	  2  where cname like 'G%'
	  3  order by cname;

		  CNUM CNAME      CITY           RATING       SNUM
	---------- ---------- ---------- ---------- ----------
		  2002 Giovanni   Rome              200       1003
		  2004 Grass      Berlin            300       1002
5) Write a query that selects the highest rating in each city.
	SQL> select max(rating) from customers;

	MAX(RATING)
	-----------
			300
6) Write a query that counts the number of salespeople registering orders for each day. (If a salesperson has more than one order on a given day, he or she should be counted only once.).
	SQL> select count(odate), odate from orders
	  2  group by odate;

	COUNT(ODATE) ODATE
	------------ ---------
			   2 04-OCT-90
			   1 05-OCT-90
			   5 03-OCT-90
			   2 06-OCT-90