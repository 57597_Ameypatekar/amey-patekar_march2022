create table temp (
user_num int,
divisible_by_5 char(5)
);

select * from temp;
drop table temp;

delimiter //
create procedure div_by_5 ( num int)
begin
	declare x char(5) ;
       if num % 5 = 0 then
			insert into temp values ( num,'YES');
       else 
			insert into temp values (num , 'NO');
       end if ;
			
end ; //
delimiter ;

call div_by_5 (10) ;
call div_by_5 (12) ;
call div_by_5 (55) ;
call div_by_5 (61) ;