create table temp (
Fahrenheit float,
Celsius float
);

select * from temp ;

delimiter //
create procedure F_to_C (f float)
begin
	declare c float ;
	set c = (f-32)* 5/9 ;
    insert into temp values (f,c) ;
end; //
delimiter ;

delimiter //
create procedure C_to_F (c float)
begin
	declare f float ;
	set f = 9/5*c + 32  ;
    insert into temp values (f,c) ;
end; //
delimiter ;

call F_to_C (98) ;
call C_to_F (36) ;

drop table temp;


