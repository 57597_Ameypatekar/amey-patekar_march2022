
create table Ord_mst(
ord_no int,
cust_cd char(2),
Status char(2)
);

create table Ord_dtl(
ord_no int,
Prod_cd char(2),
qty int);

create table Prod_mst(
Prod_cd char(2),
Prod_name char(20),
Qty_in_Stock int,
Booked_Qty int);

create table tempp(
fir int,
sec char(10)
);

insert into ord_mst values (1,'C1','P');
insert into ord_dtl values (1,'P1',100), (1,'P2',200);
insert into prod_mst values
('P1','Floppies',10000,1000),
('P2','Printers',5000,600),
('P3','Modems',3000,200);

select * from ord_mst;
select * from ord_dtl;
select * from prod_mst;
select * from tempp;
/*1. Write a Before Insert trigger on Ord_dtl. Anytime a row is inserted in Ord_dtl, the Booked_qty in Prod_mst should be increased accordingly.*/

delimiter //
create trigger abc
before insert
on ord_dtl for each row
begin
	update prod_mst set booked_qty = booked_qty + new.qty 
    where prod_cd= new.prod_cd;
	insert into tempp values(1,'inserted');
end ; //
delimiter ;

insert into ord_dtl values (1,'P1',300);

/*2. Write a Before Delete trigger on Ord_dtl. Anytime a row is deleted from Ord_dtl, the Booked_qty in Prod_mst should be decreased accordingly.*/
delimiter //
create trigger pqr
before delete
on ord_dtl for each row
begin
	update prod_mst set booked_qty = booked_qty - old.qty
    where prod_cd = old.prod_cd ;
	insert into tempp values(1,'deleted');
end ; //
delimiter ;

delete from ord_dtl where prod_cd = 'P2' ; 


/*3. Write a Before Update of Prod_cd, Qty trigger on Ord_dtl. Anytime the Prod_cd or Qty is updated, the Booked_qty in Prod_mst should be increased/decreased accordingly.*/
delimiter //
create trigger xyz
before update
on ord_dtl for each row
begin
	update prod_mst set booked_qty = booked_qty-old.qty+new.qty 
    where prod_cd = new.prod_cd;
    insert into tempp values (1,'updated');
end ; //
delimiter ;


drop trigger xyz;
update ord_dtl set qty = 900 where prod_cd = 'P1';

/*4. Write a Before Update of Status trigger on Ord_mst. If the Status is updated from P (Pending) to D (Delivered), 
the Booked_qty and Qty_in_stock from Prod_mst should be decreased accordingly.
If the Status is updated from P (Pending) to C (Cancelled), 
the details of the order should be deleted from Ord_dtl and corresponding Booked_qty from Prod_mst should be decreased accordingly. 
(The Before delete trigger on Ord_dtl would automatically decrease the Booked_qty from Prod_mst).*/

