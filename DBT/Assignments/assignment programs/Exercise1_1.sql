delimiter //
create procedure abc ()
begin
	declare l float default 20 ;
    declare w float default 15 ;
    declare a float ;
    declare p float ;
		set a = l * w ;
		set p = 2 * (l + w ) ;
	insert into temp values (l , w, a, p) ;
end ; //
delimiter ;

call abc() ;

create table temp
(
LENGTH FLOAT,
WIDTH FLOAT,
AREA FLOAT,
PERIMIETER FLOAT
);
select * from temp ;

drop procedure abc;
drop table temp;