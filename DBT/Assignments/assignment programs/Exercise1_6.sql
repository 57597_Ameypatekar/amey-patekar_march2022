create table temp (
num1 int,
num2 int,
result char(50)
);

select * from temp;
truncate table temp;
drop table temp ;

delimiter //
create procedure abc (num1 int, num2 int)
begin
	declare res char (50) ;
    if num1 * num2 = 100 then
		insert into temp values (num1,num2, 'PRODUCT IS EQUAL TO 100') ;
	elseif num1 * num2 > 100 then
		insert into temp values (num1,num2, 'PRODUCT IS GREATER THAN 100') ;
	ELSE 
		insert into temp values (num1,num2, 'PRODUCT IS NOT EQUAL TO OR GREATER THAN 100') ;
	end if ;
end ; //
delimiter ;

call abc (10,10) ;
call abc (10,5) ;
call abc (10,15) ;

drop procedure abc;
