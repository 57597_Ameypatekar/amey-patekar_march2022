create table temp(
num float,
square float,
cube_ float
);
select * from temp ;
drop table temp;
delimiter //
create procedure abc()
begin
	declare num float default 15  ;
    declare s float ;
    declare c float ;
		set s = num * num ;
        set c = num * num * num ;
        insert into temp values (num, s, c) ;
end; //
delimiter ;

call abc();
drop procedure abc;


