create table dept (
deptno int,
dname varchar(15),
loc varchar(10)
);


insert into dept values (10,'ACCOUNTING','NEW YORK'),
(20,'RESEARCH','DALLAS'),
(30,'SALES','CHICAGO'),
(40,'OPERATIONS','BOSTON');

select * from dept;

create table emp(
EMPNO int,
ENAME varchar(10),
JOB varchar(9),
HIREDATE date,
SAL float,
COMM float,
DEPTNO int
);

insert into emp values 
(7839 ,'KING', 'MANAGER', '1991-11-17', 5000, NULL, 10),
(7698 ,'BLAKE', 'CLERK', '1981-05-01', 2850, NULL, 30),
(7782 ,'CLARK', 'MANAGER', '1981-06-09', 2450, NULL, 10),
(7566 ,'JONES', 'CLERK', '1981-04-02', 2975, NULL, 20),
(7654 ,'MARTIN', 'SALESMAN', '1981-09-28', 1250, 1400, 30),
(7499 ,'ALLEN', 'SALESMAN', '1981-02-20', 1600, 300, 30);

select * from emp;

/*1. Find the names of all employees hired in the month of February (of
any year).*/
select ename , hiredate from emp 
where month(hiredate)= 2; 

select ename , hiredate from emp 
where hiredate like '_____02%';


/*2. Find the names of all employees whose name ends with the letter
‘M’.*/

select ename from emp where ename like '%m';

/*3. Display the ENAME and JOB for all employees who belong to the
same DEPTNO as employee ‘KING’.*/
select ename, job from emp 
where ename = 'KING' ;

/*4. Display the HRA for each employee given that HRA is 20% of
SAL.*/

select sal , sal*0.20 "HRA" from emp ;

/*5. Display the JOB column with duplicate values suppressed.*/
 select distinct job from emp;
 
 /*6. Display the Enames and the corresponding Dnames.*/
 select ename, dname from emp, dept 
 where emp.deptno= dept.deptno;
 
 /*7. Display the employees in descending order of DEPTNO.*/
 select ename , deptno from emp order by deptno desc;
 
 /*8. Find the names of all employees who do not receive any
commission.*/
 select * from emp where comm is null;
 
 /*9. Display the employee name and employee number of the
employees with the headings as NUMBER and NAME.*/

select empno "NUMBER", ename "NAME" from emp ;

/*10.Find the name of the employee who is receiving the maximum
salary.*/
select ename, max(sal) from emp;
 
/*11.Display the sum of SAL for all the employees belonging to
DEPTNO 10.*/
select sum(sal) "TOTAL SUM" from emp where deptno = 10; 

/*12.Display the rows where JOB column ends with the letter ‘T’.*/
select * from emp where job like '%T' ;

/*Procedures , Function
1.Write a stored procedure to convert a temperature in Fahrenheit (F) to its
equivalent in Celsius (C). The required formula is:- C= (F-32)*5/9 Insert the
temperature in Centigrade into TEMPP table. Calling program for the stored
procedure need not be written.*/

create table tempp(
tempInF float,
tempInC float);


delimiter //
create procedure FTOC (f float)
begin
	declare c float ;
    set c = (f-32)*5/9;
    insert into tempp values (f,c) ;
end; //
delimiter ;

call FTOC (98);
select * from tempp;
drop table tempp;

/*2. Write a stored function by the name of Num_cube. The stored function
should return the cube of a number ‘N’. The number ‘N’ should be passed to the
stored function as a parameter. Calling program for the stored function need not
be written.*/

create table tempp(
 Num float,
 Num_cube float
);

delimiter //
create procedure Num_cube (N float)
begin
	declare c float ;
    set c = N*N*N ;
    insert into tempp values (N,c);
end; //
delimiter ;

call Num_cube (5);
call Num_cube (10);
call Num_cube (21);
call Num_cube (11.11);

select * from tempp;



