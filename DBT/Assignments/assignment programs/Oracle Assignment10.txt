Sameer Dehadrai Page: 1
Assignment – 10
Joining a Table to Itself.
1) Write a query that produces all pairs of salespeople who are living in the same city. Exclude combinations of salespeople with themselves as well as duplicate rows with the order reversed.

2) Write a query that produces the names and cities of all customers with the same rating as Hoffman.

	SQL> select cname, city from customers
	  2  where rating =
	  3  (select rating from customers
	  4  where cname = 'Hoffman');

	CNAME      CITY
	---------- ----------
	Hoffman    London
	Clemens    London
	Pereira    Rome