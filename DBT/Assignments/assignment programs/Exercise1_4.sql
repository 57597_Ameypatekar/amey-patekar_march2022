CREATE TABLE temp (
    num INT,
    yard INT,
    feet INT,
    inches INT
);

select * from temp ;
truncate table temp;
drop table temp;

delimiter //
create procedure inches (num int)
begin
	declare y int ;
    declare f int ;
    declare i int ;
    declare m int ;
    set y = num / 36 ;
    set m = num % 36 ;
    set f = m / 12 ;
    set i = m % 12 ;
    insert into temp values (num, y, f, i) ;
end; //
delimiter ;

call inches (124) ;
drop procedure inches;

