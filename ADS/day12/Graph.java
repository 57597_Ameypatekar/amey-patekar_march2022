class Graph{
	
	public void init(int [][]arr){
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				arr[i][j]=0;
			}
		}
	}
	//directed graph
	public void addEdge(int [][]arr,int src,int dest){
		arr[src][dest]=1;
		
	}
	
	//undirected graph
	public void addEdgeToUndirectedGraph(int [][]arr,int src,int dest){
		arr[src][dest]=1;
		arr[dest][src]=1;
	}
	
	
	public void display(int [][]arr){
		for(int i=0;i<arr.length;i++)
		{
			for(int j=0;j<arr.length;j++)
			{
				System.out.print(arr[i][j]+" | ");
			}
			System.out.println();
		}
	}
	
	int hasEdge(int arr[][],int src,int dest){
		if(arr[src][dest]==1)
			return 1;
		
		return 0;
	}
	
	public void removeEdge(int arr[][],int src,int dest){
		arr[src][dest]=0;
			
	}
	
	public static void main(String [] args){
		int n=5;
		int arr[][]=new int[n][n];
		Graph g=new Graph();
		g.init(arr);
		g.addEdge(arr,0,1);
		g.addEdge(arr,0,2);
		g.addEdge(arr,0,3);
		g.addEdge(arr,1,3);
		g.addEdge(arr,1,4);
		g.addEdge(arr,2,3);
		g.addEdge(arr,3,4);
		g.display(arr);
		
		System.out.println(g.hasEdge(arr,1,0));
		System.out.println(g.hasEdge(arr,0,1));
		System.out.println(g.hasEdge(arr,0,4));
		
		g.removeEdge(arr,2,3);
		g.removeEdge(arr,3,4);
		g.display(arr);
		
		
		System.out.println();
		g.init(arr);
		g.addEdgeToUndirectedGraph(arr,4,2);
		g.display(arr);
	}
}

/*
0 | 1 | 1 | 1 | 0 |
0 | 0 | 0 | 1 | 1 |
0 | 0 | 0 | 1 | 0 |
0 | 0 | 0 | 0 | 1 |
0 | 0 | 0 | 0 | 0 |
*/



/*undirected graph
0 | 0 | 0 | 0 | 0 |
0 | 0 | 0 | 0 | 0 |
0 | 0 | 0 | 0 | 1 |
0 | 0 | 0 | 0 | 0 |
0 | 0 | 1 | 0 | 0 |
*/