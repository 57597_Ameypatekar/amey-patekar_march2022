//LinkedList
class SinglyLinkdeList{

	class Node{
	int data;
	Node next;
	
	public Node(int data){
		this.data=data;
		this.next=null;
	}
	}

	Node head=null;
	Node tail=null;
	
	public void addNode(int data){
		Node newNode=new Node(data);
		
		if(head==null)
		{
			head=newNode;
			tail=newNode;
		}
		
		else{
			tail.next=newNode;
			tail=newNode;
		}
	}
	
	public void display(){
		Node temp=head;
		
		
		if(head==null)
			{
				System.out.println("list is empty");
				return;
			}
				
			System.out.print("Nodes of singly linked list : ");
			while(temp!=null)
			{
			System.out.print(temp.data+" ");
			
			temp=temp.next;
			}
			
			
			System.out.println();
			//System.out.println("Head Next "+head.next);
			//System.out.println("Temp Next "+temp.next);
			//System.out.println("Tail Next "+tail.next);
			
	}
	
	public static void main(String []args){
		SinglyLinkdeList sll=new SinglyLinkdeList();
		sll.addNode(1);
		sll.addNode(2);
		sll.addNode(3);
		sll.addNode(5);
		sll.addNode(6);
		sll.addNode(7);
		
		sll.display();
	}
}