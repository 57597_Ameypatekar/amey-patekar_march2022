
class MCQ{
	
	 static int sum(int n) {
	 
		if (n==0)
		   return n;
		else
		   return n + sum(n-1);    
	}
	
	public static void main(String []args){
		
	System.out.println(sum(5));
	
	}
}

/*
Q.What will be the output of the above code ?
	A. 10
	B. 21
	C. 15
	D. 14
*/







/*Solution
Answer : option C

	5.	^	1 + (0)  ->6.  1
			-------
	4.	^	2 + (1)  ->7.  2 + 1  = 3
			-------
	3.	^	3 + (2)  ->8.  3 + 3  = 6
			-------
	2.	^	4 + (3)  ->9.  4 + 6  = 10
			-------
	1.	^	5 + (4)  ->10. 5 + 10 = 15 
  steps     -------
			 stack
*/