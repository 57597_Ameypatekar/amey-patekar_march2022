//sorting algorithms
import java.util.Scanner;
class SortingAlgorithms{
	
	public void setArray(int []a){
		Scanner sc=new Scanner(System.in);
		for(int i=0;i<a.length;i++)
		{
			System.out.print("\tArray Element["+i+"] : ");
			a[i]=sc.nextInt();
		}
	}
	
	public void getArray(int []a){
		System.out.println();
		System.out.print("\t");
		for(int i=0;i<a.length;i++)
		{
			System.out.print(a[i]+" ");
		}
		System.out.println("\n");
	}
	
	public void bubbleSort(int []a){
		int temp=0,flag=0;;
		for(int i=0;i<a.length-1;i++)
		{
			flag=0;
			for(int j=0;j<a.length-1-i;j++)
			{
				if(a[j]>a[j+1])
				{
					temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
					flag=1;
				}
			}
			if(flag==0)
			break;
		}
		
		getArray(a);
	}
	
	public void insertionSort(int []a){
		int j=0,temp=0;
		for(int i=1;i<a.length;i++)
		{
		  temp=a[i];
		  j=i-1;
			  while(j>0 && a[j]>temp)
			  {
				  a[j+1]=a[j];
				  j--;
			  }
		  a[j+1]=temp;
		}
	getArray(a);
	}
		
	public void quickSort(int []arr, int low, int high){
		if(low < high){
        int p = partition(arr, low, high);
        quickSort(arr, low, p-1);
        quickSort(arr, p+1, high);
	}
	}
	
	int partition(int[]arr,int low,int high){
		int p = low, j;
		for(j=low+1; j <= high; j++)
			if(arr[j] < arr[low])
				swap(arr, ++p, j);

		swap(arr, low, p);
		return p;
	} 
	
	public void swap(int []arr,int low,int pivot){
		
		int tmp = arr[low];
		arr[low] = arr[pivot];
		arr[pivot] = tmp;
	}
	
	public void selectionSort(int []a){
		int temp=0;
		for(int i=0;i<a.length-1;i++)
		{
			
			for(int j=i+1;j<a.length;j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
					
			}
					
		}
		getArray(a);
	}
	
	public static void main(String []args){
	Scanner sc=new Scanner(System.in);
	
	SortingAlgorithms s=new SortingAlgorithms();
	System.out.print("\tEnter size of array : ");
	int n=sc.nextInt();
	int []arr=new int[n];
	
	s.setArray(arr);
	System.out.println();
	int ch=1;
		while(ch==1){
			System.out.println("\t==========================================================================================");
			System.out.print("\t0.Exit\n\t1.Display\t2.Bubble sort\t3.Insertion sort\t4.Merge sort\t5.Quick sort\n\t6.Selection sort\n\tEnter Choice : ");
			int choice=sc.nextInt();
			System.out.println("\t==========================================================================================");
			
			switch(choice){
				case 0 : ch=0;
						 break;
				case 1 : s.getArray(arr);
						 break;
				case 2 : s.bubbleSort(arr);
						 break;
				case 3 : s.insertionSort(arr);
						 break;
				case 4 : s.sort(arr);
						 s.getArray(arr);
						 break;
				case 5 : s.quickSort(arr,0,arr.length-1);
						 s.getArray(arr);
						 break;
				case 6 : s.selectionSort(arr);
						 break;
				default :System.out.println("\tInvalid number");
			}
		}
	}


	public void sort(int[] arr) {
        if (arr.length < 2) return;

        int mid = arr.length / 2;

        int[] left_half = new int[mid];
        int[] right_half = new int[arr.length - mid];

        // copying the elements of array into left_half
        for (int i = 0; i < mid; i++) {
            left_half[i] = arr[i];
        }
        
        // copying the elements of array into right_half
        for (int i = mid; i < arr.length; i++) {
            right_half[i - mid] = arr[i];
        }

        sort(left_half);
        sort(right_half);
        merge(arr, left_half, right_half);
    }
  
    public void merge(int[] arr, int[] left_half, int[] right_half) {
        int i = 0, j = 0, k = 0;

        while (i < left_half.length && j < right_half.length) {
            if (left_half[i] < right_half[j]) {
                arr[k++] = left_half[i++];
            }
            else {
                arr[k++] = right_half[j++];
            }
        }
        while (i < left_half.length) {
            arr[k++] = left_half[i++];
        }
        while (j < right_half.length) {
            arr[k++] = right_half[j++];
        }
    }
}

/*
mergeSort
public class MergeSort {
    static void sort(int[] arr) {
        if (arr.length < 2) return;

        int mid = arr.length / 2;

        int[] left_half = new int[mid];
        int[] right_half = new int[arr.length - mid];

        // copying the elements of array into left_half
        for (int i = 0; i < mid; i++) {
            left_half[i] = arr[i];
        }
        
        // copying the elements of array into right_half
        for (int i = mid; i < arr.length; i++) {
            right_half[i - mid] = arr[i];
        }

        sort(left_half);
        sort(right_half);
        merge(arr, left_half, right_half);
    }
  
    static void merge(int[] arr, int[] left_half, int[] right_half) {
        int i = 0, j = 0, k = 0;

        while (i < left_half.length && j < right_half.length) {
            if (left_half[i] < right_half[j]) {
                arr[k++] = left_half[i++];
            }
            else {
                arr[k++] = right_half[j++];
            }
        }
        while (i < left_half.length) {
            arr[k++] = left_half[i++];
        }
        while (j < right_half.length) {
            arr[k++] = right_half[j++];
        }
    }
    public static void main(String[] args) {
        int[] arr = {5, 1, 7, 3, 8, 0, 1, 5, 7, 2, 8, 9, -7, 4, -9, -3, 4};
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
*/