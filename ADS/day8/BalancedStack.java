
import java.util.Scanner;
class BalancedStack{
	
	static char []stack=new char[10];
	int top=-1;
	int open=0,close=0,sqopen=0,sqclose=0,curopen=0,curclose=0;
	
	public void push(char x){
		if(top==(10-1)){
			System.out.println("\tStack Overflow");
		}
		else{
			
			top++;
			stack[top]=x;
		}
		if(x=='(')
		{
			open++;
		}
		if(x==')')
		{
			close++;
		}
		if(x=='[')
		{
			sqopen++;
		}
		if(x==']')
		{
			sqclose++;
		}
		if(x=='{')
		{
			curopen++;
		}
		if(x=='}')
		{
			curclose++;
		}
	}
	
	public void display(){
		if(top==-1){
			System.out.println("\tStack is empty");
		}
		
		else{
		for(int i=top;i>=0;i--)
		{
			System.out.println("\tstack["+i+"]  "+stack[i]);
		}
		
		}
		
		
	}
	
	public void checkBal(){
		
		
		if(open==close&&sqopen==sqclose&&curopen==curclose)
		{
			System.out.println("\tBalance stack");
		}
		else{
			System.out.println("\tUnbalance stack");
		}
	}
	
	public static void main(String []args){
		BalancedStack s=new BalancedStack();
		Scanner sc=new Scanner(System.in);
		
		int c=1;
		while(c==1)
		{
			System.out.println("\t=========================================");
		System.out.print("\t0.Exit\n\t1.push\n\t2.display\n\t3.checkBal\n\tEnter Number : ");
		int choice=sc.nextInt();
		System.out.println("\t=========================================");
		switch(choice){
			case 0:	c=0;
					break;
			case 1:	System.out.print("\tEnter only (,[,{,},],) : ");
					char ch=sc.next().charAt(0);
					s.push(ch);
					break;
			case 2: s.display();
					break;
			case 3: s.checkBal();
					break;
			default:System.out.println("\tEntered Invalid Number");
					break;
		
		}
		}

	}
}


