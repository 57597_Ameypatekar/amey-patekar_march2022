import java.util.Scanner;
class Stack_usingarray{
	static final int n=5;
	static int stack[]=new int[n];
	static int top =-1;
	
	public boolean isEmpty(){
		if(top<0)
		{
		System.out.println("\tStack is Empty ");
		return true;
		}
		
		return false;
		
	}
	
	public boolean isFull(){
		if(top >= n-1)
		{
			System.out.println("\tStack is Full ");
			return true;
		}
		
		return false;
			
	}
	
	public void push(int x){
		if(isFull())
		{}	
		else{
		stack[++top]=x;
		}		
	}
	
	public void pop(){
		int x=0;
		if(isEmpty())
		{}
		else{
			x=stack[top];
			top--;
			System.out.println("\tPopped element is "+x);
		}
		
	}
	
	public void peek(){
		if(isEmpty())
		{}
		else{
			System.out.println("\tPeek element is "+stack[top]);
		}
	}
	
	public void display(){
		if(isEmpty())
		{}			
		for(int i=top;i>-1;i--)
		{	
		System.out.println("\tStack element ["+i+"] "+stack[i]);
		}
		
	}
	
	public void count(){
		if(isEmpty())
		{}
		else
		System.out.println("\tThe number of elements in stack is "+(top+1));
	}
	
	public static void main(String []args){
		Stack_usingarray s=new Stack_usingarray();
		Scanner sc=new Scanner(System.in);
		int ch=1;
	while(ch==1)
	{
		
		System.out.println("\t=========================================");
		System.out.print("\t0.Exit\n\t1.push\n\t2.pop\n\t3.peek\n\t4.display\n\t5.isEmpty\n\t6.isFull\n\t7.count\n\tEnter Number : ");
		int choice=sc.nextInt();
		System.out.println("\t=========================================");
		switch(choice){
			case 0:	ch=0;
					break;
			case 1:	System.out.print("\tEnter data :");
					int n1=sc.nextInt();
					s.push(n1);
					break;
			case 2:s.pop();break;
			case 3:s.peek();break;
			case 4:s.display();break;
			case 5:s.isEmpty();break;
			case 6:s.isFull();break;
			case 7:s.count();break;
			default:System.out.println("\tEntered Invalid Number");
					break;
		
	}
	}
}
}