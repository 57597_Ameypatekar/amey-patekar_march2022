import java.util.Scanner;
class SinglyCircularLinkedList {

    class Node {
        int data;
        Node next;

        public Node(int d) {
            this.data = d;
            this.next = null;
        }
    }

    Node head = null;
    Node tail = null;
    Node trav = null;
    Node temp = null;
	
	
    public void addNode(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
		}
		tail.next=head;
		//System.out.print(tail.next.data + " "); //checking circular link is established or not
    }
	
    public void display() {

        trav = head;

        if (head == null) {
            System.out.println(" Singly Circular Linkedlist is empty");
            return;
        }

        System.out.print("Singly Circular Linked list : ");
        while (trav.next != head){
            System.out.print(trav.data + " ");
            trav = trav.next;
        }
		System.out.print(trav.data + " ");
        System.out.println();

    }

    public void insertAtFirst(int data) {
        Node newNode = new Node(data);
		if(head==null)
		{
			head=tail=newNode;
			newNode.next=head;
			tail.next=head;
		}
		else{
			newNode.next = head;
			head = newNode;
			tail.next=head;
		}
	   
        display();
    }

    public void insertAtEnd(int data) {
        Node newNode = new Node(data);
        trav = head;
		if(head==null)
		{
			insertAtFirst(data);
		}
		else{
			
			newNode.next = tail.next;
			tail.next = newNode;
			tail=newNode;
			display();
		}
    }

    public void insertNode(int data, int pos) {
        Node newNode = new Node(data);
		temp = head;
        int count = 0;
        do  {
			if(head!=null){
            temp = temp.next;
            count++;
			}
        }while(temp != head);
        
		if(head==null&&pos==0)
		{
			insertAtFirst(data);
		}
		
		else if(pos<0||pos>count||pos==0)
		{
			System.out.println("Invalid Position");
		}
		
		else if(pos==1)
		{
			insertAtFirst(data);
		}
		else if(pos==count+1)
		{
			insertAtEnd(data);
		} 
		else{
			trav=head;
			int i=1;
			while (i < pos - 1) {
                trav = trav.next;
                i++;
            }
			
			newNode.next=trav.next;
			trav.next=newNode;
            display();
		}
        
    }

    public void deleteAtFirst() {
       if(head==null)
	   {
		   display();
	   }
	   else if(head.next==head){
		   head=null;
		   display();
	   }
	   else{
		head = head.next;
		tail.next = head; 
        display();
	   }
    }

    public void deleteAtEnd() {
        trav = head;
		if(head==null)
		{
			display();
		}
        else if (trav.next == head) {
            head = null;
            display();
        } else {
            while (trav.next != head) {
                temp = trav;
                trav = trav.next;
            }
            temp.next = trav.next;
			tail=temp;
            display();
        }
    }

    public void deleteAtGivenPosition(int pos) {
        trav = head;
        int i = 1;
		temp = head;
        int count = 0;
        do  {
			if(head!=null){
            temp = temp.next;
            count++;
			}
        }while(temp != head);
		
		if(head==null)
		{
			display();
		}
		else if (trav.next == head) {
            head = null;
            display();
		}
		else if(pos<0||pos>count){
			System.out.println("Invalid Position");
		}
        else if (pos == 1) {
            head = head.next;
			tail.next = head;
			tail=head;
			display();
        } else {
            while (i < pos - 1) {
                trav = trav.next;
                i++;
            }
            temp = trav.next;
            trav.next = temp.next;
            temp = null;
            display();
        }
		

    }

    public void deleteNode(int data) {
        Node prevNode = null;
        trav = head;
        //finding length of linkedlist
        temp = head;
        int count = 0,flag=0;
        do  {
			if(head!=null){
            temp = temp.next;
            count++;
			}
        }while(temp != head);
			
        do {

            if (trav.data == data) {
                if (trav == head && count == 1) {
                    head = null;
                    display();
					flag=1;
                    break;
                } else if (trav.next == head) {
                    deleteAtEnd();
					flag=1;
                    break;
                } else if (trav == head) {
                    head = trav.next;
					trav=null;
					tail.next=head;
                    display();
					flag=1;
                    break;
                } else {
                    prevNode.next = trav.next;
                    trav = null;
					tail=head;
                    display();
					flag=1;
                    break;
                }
            }
				
            prevNode = trav;
            trav = trav.next;
			
        }while (trav != head);
		if(flag==0)
		{
			searchNode(data);
		}
  
  }

    public void deleteAll() {
        head = null;
        display();
    }

    public void length() {
        temp = head;
        int count = 0;
        do{
            temp = temp.next;
            count++;
        }while (temp != head); 
        System.out.println("The length of Linked List is " + count);

    }

    public void sortLinkedList() {

        Node index = null;
        trav = head;
        int tempvar;
        Scanner sc = new Scanner(System.in);
        System.out.print(" 1.Ascending Order \n 2.Descending Order\nEnter Choice : ");
        int ch = sc.nextInt();

        switch (ch) {
            case 1:
                if (head == null) {
                    display();
                    return;
                } else {
                    while (trav != head) {
                        index = trav.next;
                        while (index != head) {
                            if (trav.data > index.data) {
                                tempvar = trav.data;
                                trav.data = index.data;
                                index.data = tempvar;
                            }
                            index = index.next;
                        }
                        trav = trav.next;
                    }
                }
                display();
                break;
            case 2:
                if (head == null) {
                    display();
                    return;
                } else {
                    while (trav != null) {
                        index = trav.next;
                        while (index != null) {
                            if (trav.data < index.data) {
                                tempvar = trav.data;
                                trav.data = index.data;
                                index.data = tempvar;
                            }
                            index = index.next;
                        }
                        trav = trav.next;
                    }
                }
                display();
                break;
            default:
                System.out.println("Invalid Number");
                break;
        }
  
  }

    public void reverse() {
        Node prevNode = null;
        Node nextNode = null;
		Node current = null;
        current = tail.next;
		nextNode = current.next;
        System.out.print("Reverse ");
		if(head==null){
			display();
		}
		
        else {
			do
			{
				prevNode=current;
				current=nextNode;
				nextNode=current.next;
				current.next=prevNode;
			}while (current != tail);
			nextNode.next=tail;
			tail=nextNode;
			
			display();
		}
        
    }

    public void searchNode(int data) {
        trav = head;
        int count = 0, flag = 0;
		if(head==null)
		{
			flag=0;
		}
		else{
        do{
            count++;
            if (trav.data == data) {
                flag = 1;
                break;
            }
            trav = trav.next;
        }while (trav != head);
		}
        if (flag == 1)
            System.out.println("The Searching element " + data + " is found at position : " + count);
        else if (flag == 0)
            System.out.println("The Searching element " + data + " is not found in Linked List");
    }

    public static void main(String[] args) {

        SinglyCircularLinkedList l = new SinglyCircularLinkedList();

        Scanner sc = new Scanner(System.in);
        int ch = 1;
        while (ch == 1) {
            System.out.print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("\t\t\t\tSINGLY CIRCULAR LINKED LIST OPERATIONS\n");
            System.out.println("\t1.AddNode\t\t\t\t2.Insert Node at Beginning\t3.Insert Node at End\n\t4.Insert Node at Given Position\t\t5.Delete First Node\t\t6.Delete Last Node\n\t7.Delete Node at Given Position\t\t8.Delete Node\t\t\t9.Delete All Nodes\n\t10.Search Node\t\t\t\t11.Length of LinkedList\t\t12.Reverse LinkedList\n\t13.Sort Linkedlist\t\t\t14.Display\t\t\t15.Exit");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.print("Enter your choice :");
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("Add Node :");
                    int n1 = sc.nextInt();
                    l.addNode(n1);
                    System.out.println();
                    break;
                case 2:
                    System.out.print("Insert Node at Beginning :");
                    int n2 = sc.nextInt();
                    l.insertAtFirst(n2);
                    System.out.println();
                    break;
                case 3:
                    System.out.print("Insert Node at End :");
                    int n3 = sc.nextInt();
                    l.insertAtEnd(n3);
                    System.out.println();
                    break;
                case 4:
                    System.out.print("Insert Node :");
                    int n4 = sc.nextInt();
                    System.out.print("Enter position :");
                    int pos = sc.nextInt();
                    l.insertNode(n4, pos);
                    System.out.println();
                    break;
                case 5:
                    l.deleteAtFirst();
                    System.out.println();
                    break;
                case 6:
                    l.deleteAtEnd();
                    System.out.println();
                    break;
                case 7:
                    System.out.print("Enter position :");
                    int pos1 = sc.nextInt();
                    l.deleteAtGivenPosition(pos1);
                    System.out.println();
                    break;
                case 8:
                    System.out.print("Enter Element from Linked List :");
                    int n5 = sc.nextInt();
                    l.deleteNode(n5);
                    System.out.println();
                    break;
                case 9:
                    l.deleteAll();
                    System.out.println();
                    break;
                case 10:
                    System.out.print("Enter Element from Linked List :");
                    int n = sc.nextInt();
                    l.searchNode(n);
                    System.out.println();
                    break;
                case 11:
                    l.length();
                    System.out.println();
                    break;
                case 12:
                    l.reverse();
                    System.out.println();
                    break;
                case 13:
                    l.sortLinkedList();
                    System.out.println();
                    break;
                case 14:
                    l.display();
                    System.out.println();
                    break;
                case 15:
                    ch = 0;
                    break;
                default:
                    System.out.println("Invalid Choice\n");
                    break;
            }
        }
    }
}