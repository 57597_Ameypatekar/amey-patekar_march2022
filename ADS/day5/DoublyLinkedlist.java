import java.util.Scanner;
class DoublyLinkedlist{
	class Node{
		int data;
		Node next;
		Node prev;
		
		public Node(int data){
			this.data=data;
			this.next=null;
			this.prev=null;
		}
	}
	
	Node head,trav,temp,tail;
	
	public void addNode(int data){
		Node newNode= new Node(data);
		
		if(head==null){
			head=tail=newNode;
			
		}
		else{
				tail.next=newNode;
				newNode.prev=tail;
				tail=newNode;
			}
	}
	
	public void display(){
		trav=temp=head;
		if(head==null){
			System.out.println("The Doubly Linkedlist is Empty");
			return;
		}
		else{
			System.out.print("Forward Display : ");
			while(trav!=null){
				System.out.print(trav.data+" ");
				temp=trav;
				trav=trav.next;
			}
			
		}
	}
	
	public void reverseDisplay(){
		trav=temp=head;
		if(head==null){
			System.out.println("The Doubly Linkedlist is Empty");
			return;
		}
		else{
			while(trav!=null){
				temp=trav;
				trav=trav.next;
			}
			
			System.out.println();
			System.out.print("Reverse Display : ");
			
			
			while(temp!=null){
					System.out.print(temp.data+" ");
					temp=temp.prev;
				}
		}
	}
	
	public void insertAtFirst(int data){
		Node newNode=new Node(data);
		newNode.next=head;
		head.prev=newNode;
		head=newNode;
		display();
	}
	
	public void insertAtEnd(int data){
		Node newNode=new Node(data);
		trav=head;
		if(head==null)
		{
			insertAtFirst(data);
		}
		else{
			while(trav.next!=null){
				trav=trav.next;
				
			}
			trav.next=newNode;
			newNode.prev=trav;
			display();
		}
		
	}
	
	public void insertNode(int data, int pos){
		Node newNode = new Node(data);
        trav = temp = head;
        int count = 1;
        if (count == pos) {
            insertAtFirst(data);

        } else {
            while (count < pos - 1) {
                trav = trav.next;
                temp = trav.next;
				count++;
            }
            newNode.next = trav.next;
			trav.next = newNode;
			temp.prev=newNode;
			newNode.prev=trav;
            display();
        }
	}
	
	public void deleteAtFirst(){
		if(head.next==null)
		{
			head=null;
			display();
		}
		else{
		head=head.next;
		head.prev=null;
		display();
		}
	}
	
	public void deleteAtEnd(){
		trav=head;
		if(head==null)
		{
			display();
		}
		else if(trav.next==null)
		{
			head=null;
			display();
		}
		else{
			while(trav.next!=null){
				temp=trav;
				trav=trav.next;
			}
			tail=temp;
			temp.next=null;
			display();
		}
		
	}
	
	public void deleteAtGivenPosition(int pos){
		trav = temp = head;
		Node temp2=null;
        int count = 1;
		
		int count1 = 0;
        while (temp != null) {
            temp = temp.next;
            count1++;
        }
		
		if(pos>count1||pos<1)
		{
			System.out.println("Invalid position");
		}
		
		else if(pos==1 && count1==1)
		{
			head=null;
			display();
		}
			
        else if (pos == 1) {
            deleteAtFirst();
			
        } else {
            while (count < pos - 1) {
                trav = trav.next;
                count++;
            }
			
			if(count1==(count+1))
			{
				trav.next=null;
				tail=trav;
				display();
			}
			else{
				temp = trav.next;
				trav.next = temp.next;
				temp2 = temp.next;
				temp2.prev= trav;
				temp = null;
				display();
			}
        }
	}
	
	public void deleteNode(int data){
		Node prevNode = null;
        trav = head;
        //finding length of linkedlist
        temp = head;
        int count = 0,flag=0;
        while (temp != null) {
            temp = temp.next;
            count++;
        }
			
        while (trav != null) {

            if (trav.data == data) {
                if (trav == head && count == 1) {
                    head = null;
                    display();
					flag=1;
                    break;
                } else if (trav.next == null) {
                    deleteAtEnd();
					flag=1;
                    break;
                } else if (trav == head) {
                    head = trav.next;
					head.prev=null;
                    display();
					flag=1;
                    break;
                } else {
                    prevNode.next = trav.next;
					temp=trav.next;
					temp.prev=prevNode;
                    trav = null;
                    display();
					flag=1;
                    break;
                }
            }
				
            prevNode = trav;
            trav = trav.next;
			
        }
		if(flag==0)
		{
			searchNode(data);
		}
	}
	
	public void deleteAll(){
		head=null;
		display();
	}
	
	public void length() {
        temp = head;
        int count = 0;
        while (temp != null) {
            temp = temp.next;
            count++;
        }
        System.out.println("The length of Linked List is " + count);

    }
	
    public void sortLinkedList() {

        Node index = null;
        trav = head;
        int tempvar;
        Scanner sc = new Scanner(System.in);
        System.out.print(" 1.Ascending Order \n 2.Descending Order\nEnter Choice : ");
        int ch = sc.nextInt();

        switch (ch) {
            case 1:
                if (head == null) {
                    display();
                    return;
                } else {
                    while (trav != null) {
                        index = trav.next;
                        while (index != null) {
                            if (trav.data > index.data) {
                                tempvar = trav.data;
                                trav.data = index.data;
                                index.data = tempvar;
                            }
                            index = index.next;
                        }
                        trav = trav.next;
                    }
                }
                display();
                break;
            case 2:
                if (head == null) {
                    display();
                    return;
                } else {
                    while (trav != null) {
                        index = trav.next;
                        while (index != null) {
                            if (trav.data < index.data) {
                                tempvar = trav.data;
                                trav.data = index.data;
                                index.data = tempvar;
                            }
                            index = index.next;
                        }
                        trav = trav.next;
                    }
                }
                display();
                break;
            default:
                System.out.println("Invalid Number");
                break;
        }
  
  }	
	
	public void searchNode(int data) {
        trav = head;
        int count = 0, flag = 0;
        while (trav != null) {
            count++;
            if (trav.data == data) {
                flag = 1;
                break;
            }
            trav = trav.next;
        }
        if (flag == 1)
            System.out.println("The Searching element " + data + " is found at position : " + count);
        else if (flag == 0)
            System.out.println("The Searching element " + data + " is not found in Linked List");
    }
	
	public void reverse() {
        Node currentNode = null;
        Node nextNode = null;
        currentNode = head;
        System.out.print("Reverse ");
        while (currentNode != null) {
			
            nextNode = currentNode.next;
			currentNode.next = currentNode.prev;
			            
			currentNode.prev = nextNode;
            currentNode = nextNode;
        }
		currentNode=head;
        head = tail;
		tail = currentNode;
        display();
		
    }
	
 public static void main(String[] args) {

        DoublyLinkedlist l = new DoublyLinkedlist();

        Scanner sc = new Scanner(System.in);
        int ch = 1;
        while (ch == 1) {
            System.out.print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("\t\t\t\t\t\tDOUBLY    LINKED    LIST    OPERATIONS\n");
            System.out.println("\t1.AddNode\t\t\t\t2.Insert Node at Beginning\t3.Insert Node at End\n\t4.Insert Node at Given Position\t\t5.Delete First Node\t\t6.Delete Last Node\n\t7.Delete Node at Given Position\t\t8.Delete Node\t\t\t9.Delete All Nodes\n\t10.Search Node\t\t\t\t11.Length of LinkedList\t\t12.Reverse LinkedList\n\t13.Sort Linkedlist\t\t\t14.Forward Display\t\t\t15.Reverse Display\n\t16.Exit");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.print("Enter your choice :");
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("Add Node :");
                    int n1 = sc.nextInt();
                    l.addNode(n1);
                    System.out.println();
                    break;
                case 2:
                    System.out.print("Insert Node at Beginning :");
                    int n2 = sc.nextInt();
                    l.insertAtFirst(n2);
                    System.out.println();
                    break;
                case 3:
                    System.out.print("Insert Node at End :");
                    int n3 = sc.nextInt();
                    l.insertAtEnd(n3);
                    System.out.println();
                    break;
                case 4:
                    System.out.print("Insert Node :");
                    int n4 = sc.nextInt();
                    System.out.print("Enter position :");
                    int pos = sc.nextInt();
                    l.insertNode(n4, pos);
                    System.out.println();
                    break;
                case 5:
                    l.deleteAtFirst();
                    System.out.println();
                    break;
                case 6:
                    l.deleteAtEnd();
                    System.out.println();
                    break;
                case 7:
                    System.out.print("Enter position :");
                    int pos1 = sc.nextInt();
                    l.deleteAtGivenPosition(pos1);
                    System.out.println();
                    break;
                case 8:
                    System.out.print("Enter Element from Linked List :");
                    int n5 = sc.nextInt();
                    l.deleteNode(n5);
                    System.out.println();
                    break;
                case 9:
                    l.deleteAll();
                    System.out.println();
                    break;
                case 10:
                    System.out.print("Enter Element from Linked List :");
                    int n = sc.nextInt();
					l.searchNode(n);
                    System.out.println();
                    break;
                case 11:
                    l.length();
                    System.out.println();
                    break;
                case 12:
                    l.reverse();
                    System.out.println();
                    break;
                case 13:
                    l.sortLinkedList();
                    System.out.println();
                    break;
                case 14:
                    l.display();
                    System.out.println();
                    break;
				case 15:
                    l.reverseDisplay();
                    System.out.println();
                    break;	
                case 16:
                    ch = 0;
                    break;
                default:
                    System.out.println("Invalid Choice\n");
                    break;
            }
        }
    }
}