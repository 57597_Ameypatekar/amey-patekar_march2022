//Recursion
class RecursionDemo2{
	static int i=0;
	static void recursion(){
		i++;
		if(i<=5)//base condition
		{
		System.out.println(i+".HELLO...");
		recursion();//recursive function call
		}
	}
	
	public static void main(String []args){
		
		recursion();//function call
	
	}
}


/*
1.HELLO...
2.HELLO...
3.HELLO...
4.HELLO...
5.HELLO...
*/