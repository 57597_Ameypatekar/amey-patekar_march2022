//Recursion
class RecursionDemo3{
	
	static int display(int n){
		
		
		if(n==4)
			return n;
		else
			return 2*display(n-1);
	
		
	}
	
	public static void main(String []args){
		System.out.println(display(6));
		
	
	}
}


/*
1.HELLO...
2.HELLO...
3.HELLO...
4.HELLO...
5.HELLO...
*/