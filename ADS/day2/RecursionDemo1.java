//Recursion
class RecursionDemo1{
	
	static void recursion(){
		System.out.println("HELLO...");
		recursion1();//recursive function call
	}
	
	static void recursion1(){
		System.out.println("AMEY");
		recursion();//recursive function call
	}
	
	public static void main(String []args){
		
		recursion();//function call
	
	}
}

/*
.
.
.
HELLO...
AMEY
HELLO...
AMEY
Exception in thread "main" java.lang.StackOverflowError
*/