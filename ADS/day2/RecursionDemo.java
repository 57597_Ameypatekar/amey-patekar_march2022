//Recursion
class RecursionDemo{
	
	static void recursion(){
		System.out.println("HELLO...");
		recursion();//recursive function call
	}
	
	public static void main(String []args){
		
		recursion();//function call
	
	}
}


/*
.
.
.
HELLO...
HELLO...
HELLO...
HELLO...
Exception in thread "main" java.lang.StackOverflowError
*/