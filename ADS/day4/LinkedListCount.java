import java.util.Scanner;
class LinkedListCount {

    class Node {
        int data;
        Node next;

        public Node(int d) {
            this.data = d;
            this.next = null;
        }
    }

    static Node head ;
    Node tail ;
    Node trav ;
   

    public void addNode(int data) {
        Node newNode = new Node(data);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
    }

    public void display() {

        trav = head;

        if (head == null) {
            System.out.println(" Linkedlist is empty");
            return;
        }

        System.out.print("Singly Linked list : ");
        while (trav != null) {
            System.out.print(trav.data + " ");
            trav = trav.next;
        }
        System.out.println();

    }

     public static int recursiveCount(Node n){
		 if(n==null)
		 {
			 return 0;
			 
		 }
		 return 1+recursiveCount(n.next);
	 }
	
	public static int getCount()
    {
		
        return recursiveCount(head);
    }
	
    public static void main(String[] args) {

        LinkedList l = new LinkedList();

        Scanner sc = new Scanner(System.in);
        
        l.addNode(10);
		l.addNode(20);	
		l.addNode(30);
		l.addNode(40);
		l.addNode(50);
		l.addNode(60);		
                
        l.display();
		int count=l.getCount();
        System.out.println("Length of LinkedList : "+count);       
                   
                
        
    }
}