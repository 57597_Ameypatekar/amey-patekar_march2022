import java.util.Scanner;
class ArrayMethods{
		int arr[]=new int[50];
		int size;
		void setArray(){
			Scanner sc=new Scanner(System.in);
			System.out.print("Enter size of array : ");
			size = sc.nextInt();
			//accepting array elements
			for(int i=0;i<size;i++)
			{
				System.out.print("array["+i+"] = ");
				arr[i]=sc.nextInt();
			}
		}
		
		void getArray(){
			System.out.println();
			System.out.print("Array elements : ");
			for(int i=0;i<size;i++)
			{
				System.out.print(arr[i]+" ");
			}
			
		}
		
		void insert(){
			Scanner sc=new Scanner(System.in);
			System.out.println();
			System.out.print("Enter element you want to insert : ");
			int value=sc.nextInt();
			System.out.println();
			System.out.print("Enter positon at which you want to insert : ");
			int pos=sc.nextInt();
			
			if(pos<=0||pos>size+1)
			{
				System.out.println("Invalid position");
			}
			else{
				for(int i=size-1;i>=pos-1;i--)
				{
					arr[i+1]=arr[i];
				}
				arr[pos-1]=value;
				size++;
				}
				getArray();
				System.out.println();
		}
		
		void delete(){
			Scanner sc=new Scanner(System.in);
			System.out.println();
			System.out.print("Enter position to delete : ");
			int dpos=sc.nextInt();
			
			if(dpos<=0||dpos>size)
			{
				System.out.println("Invalid position");
			}
			else{
				for(int i=dpos-1;i<size-1;i++)
				{
					arr[i]=arr[i+1];
				}
				size--;
			}
			getArray();
		}
		
		void search(){
			Scanner sc=new Scanner(System.in);
			System.out.println();
			int flag=0;
			System.out.print("Enter value you want to search : ");
			int svalue=sc.nextInt();
			for(int i=0;i<size;i++)
				{	
					if(arr[i]==svalue)
					{
						flag=1;
						break;
					}	
					
				}
				
				if(flag==1)
				{
						System.out.println(svalue+" is Found");
				}
				else{
					System.out.println(svalue+" is Not Found");
				}
		}
}
class ArrayDemo1{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		
		ArrayMethods a=new ArrayMethods();
		a.setArray();
		int choice=1;
		while (choice==1)
		{	System.out.println();
			System.out.println("==============================================");
			System.out.println("1.INSERT 2.SEARCH 3.DISPLAY 4.DELETE 5.EXIT");
			System.out.println("==============================================");
			System.out.print("Enter Number:");
			int n=sc.nextInt();
			switch(n)
			{
				case 1:a.insert();
						break;
				case 2:a.search();
						break;
				case 3:a.getArray();
						break;
				case 4:a.delete();
						break;
				case 5:choice=0;
						break;
				default:System.out.println("Invalid number");
						break;
			}
		}
		
	}
}