/*
5. Write a Java program to find Row with max 1s.
		Given a boolean 2D array of n x m dimensions where each row is sorted. Find the 0-based index of the first row that has the maximum number of 1's.

			Example 1:

			Input: 
			N = 4 , M = 4
			Arr[][] = {{0, 1, 1, 1},       r1=3
								 {0, 0, 1, 1},  r2=2
								 {1, 1, 1, 1},  r3=4
								 {0, 0, 0, 0}} r4=0
			Output: 2
			Explanation: Row 2 contains 4 1's (0-based indexing).

			Example 2:

			Input: 
			N = 2, M = 2
			Arr[][] = {{0, 0}, {1, 1}}
			Output: 1
			Explanation: Row 1 contains 2 1's (0-based indexing).*/
import java.util.Scanner;
class ArrayMax1s{
	
	void sortingArray(int [][]a,int sizem,int sizen){
		int temp=0;
		for(int i=0;i<sizem;i++)
		{
			for(int j=0;j<sizen-i-1;j++)
			{
				if(a[i][j]>=a[i][j+1])
				{
					temp=a[i][j];
					a[i][j]=a[i][j+1];
					a[i][j+1]=temp;
				}
			}
			
		}
	}
	
	void max1s(int [][]a,int sizem,int sizen){
		int row=0;
		int count1=0;
		for(int i=0;i<sizem;i++)
		{
			int count2=0;
			for(int j=0;j<sizen;j++)
			{
				if(a[i][j]==1)
				{
					count2++;
					if(count2>count1)
					{
						count1=count2;
						row=i;
					}
				}
			}
			
		}
		
		
		System.out.println("Row "+row+" contains "+count1+" 1s");
		
		
	}
	
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Size of 2D-Array m : ");
		int m=sc.nextInt();
		System.out.print("Enter Size of 2D-Array n : ");
		int n=sc.nextInt();
		System.out.println("Enter only 1 and 0 ");
		int [][]arr=new int[m][n];
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				arr[i][j]=sc.nextInt();
			}
			System.out.println();
		}
		
		ArrayMax1s a=new ArrayMax1s();
		a.sortingArray(arr,m,n);
		
		System.out.println();
		System.out.println("2D Array:");
		//displaying 2D-array
		for(int i=0;i<m;i++)
		{
			for(int j=0;j<n;j++)
			{
				System.out.print(arr[i][j]+"  ");
			}
			System.out.println();
		}
		
		a.max1s(arr,m,n);
		
	}
}