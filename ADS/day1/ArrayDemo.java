import java.util.Scanner;
class ArrayDemo{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		
		int arr[]=new int[50];
		System.out.println("Enter size of array");
		int size = sc.nextInt();
		//accepting array elements
		for(int i=0;i<size;i++)
		{
			arr[i]=sc.nextInt();
		}
		//displaying array elements
		for(int i=0;i<size;i++)
		{
			System.out.print(arr[i]+" ");
		}
		
		//insertion
		System.out.println();
		System.out.println("Enter positon at which you want to inset");
		int pos=sc.nextInt();
		System.out.println("Enter element you want to insert");
		int value=sc.nextInt();
		if(pos<=0||pos>size+1)
		{
			System.out.println("Invalid position");
		}
		else{
			for(int i=size-1;i>=pos-1;i--)
			{
				arr[i+1]=arr[i];
			}
			arr[pos-1]=value;
			size++;
			}
		//displaying array elements
		for(int i=0;i<size;i++)
		{
			
			System.out.print(arr[i]+" ");
		}
		
		
		//deletion
		System.out.println();
		System.out.println("Enter positon to delete");
		int dpos=sc.nextInt();
		
		if(dpos<=0||dpos>size)
		{
			System.out.println("Invalid position");
		}
		else{
			for(int i=dpos-1;i<size-1;i++)
			{
				arr[i]=arr[i+1];
			}
			size--;
		}
		//displaying array elements
		for(int i=0;i<size;i++)
		{
			
			System.out.print(arr[i]+" ");
		}
		
		//searching
		System.out.println();
		int flag=0;
		System.out.println("Enter value you want to search");
		int svalue=sc.nextInt();
		for(int i=0;i<size;i++)
			{	
				if(arr[i]==svalue)
				{
					flag=1;
					break;
				}	
				
			}
			
			if(flag==1)
			{
					System.out.println(svalue+" is Found");
			}
			else{
				System.out.println(svalue+" is Not Found");
			}
				
			
			
	}
}