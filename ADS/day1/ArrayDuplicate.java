/*
4. Write a Java Program to find the Common elements 
		Given three arrays sorted in increasing order. Find the elements that are common in all three arrays.
		Note: can you take care of the duplicates without using any additional Data Structure?

		Example 1:
		Input:
		n1 = 6; A = {1, 5, 10, 20, 40, 80}
		n2 = 5; B = {6, 7, 20, 80, 100}
		n3 = 8; C = {3, 4, 15, 20, 30, 70, 80, 120}
		Output: 20 80
*/
import java.util.Scanner;
class ArrayDuplicate{
	
	void sortingArray(int []a,int size){
		int temp=0;
		for(int i=0;i<size-1;i++)
		{
			for(int j=0;j<size-i-1;j++)
			{
				if(a[j]>a[j+1])
				{
					temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
			
		}
	}
	
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Size of First Array :");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		for(int i=0;i<size1;i++)
		{
			System.out.print("Array1["+i+"] = ");
			arr1[i]=sc.nextInt();
		}
		
		
		System.out.println();
		System.out.print("Enter Size of Second Array :");
		int size2=sc.nextInt();
		int arr2[]=new int[size2];
		for(int i=0;i<size2;i++)
		{
			System.out.print("Array2["+i+"] = ");
			arr2[i]=sc.nextInt();
		}
		
		System.out.println();
		System.out.print("Enter Size of Third Array :");
		int size3=sc.nextInt();
		int arr3[]=new int[size3];
		for(int i=0;i<size3;i++)
		{
			System.out.print("Array3["+i+"] = ");
			arr3[i]=sc.nextInt();
		}
		
		
		ArrayDuplicate ad=new ArrayDuplicate();
		ad.sortingArray(arr1,size1);
		ad.sortingArray(arr2,size2);
		ad.sortingArray(arr3,size3);
		
		System.out.println();
		System.out.println("Array After Sorting");
		System.out.print("First Array : ");
		for(int i=0;i<size1;i++)
		{
			System.out.print(arr1[i]+" ");
			
		}
		
		System.out.println();
		System.out.print("Second Array : ");
		for(int i=0;i<size2;i++)
		{
			System.out.print(arr2[i]+" ");
			
		}
		
		System.out.println();
		System.out.print("Third Array : ");
		for(int i=0;i<size3;i++)
		{
			System.out.print(arr3[i]+" ");
			
		}
		System.out.println();
		
		
		
		//Finding duplicates
		int []output=new int[size1+size2+size3];
		int size=size1>size2? (size1>size3?size1:size3):size2>size3?size2:size3;
		//System.out.println("largest size : "+size);
		//first array
		if(size==size1)
		{	
			for(int i=0;i<size;i++)
			{
				for(int j=0;j<size2;j++)
				{
					for(int k=0;k<size3;k++)
					{	
						if(arr1[i]==arr2[j]&&arr1[i]==arr3[k])
						output[i]=arr1[i];
					}
				}
				
			}
		}
		
		
		//second array
		else if(size==size2)
		{	
			for(int i=0;i<size;i++)
			{
				for(int j=0;j<size1;j++)
				{
					for(int k=0;k<size3;k++)
					{	
						if(arr2[i]==arr1[j]&&arr2[i]==arr3[k])
						output[i]=arr2[i];
					}
				}
				
			}
		}
		
		//third array
		else if(size==size3)
		{	
			for(int i=0;i<size;i++)
			{
				for(int j=0;j<size1;j++)
				{
					for(int k=0;k<size2;k++)
					{	
						if(arr3[i]==arr1[j]&&arr3[i]==arr2[k])
						output[i]=arr3[i];
					}
				}
				
			}
		}
		
		System.out.println();
		System.out.print("Duplicates from given Array : ");
		for(int i=0;i<size;i++)
		{
			if(output[i]==0)
			{
				
			}
			else {
				System.out.print(output[i]+" ");
			}
		}
		
	}
}