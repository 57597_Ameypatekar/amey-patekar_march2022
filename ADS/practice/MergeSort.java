class MergeSort{
	public static void mergeSort(int arr[]){
		if(arr.length<2)
			return;
		int mid=arr.length/2;
		int leftarr[]=new int[mid];
		int rightarr[]=new int[arr.length-mid];
		
		for(int i=0;i<mid;i++)
			leftarr[i]=arr[i];
		
		for(int i=mid;i<arr.length;i++)
			rightarr[i-mid]=arr[i];
		
		mergeSort(leftarr);
		mergeSort(rightarr);
		merge(arr,leftarr,rightarr);
	}
	
	public static void merge(int arr[],int []leftarr, int []rightarr){
		int i=0,j=0,k=0;
		while(i<leftarr.length && j<rightarr.length)
		{
			if(leftarr[i]<rightarr[j]){
				arr[k++]=leftarr[i++];
			}
			else {
				arr[k++]=rightarr[j++];
			}
		}
			while(i<leftarr.length)
			{
				arr[k++]=leftarr[i++];
			}
			while(j<rightarr.length)
			{
				arr[k++]=rightarr[j++];
			}
		
		
	}
	
	
	public static void main(String []args){
		int a[]=new int[]{10,50,15,5,25,30};
		
		mergeSort(a);
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+" ");
	}
}