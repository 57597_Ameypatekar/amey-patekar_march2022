import java.util.*;
class QuickSort{
	public static void quickSort(int a[],int start, int end){
		if(start<end){
			int pIndex=partition(a,start,end);
			quickSort(a,start,pIndex-1);
			quickSort(a,pIndex+1,end);
		}
		return;
	}
	
	public static int partition(int a[],int start, int end){
		int pIndex=start;
		int pivot=a[end];
		for(int i=start; i<end; i++)
		{
			if(a[i]<pivot)
			{
				
				int temp=a[i];
				a[i]=a[pIndex];
				a[pIndex]=temp;
				pIndex++;
			}
		}
		
		int temp=a[end];
		a[end]=a[pIndex];
		a[pIndex]=temp;
		return pIndex;
	}
	

	public static void main(String []args){
		int a[]=new int[]{10,50,15,5,25,30};
		System.out.println(Arrays.toString(a));
		quickSort(a,0,a.length-1);
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+" ");
	}
}