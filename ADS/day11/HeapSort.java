class HeapSort{

	public static  void heapSort(int arr[],int size){
		buildheap(arr,size);
		while(size > 0)
		{
			//swap(arr[0],arr[size])
			int temp=arr[0];
			arr[0]=arr[size];
			arr[size]=temp;
			size--;
			heapify(arr,0,size);
		}
	}
	
	public static void buildheap(int []arr, int size){
	
	for(int i=size/2;i>=0;i--)
		heapify(arr,i,size);
	}
	
	public static void heapify(int arr[],int index, int size){
		int left = 2*index + 1;
		int right = left+1;
		
		int max = index;
		
		if(left<=size && arr[left]> arr[max])
			max=left;
		
		if(right<=size && arr[right]>arr[max])
			max=right;
		
		if(index!=max)
		{
			//swap(arr[max],arr[index]);
			int temp=arr[max];
			arr[max]=arr[index];
			arr[index]=temp;
			heapify(arr,max,size);
		}
	}
	
	public static void main(String []args){
		int a[]=new int[]{10,50,15,5,25,30};
		
		heapSort(a,a.length-1);
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+" ");
	}
}